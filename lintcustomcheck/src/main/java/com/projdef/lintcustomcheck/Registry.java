package com.projdef.lintcustomcheck;

import com.android.tools.lint.client.api.IssueRegistry;
import com.android.tools.lint.detector.api.Issue;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Marius Savickas on 2016.12.14.
 */

public final class Registry extends IssueRegistry
    {
    @Override
    public List<Issue> getIssues() {
        return Arrays.asList(EnumDetector.ISSUE);
    }
}
