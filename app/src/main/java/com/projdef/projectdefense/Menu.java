/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang3.mutable.MutableLong;

import com.projdef.projectdefense.Base.BaseWorld;
import com.projdef.projectdefense.Entity.Gun;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Graphics.KeyAnimation;
import com.projdef.projectdefense.Manager.TransactionManager;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.Vector;
import com.projdef.projectdefense.R;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.KeyEvent;

/**
 * This file holds most of the Game states. Excluded are World and Transaction states.
 * @author Marius Savickas
 *
 */
class Menu extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private KeyAnimation Button_Play;
	private KeyAnimation Button_Score;
	private KeyAnimation Button_About;
	private KeyAnimation Button_Exit;
	
	private KeyAnimation Button_Sound;
	
	Menu()
	{
		//Text information
		paint.setStyle(Paint.Style.STROKE);
		paint.setAntiAlias(true);
		paint.setDither(false);
		paint.setFilterBitmap(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(Color.WHITE);
		dataPaint.setTextSize(15);
	}
	
	public void Initialize()
	{
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		//If initialized - reset animation
		if(Button_Play != null)
		{
			Button_Play.ChangeFrame(0);
			Button_Score.ChangeFrame(0);
			Button_About.ChangeFrame(0);
			Button_Exit.ChangeFrame(0);
		}
		
		if(Button_Sound != null)
		{
			if((Player.Instance().soundFlag & Common.FLAG_SOUND) != 0)
				Button_Sound.ChangeFrame(0);
			else
				Button_Sound.ChangeFrame(1);
		}
		
		//Skip it
		if(Initialised) return;
		
		//Set Bacground position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(0).getWidth())/2;
		TitleY = 60*Settings.Instance().screenDensity;
		
		//Used for determining position of buttons
		float x, y;
		
		//Space between maps
		int wordSpace = (int)(10*Settings.Instance().screenDensity);
		
		Button_Play = new KeyAnimation();
		Button_Score = new KeyAnimation();
		Button_About = new KeyAnimation();
		Button_Exit = new KeyAnimation();
		Button_Sound = new KeyAnimation();
		
		Button_Play.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_play, Settings.Instance().screenScaleMenu));
		Button_Play.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_play_pressed, Settings.Instance().screenScaleMenu));
		x = (Settings.Instance().screenWidth - Button_Play.getImgWidth(0))/2;
		y = (Settings.Instance().screenHeight - Button_Play.getImgHeight(0)*3)/2;
		Button_Play.SetLocation(x, y);
		
		
		Button_Score.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_score, Settings.Instance().screenScaleMenu));
		Button_Score.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_score_pressed, Settings.Instance().screenScaleMenu));
		x = (Settings.Instance().screenWidth - Button_Score.getImgWidth(0))/2;
		y += wordSpace + Button_Score.getImgHeight(0);
		Button_Score.SetLocation(x, y);
		
		
		Button_About.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_about, Settings.Instance().screenScaleMenu));
		Button_About.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_about_pressed, Settings.Instance().screenScaleMenu));
		x = (Settings.Instance().screenWidth - Button_About.getImgWidth(0))/2;
		y += wordSpace + Button_About.getImgHeight(0);
		Button_About.SetLocation(x, y);
		
		
		Button_Exit.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_exit, Settings.Instance().screenScaleMenu));
		Button_Exit.AddBitmap(Graphics.loadImg(R.drawable.bg_menu_buttons_exit_pressed, Settings.Instance().screenScaleMenu));
		x = (Settings.Instance().screenWidth - Button_Exit.getImgWidth(0))/2;
		y += wordSpace + Button_Exit.getImgHeight(0);
		Button_Exit.SetLocation(x, y);
		
		
		Button_Sound.AddBitmap(Graphics.loadImg(R.drawable.sound_on, Settings.Instance().screenScaleMenu*0.5f));
		Button_Sound.AddBitmap(Graphics.loadImg(R.drawable.sound_off, Settings.Instance().screenScaleMenu*0.5f));
		x = 15 * Settings.Instance().screenDensity;
		y = Settings.Instance().screenHeight - Button_Sound.getImgHeight(0) - 15 * Settings.Instance().screenDensity;
		Button_Sound.SetLocation(x, y);
		Button_Sound.SetOpacity(Math.round(255*0.75f));
		
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) != 0)
			Button_Sound.ChangeFrame(0);
		else
			Button_Sound.ChangeFrame(1);
		
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		
		//Play
		if(Common.isInBounds2(Button_Play.getLocation().x, Button_Play.getLocation().y, Button_Play.getImgWidth(0), Button_Play.getImgHeight(0), x, y))
		{
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.MapSelect);
			Draw = true;
		}
		//Scores
		else if(Common.isInBounds2(Button_Score.getLocation().x, Button_Score.getLocation().y, Button_Score.getImgWidth(0), Button_Score.getImgHeight(0), x, y))
		{
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Score);
			Draw = true;
		}
		//About
		else if(Common.isInBounds2(Button_About.getLocation().x, Button_About.getLocation().y, Button_About.getImgWidth(0), Button_About.getImgHeight(0), x, y))
		{			
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.About);
			Draw = true;
		}
		//Exit
		else if(Common.isInBounds2(Button_Exit.getLocation().x, Button_Exit.getLocation().y, Button_Exit.getImgWidth(0), Button_Exit.getImgHeight(0), x, y))
		{	
			Player.Instance().SaveFile();
			Settings.Instance().Activity.finish();
		}
		//sound
		else if(Common.isInBounds2(Button_Sound.getLocation().x, Button_Sound.getLocation().y, Button_Sound.getImgWidth(0), Button_Sound.getImgHeight(0), x, y))
		{	
			//On
			if((Player.Instance().soundFlag & Common.FLAG_SOUND) != 0)
			{
				//Make it off
				Button_Sound.ChangeFrame(1);
				Player.Instance().soundFlag = (byte) (Player.Instance().soundFlag & 0xFE);
				Settings.Instance().Sound.stopMenu();
			}
			//Off
			else
			{
				//Make it on
				Button_Sound.ChangeFrame(0);
				Player.Instance().soundFlag = (byte) (Player.Instance().soundFlag | Common.FLAG_SOUND);
				Settings.Instance().Sound.playMenu(Settings.Instance().Context);
			}
			
			Player.Instance().SaveFile();
			Draw = true;
		}
		else
		{
			Button_Play.ChangeFrame(0);
			Button_Score.ChangeFrame(0);
			Button_About.ChangeFrame(0);
			Button_Exit.ChangeFrame(0);
			Draw = true;
		}
		
		//Draw = true;
	}
	
	public void HandleTouchPress(float x, float y)
	{
		//Play
		if(Common.isInBounds2(Button_Play.getLocation().x, Button_Play.getLocation().y, Button_Play.getImgWidth(0), Button_Play.getImgHeight(0), x, y))
		{
			Button_Play.ChangeFrame(1);
			Draw = true;
		}
		//Scores
		else if(Common.isInBounds2(Button_Score.getLocation().x, Button_Score.getLocation().y, Button_Score.getImgWidth(0), Button_Score.getImgHeight(0), x, y))
		{
			Button_Score.ChangeFrame(1);
			Draw = true;
		}
		//About
		else if(Common.isInBounds2(Button_About.getLocation().x, Button_About.getLocation().y, Button_About.getImgWidth(0), Button_About.getImgHeight(0), x, y))
		{ 
			Button_About.ChangeFrame(1);
			Draw = true;
		}
		//Exit
		else if(Common.isInBounds2(Button_Exit.getLocation().x, Button_Exit.getLocation().y, Button_Exit.getImgWidth(0), Button_Exit.getImgHeight(0), x, y))
		{ 
			Button_Exit.ChangeFrame(1);
			Draw = true;
		}
		
		
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		Player.Instance().SaveFile();
		return false;
	}
	
	public void HandleMovement(float x, float y)
	{
		if(Button_Play.getFrame() != 0) 
		{
			Button_Play.ChangeFrame(0);
			Draw = true;
		}
		if(Button_Score.getFrame() != 0) 
		{
			Button_Score.ChangeFrame(0);
			Draw = true;
		}
		if(Button_About.getFrame() != 0) 
		{
			Button_About.ChangeFrame(0);
			Draw = true;
		}
		if(Button_Exit.getFrame() != 0) 
		{
			Button_Exit.ChangeFrame(0);
			Draw = true;
		}
		
		//Play
		if(Common.isInBounds2(Button_Play.getLocation().x, Button_Play.getLocation().y, Button_Play.getImgWidth(0), Button_Play.getImgHeight(0), x, y))
		{
			Button_Play.ChangeFrame(1);
			Draw = true;
		}
		//Scores
		else if(Common.isInBounds2(Button_Score.getLocation().x, Button_Score.getLocation().y, Button_Score.getImgWidth(0), Button_Score.getImgHeight(0), x, y))
		{
			Button_Score.ChangeFrame(1);
			Draw = true;
		}
		//About
		else if(Common.isInBounds2(Button_About.getLocation().x, Button_About.getLocation().y, Button_About.getImgWidth(0), Button_About.getImgHeight(0), x, y))
		{ 
			Button_About.ChangeFrame(1);
			Draw = true;
		}
		//Exit
		else if(Common.isInBounds2(Button_Exit.getLocation().x, Button_Exit.getLocation().y, Button_Exit.getImgWidth(0), Button_Exit.getImgHeight(0), x, y))
		{ 
			Button_Exit.ChangeFrame(1);
			Draw = true;
		}
	}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		//Clear canvas and draw background
		Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: Project Defense
		Settings.Instance().Canvas.drawBitmap(Graphics.TITLES.get(0), TitleX, TitleY, paint);
		
		Button_Play.Draw();
		Button_Score.Draw();
		Button_About.Draw();
		Button_Exit.Draw();
		
		Button_Sound.Draw();
		
		//Settings.Instance().Canvas.drawText(Settings.Instance().Game.fps + "fps", 260, 30, dataPaint);
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}
	
};














//
//========================================================== Lost ============================================
//
//This state is drawn when all humans dies. In other words, when you lose.
class MapSelect extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	private Paint dataPaintPressed = new Paint();
	private Paint starPaint = new Paint();
	int MapQuantity = 0;
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private int pressedButton = -1;
	
	MapSelect()
	{	
		//Text information
		paint.setStyle(Paint.Style.STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(28*Settings.Instance().screenDensity);
		
		dataPaintPressed.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaintPressed.setAntiAlias(true);
		dataPaintPressed.setColor(0xFFae8f4c);
		dataPaintPressed.setTextSize(28*Settings.Instance().screenDensity);
		
		starPaint.setAlpha(255);
	}
	
	public void Initialize()
	{
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		pressedButton = -1;
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(1).getWidth())/2;
		TitleY = 60*Settings.Instance().screenDensity;
		
		MapQuantity = Settings.Instance().numberOfMaps;
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		Initialised = false;
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		Iterator<Player.MapStats> MapIter = Player.Instance().stats.iterator();
		for(int i = 0, o = 0, p = 0; i < MapQuantity; i++, p++)
		{
			float drawX = (5*Settings.Instance().screenDensity)+(p*80*Settings.Instance().screenDensity);
			float drawY = (120*Settings.Instance().screenDensity)+(o*80*Settings.Instance().screenDensity);
			
			//Check for lock and boundaries
			if(!MapIter.next().lock && Common.isInBounds2(drawX, drawY, Graphics.MENU.get(1).getWidth(), Graphics.MENU.get(1).getHeight(), x, y))
			{
				//Tell the game what map number to use
				pressedButton = i;
				((World)Settings.Instance().Game.World).setMapNumber(i);
				//((World)Settings.Instance().Game.World).Initialize();
				if(DisplayNotices(i) == false)
					TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.World);
				
				Draw = true;
				return;
			}
			
			if(((i+1) % 4 == 0) && i != 0) {o++;p = -1;}
		}
		
		pressedButton = -1;
		Draw = true;
	}
	
	public void HandleTouchPress(float x, float y)
	{
		Iterator<Player.MapStats> MapIter = Player.Instance().stats.iterator();
		for(int i = 0, o = 0, p = 0; i < MapQuantity; i++, p++)
		{
			float drawX = (5*Settings.Instance().screenDensity)+(p*80*Settings.Instance().screenDensity);
			float drawY = (120*Settings.Instance().screenDensity)+(o*80*Settings.Instance().screenDensity);
			
			//Check for lock and boundaries
			if(!MapIter.next().lock && Common.isInBounds2(drawX, drawY, Graphics.MENU.get(1).getWidth(), Graphics.MENU.get(1).getHeight(), x, y))
			{
				//Tell the game what map number to use
				pressedButton = i;
				Draw = true;
				return;
			}
			
			if(((i+1) % 4 == 0) && i != 0) {o++;p = -1;}
		}
		
		pressedButton = -1;
		Draw = true;
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Menu);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		//Clear canvas and draw background
		Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		
		//Title: Select Map
		Settings.Instance().Canvas.drawBitmap(Graphics.TITLES.get(1), TitleX, TitleY, paint);
		
		//Iterate through map statistics
		Iterator<Player.MapStats> MapIter = Player.Instance().stats.iterator();
		Player.MapStats MapStats = null;
		for(int i = 0, o = 0, p = 0; i < MapQuantity; i++, p++)
		{	
			float drawX = (5*Settings.Instance().screenDensity)+(p*80*Settings.Instance().screenDensity);
			float drawY = (120*Settings.Instance().screenDensity)+(o*80*Settings.Instance().screenDensity);
			
			MapStats = MapIter.next();
			if(MapStats.lock == true)
			{
				paint.setAlpha(130);
				dataPaint.setAlpha(130);
				starPaint.setAlpha(130);
			}
			
			//Draws background of a button
			if(pressedButton == i)
				Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(3), drawX, drawY, paint);
			else Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(1), drawX, drawY, paint);
			
			//Render one digit number
			paint.setAlpha(paint.getAlpha()-102);
			dataPaint.setAlpha(dataPaint.getAlpha()-102);
			
			Rect textBounds = new Rect();
			dataPaint.getTextBounds("0", 0, 1, textBounds);
			
			
			//Render numbers
			if(pressedButton == i)
				Settings.Instance().Canvas.drawText("" + (i+1),
						   drawX + (Graphics.MENU.get(1).getWidth() - dataPaint.measureText("" + (i+1)))/2,
						   drawY + textBounds.height()+15*Settings.Instance().screenDensity,
						   dataPaintPressed);
			else Settings.Instance().Canvas.drawText("" + (i+1),
												   drawX + (Graphics.MENU.get(1).getWidth() - dataPaint.measureText("" + (i+1)))/2,
												   drawY + textBounds.height()+15*Settings.Instance().screenDensity,
												   dataPaint);
				
			
			//Render stars
			for(int star = 0, offset = -3; star < 3; star++, offset+=3)
			{
				//Only to make the positions more attractive
				if(offset > 0) offset*=-1;
				
				//Render accomplished stars
				if(Player.Instance().stats.get(i).stars > star)
				{
					Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(10),
													    drawX + 3*Settings.Instance().screenDensity + Graphics.THUMBNAILS.get(10).getWidth()*star,
													    drawY - 7*Settings.Instance().screenDensity + offset*Settings.Instance().screenDensity + (Graphics.MENU.get(1).getHeight()-Graphics.THUMBNAILS.get(10).getHeight()),
													    starPaint);
				}
				//Render unaccomplished stars
				else 
				{
					Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(11),
													    drawX + 3*Settings.Instance().screenDensity + Graphics.THUMBNAILS.get(11).getWidth()*star,
													    drawY - 7*Settings.Instance().screenDensity + offset*Settings.Instance().screenDensity + (Graphics.MENU.get(1).getHeight()-Graphics.THUMBNAILS.get(11).getHeight()),
													    starPaint);
				}
			}
			

			paint.setAlpha(255);
			dataPaint.setAlpha(255);
			starPaint.setAlpha(255);
			
			if(MapStats.lock == true)
			{
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(8),
													  drawX + (Graphics.MENU.get(1).getWidth() - Graphics.THUMBNAILS.get(8).getWidth())/2,
													  drawY + (Graphics.MENU.get(1).getHeight() -  Graphics.THUMBNAILS.get(8).getHeight())/2,
													  paint);
			}
			
			if(((i+1) % 4 == 0) && i != 0) {o++;p = -1;}
		}
		
		
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}

	/**
	 * Displays notices
	 * @param mapnum - number of a map
	 * @return	returns true if notice was displayed, otherwise false
	 */
	private boolean DisplayNotices(int mapnum)
	{
		Settings S = Settings.Instance();
		Player 	 P = Player.Instance();
		Notice notice = (Notice)S.Game.Notice;
		//Display notices accordingly to a map number
		switch(mapnum)
		{
		case 0:
			if((P.noticeFlags & Common.FLAG_NOTICE1) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE1);
			P.SaveFile();
			
			notice.setupStates(this, new JobAccepted(), new JobNotAccepted());
			notice.Text("Just in a few hours the entire world went to hell. Monsters are appearing from nowhere and attacking humans.");
			notice.Text("Nobody knows why this is happening; people are scared, so they put you in charge of protecting them.");
			notice.Text(" ");
			notice.Text("Do you accept this job?");
			TransactionManager.Instance().doSimple(notice);
			return true;
		case 1:
			if((P.noticeFlags & Common.FLAG_NOTICE2) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE2);
			P.SaveFile();
			
			notice.setupStates(this, S.Game.World, true, false);
			notice.Text("Phew� Last fight was a nasty one, but you got the job done. That base is well defended now.");
			notice.Text("Good thinking in moving quickly to the next area! Give them hell!");
			TransactionManager.Instance().doSimple(notice);
			return true;
			
		case 3:
			if((P.noticeFlags & Common.FLAG_NOTICE3) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE3);
			P.SaveFile();
			
			notice.setupStates(this, S.Game.World, true, false);
			notice.Text("Okay. Now this earth is partially defended from those creatures. Also some information came up about them.");
			notice.Text("As it appears, those things crawled out of caves and holes. Nothing much is known besides the fact that they are probably from the center of the earth, but what were they doing there?");
			TransactionManager.Instance().doSimple(notice);
			return true;
			
		case 5:
			if((P.noticeFlags & Common.FLAG_NOTICE4) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE4);
			P.SaveFile();
			
			notice.setupStates(this, S.Game.World, true, false);
			notice.Text("Reconnaissance mission showed that there is a large quantity of those monsters.  Their army is probably large enough to destroy every human on this earth. Is this what they are doing?");
			TransactionManager.Instance().doSimple(notice);
			return true;
			
		case 7:
			if((P.noticeFlags & Common.FLAG_NOTICE5) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE5);
			P.SaveFile();
			
			notice.setupStates(this, S.Game.World, true, false);
			notice.Text("Our 31st Anti-bastard division caught one of those bastards. By jacking up a car battery to his nipples we found out their plans. Apparently we were correct in thinking that their goal is to destroy us. Also, it became clear how they survived underground. Their habitat was not at the earth's center, but instead it was between the earth�s center and the earth's surface. While being underground, they managed to make an artificial sun, which provided energy and warmth for them. So why did they attack humans if the conditions were decent for life below the earth's surface?");
			TransactionManager.Instance().doSimple(notice);
			return true;
		
		case 9:
			if((P.noticeFlags & Common.FLAG_NOTICE6) != 0) return false;
			
			P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE6);
			P.SaveFile();
			
			notice.setupStates(this, S.Game.World, true, false);
			notice.Text("Get ready! Opposite forces are getting stronger and it looks like they are preparing for the final attack. I guess they will be bringing some hell to us now, ey?");
			TransactionManager.Instance().doSimple(notice);
			return true;
		};
		
		return false;
	}
	
	public class JobAccepted implements MSG
	{
		public void display()
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			notice.setupStates(S.Game.MapSelect, S.Game.World, true, false);
			notice.Text("Great! You will be the savior of the earthlings.");
			TransactionManager.Instance().doSimple(notice);
		}
	}
	
	public class JobNotAccepted implements MSG
	{
		public void display()
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			notice.setupStates(S.Game.MapSelect, S.Game.World, true, false);
			notice.Text("You have no choice.");
			notice.Text("They put a gun to your head and said that you will be a sacrifice to the monsters� ");
			notice.Text("Just do it, okay?");
			TransactionManager.Instance().doSimple(notice);
		}
	}
	
	public class TutOn implements MSG
	{
		public void display()
		{
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT_ON);
			Player.Instance().SaveFile();
			TransactionManager.Instance().doSimple(Settings.Instance().Game.World);
		}
	}
	
	public class TutOff implements MSG
	{
		public void display()
		{
			Player.Instance().tutorialFlags = (byte)0xFF;
			Player.Instance().SaveFile();
			TransactionManager.Instance().doSimple(Settings.Instance().Game.World);
		}
	}
	
	public interface MSG
	{
		public void display();
	}
};






/**
 * About state.
 */
class About extends BaseWorld
{
	private Paint paint;
	private Paint dataPaint;
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private String text;
	
	About()
	{	
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		//Text information
		paint = new Paint();
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(15);
		
		dataPaint = new Paint();
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(15);
	}
	
	public void Initialize()
	{	
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		Draw = true;
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(4).getWidth())/2;
		TitleY = 60*Settings.Instance().screenDensity;
		
		//Adjust for bigger screens.
		dataPaint.setTextSize(dataPaint.getTextSize()*Settings.Instance().screenDensity);
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		Draw = true;
	}
	
	public void HandleTouchPress(float x, float y){}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Menu);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		float Y = BGy+200*Settings.Instance().screenDensity;
		float spacing = 20*Settings.Instance().screenDensity;
		
		//Clear canvas and draw background
		Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: About
		Settings.Instance().Canvas.drawBitmap(Graphics.TITLES.get(4), TitleX, TitleY, paint);
		
		text = "Project Defense";
		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y, dataPaint);
		
		text = "Copyright � 2015";
		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*2, dataPaint);
		
		
		text = "Marius Savickas";
		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*4, dataPaint);
		
		text = "Aivaras Baliuckas";
		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*6, dataPaint);
		
		text = "IF-3/8";
		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*8, dataPaint);
		
		
//		text = "I'm just a self-taught developer with an";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*3, dataPaint);
//		text = "interest in developing games.";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*4, dataPaint);
//		
//		text = "If you liked this game and want to tell";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*6, dataPaint);
//		text = "me, you can do that via email:";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*7, dataPaint);
//		
//		text = "projdefense@gmail.com";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*9, dataPaint);
//		
//		
//		text = "Credits";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*11, dataPaint);
//		text = "Music:   moosader.com";
//		Settings.Instance().Canvas.drawText(text, Common.hcenter(Settings.Instance().screenWidth, text, dataPaint), Y+spacing*13, dataPaint);
//		
		//Draw into phone canvas.
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}
};



class Score extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	private Paint paint_total = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	Score()
	{	
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(15);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(15);
		
		paint_total.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_total.setAntiAlias(true);
		paint_total.setColor(0xFF81ecff);
		paint_total.setTextSize(20);
	}
	
	public void Initialize()
	{	
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		Draw = true;
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		dataPaint.setTextSize(dataPaint.getTextSize()*Settings.Instance().screenDensity);
		paint_total.setTextSize(paint_total.getTextSize()*Settings.Instance().screenDensity);
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(5).getWidth())/2;
		TitleY = 60*Settings.Instance().screenDensity;
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		//Draw = true;
	}
	
	public void HandleTouchPress(float x, float y){}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Menu);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		//Clear canvas and draw background
		Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: Score
		Settings.Instance().Canvas.drawBitmap(Graphics.TITLES.get(5), TitleX, TitleY, paint);
		
		ArrayList<Player.MapStats> list = Player.Instance().stats;
		
		//Settings.Instance().Canvas.drawText("Unlock level: " + Player.Instance().tierLock, 20, 125, dataPaint);
		float Y = 130*Settings.Instance().screenDensity;
		float X = 120*Settings.Instance().screenDensity;
		int i = 1;
		int spacing = Math.round(30*Settings.Instance().screenDensity);
		int staroffset = Graphics.THUMBNAILS.get(10).getHeight()/2 + Math.round(5*Settings.Instance().screenDensity);
		long total = 0;
		for(Player.MapStats M: list)
		{
			Settings.Instance().Canvas.drawText("Level: " + i, 20*Settings.Instance().screenDensity, Y, dataPaint);

			if(M.stars >= 1)
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(10), X, Y-staroffset, null);
			else
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(11), X, Y-staroffset, null);
			
			if(M.stars >= 2)
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(10), X+spacing, Y-staroffset, null);
			else
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(11), X+spacing, Y-staroffset, null);
			
			if(M.stars >= 3)
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(10), X+spacing*2, Y-staroffset, null);
			else
				Settings.Instance().Canvas.drawBitmap(Graphics.THUMBNAILS.get(11), X+spacing*2, Y-staroffset, null);
			
			Settings.Instance().Canvas.drawText("" + M.score, 260*Settings.Instance().screenDensity, Y, dataPaint);
			
			i++;
			Y+=Math.round(25*Settings.Instance().screenDensity);
			total+=M.score;
		}
		
		Settings.Instance().Canvas.drawText("Total Score: " + total, (Settings.Instance().screenWidth-paint_total.measureText("Total Score: " + total))/2, 420*Settings.Instance().screenDensity, paint_total);
		
		//Draw into phone canvas.
		//Settings.Instance().Canvas.drawText(Settings.Instance().Game.fps + "fps", 260, 30, dataPaint);
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}
};






//
//========================================================== Lost ============================================
//
//This state is drawn when all humans dies. In other words, when you lose.
class Lost extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private KeyAnimation ButtonMenu;
	
	private String lostText;
	
	Lost()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(20);
	}
	
	public void Initialize()
	{
		for(Gun G: ((World)Settings.Instance().Game.World).GunList)
		{
			if(G != null)
				G.unloadSound();
		}
		
		//Skip it
		if(Initialised) return;
		
		//Set Bacground position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(3).getWidth())/2;
		TitleY = 100*Settings.Instance().screenDensity;
		
		
		ButtonMenu = new KeyAnimation();
		
		ButtonMenu.AddBitmap(Graphics.loadImg(R.drawable.bg_button_tomenu, Settings.Instance().screenScaleMenu));
		ButtonMenu.AddBitmap(Graphics.loadImg(R.drawable.bg_button_tomenu_pressed, Settings.Instance().screenScaleMenu));
		float x = (Settings.Instance().screenWidth - ButtonMenu.getImgWidth(0))/2;
		float y = (Settings.Instance().screenHeight - ButtonMenu.getImgHeight(0)*2.5f);
		ButtonMenu.SetLocation(x, y);
		
		Initialised = true;
	}
	
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		//To menu
		if(Common.isInBounds2(ButtonMenu.getLocation().x, ButtonMenu.getLocation().y, ButtonMenu.getImgWidth(0), ButtonMenu.getImgHeight(0), x, y))
		{
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Menu);
		}
		
		ButtonMenu.ChangeFrame(0);
	}
	
	public void HandleTouchPress(float x, float y)
	{
		if(Common.isInBounds2(ButtonMenu.getLocation().x, ButtonMenu.getLocation().y, ButtonMenu.getImgWidth(0), ButtonMenu.getImgHeight(0), x, y))
		{
			ButtonMenu.ChangeFrame(1);
		}
		
		Draw = true;
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.Menu);
			return true;
	    }
		
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		//Clear canvas and draw background
		Settings.Instance().Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: Lose
		Settings.Instance().Canvas.drawBitmap(Graphics.TITLES.get(3), TitleX, TitleY, paint);
		
		Graphics.drawMultiLineText(lostText, TitleY + Graphics.TITLES.get(3).getHeight() + 30 * Settings.Instance().screenDensity, dataPaint, Settings.Instance().Canvas);
		
		ButtonMenu.Draw();
		
		//c.drawText(_gameworld._fps + "fps", 260, 30, dataPaint);
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}
	
	public void setPhrase()
	{
		lostText = Common.generateLostPhrase();
	}
};







//
//========================================================== Win ============================================
//
//This state is drawn when all monsters die. In other words, when you win.
class Win extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private KeyAnimation ButtonOK;

	private volatile String winText = null;
	
	Win()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(20*Settings.Instance().screenDensity);
	}
	
	public void Initialize()
	{	
		
		for(Gun G: ((World)Settings.Instance().Game.World).GunList)
		{
			if(G != null)
				G.unloadSound();
		}
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(2).getWidth())/2;
		TitleY = 120*Settings.Instance().screenDensity;
		
		ButtonOK = new KeyAnimation();
		
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok, Settings.Instance().screenScaleMenu));
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok_pressed, Settings.Instance().screenScaleMenu));
		float x = (Settings.Instance().screenWidth - ButtonOK.getImgWidth(0))/2;
		float y = (Settings.Instance().screenHeight - ButtonOK.getImgHeight(0)*3f);
		ButtonOK.SetLocation(x, y);

		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		//OK
		if(Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{	
			((WinUnlocks)Settings.Instance().Game.WinUnlocks).unlocked = false;
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.WinScore);
		}
		
		ButtonOK.ChangeFrame(0);
	}
	
	public void HandleTouchPress(float x, float y)
	{
		if(Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{
			ButtonOK.ChangeFrame(1);
		}
		
		Draw = true;
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.WinScore);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		Settings S = Settings.Instance();
		//Clear canvas and draw background
		S.Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: Win
		S.Canvas.drawBitmap(Graphics.TITLES.get(2), TitleX, TitleY, paint);
		
		Graphics.drawMultiLineText(winText, TitleY + Graphics.TITLES.get(2).getHeight() + 30 * Settings.Instance().screenDensity, dataPaint, S.Canvas);
		
		ButtonOK.Draw();
		
		//Draw into phone canvas.
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
	}
	
	public void setPhrase()
	{
		winText = Common.generateWinPhrase();
	}
};








/**
 * State where text or images are displayed
 * @author Marius Savickas
 *
 */
class Notice extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint paint_text = new Paint();
	
	//Save states for further usage
	private BaseWorld prevState, nextState;
	//Bitmaps for drawing
	private Bitmap prevBg_top, prevBg_bottom, prevBg_left, prevBg_right,
				   noticeBg, notice;
	
	private int noticeBgWidth, noticeBgHeight,
				verticalGap, horizontalGap,
				buttonHeight, marginX, marginY;
	
	//Scroll variables
	private float scrollPosition, lastScrollPosition, velocity;
	private int scrollableHeight, BGy;
	private Vector lastCoord, curPosition;
	
	private boolean transaction, question, blurBGdrawn;
	
	private KeyAnimation OkButton, YesButton, NoButton;
	
	private MapSelect.MSG yesMSG, noMSG;
	
	Notice()
	{
		//fps information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(15);
		
		paint_text.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_text.setAntiAlias(true);
		paint_text.setColor(Color.WHITE);
		paint_text.setTextSize(20);
		
		OkButton = new KeyAnimation();
		OkButton.AddBitmap(Graphics.NOTICE.get(3));
		OkButton.AddBitmap(Graphics.NOTICE.get(4));
		
		YesButton = new KeyAnimation();
		YesButton.AddBitmap(Graphics.NOTICE.get(5));
		YesButton.AddBitmap(Graphics.NOTICE.get(6));
		
		NoButton = new KeyAnimation();
		NoButton.AddBitmap(Graphics.NOTICE.get(7));
		NoButton.AddBitmap(Graphics.NOTICE.get(8));
	}
	
	
	/**
	 * Defines what state has been previous and what state should be next. Should be called first
	 * @param prevState
	 * @param nextState
	 * @param transaction - true if there is a transaction after a notices
	 * @param question  - true to enable yes or no.
	 */
	public void setupStates(BaseWorld prevState, BaseWorld nextState, boolean transaction, boolean question)
	{
		this.prevState = prevState;
		this.nextState = nextState;
		
		this.transaction 	= transaction;
		this.question		= question;
		
		notice = null;
		velocity = 0;
		
		marginX = Math.round(30*Settings.Instance().screenDensity);
		marginY = Graphics.NOTICE.get(0).getHeight();
		
		yesMSG = null;
		noMSG = null;
	}
	
	public void setupStates(BaseWorld prevState, MapSelect.MSG yesmsg, MapSelect.MSG nomsg)
	{
		this.prevState = prevState;
		this.nextState = null; //nextState;
		
		this.transaction 	= false;
		this.question		= true;
		
		notice = null;
		velocity = 0;
		
		marginX = Math.round(30*Settings.Instance().screenDensity);
		marginY = Graphics.NOTICE.get(0).getHeight();
		
		yesMSG = yesmsg;
		noMSG = nomsg;
	}
	
	
	/**
	 * Adds text to show in the notice
	 * @param txt - string of text
	 */
	public void Text(String txt)
	{
		Init();
		
		//Get old parameters
		int oldWidth = noticeBgWidth - marginX*2;
		int oldHeight = 0;
		
		if(notice != null)
		{
			oldWidth = notice.getWidth();
			oldHeight = notice.getHeight();
		}
		
		//Calculate size of the word bitmap
		int lastValidIndex = 0;
		int lastSentenceIndex = 0;
		int rows = 0;
		for(int i = 0; i < txt.length(); i++)
		{
			if(txt.charAt(i) == 32 || txt.charAt(i) == '.' || txt.charAt(i) == '!' || txt.charAt(i) == '?')
			{
				String tmpStr = txt.substring(lastSentenceIndex, i);
				if(paint_text.measureText(tmpStr) >= oldWidth || txt.length() == i-1)
				{
					rows++;
					lastSentenceIndex = lastValidIndex+1;
				}
				lastValidIndex = i;
			}
		    if(txt.length() == i+1)
				rows++;
		}
		
		//Calculate new parameters
		int newWidth = oldWidth;
		int newHeight = oldHeight + rows*(Math.round(paint_text.getTextSize()+2))+10;
		
		//Save old bitmap
		Bitmap oldNotice = null;
		if(notice != null) oldNotice = notice.copy(Graphics.BitmapFormat, true);
		//Prepare bitmap for drawing
		notice = Bitmap.createBitmap(newWidth, newHeight, Graphics.BitmapFormat);
		Canvas c = new Canvas(notice);
		//Draw old bitmap
		if(oldNotice != null) c.drawBitmap(oldNotice, 0,  0, null);
		
		
		//Draw text
		String drawValue = "";
		lastValidIndex = 0;
		lastSentenceIndex = 0;
		float Y = paint_text.getTextSize()+oldHeight;
		int textLength = txt.length();
		for(int i = 0; i < textLength; i++)
		{
			//Handle multiple lines
			if(txt.charAt(i) == 32 || txt.charAt(i) == '.' || txt.charAt(i) == '!' || txt.charAt(i) == '?')
			{
				//Get characters
				String tmpStr = txt.substring(lastSentenceIndex, i);
				
				//Check the boundaries of this sentence
				if(paint_text.measureText(tmpStr) >= newWidth || textLength == i-1)
				{
					
					//Draw string to bitmap
					drawValue = txt.substring(lastSentenceIndex, lastValidIndex);
					
					c.drawText(drawValue, Common.hcenter(newWidth, drawValue, paint_text), Y, paint_text);
					Y+=paint_text.getTextSize()+2;
					//Skip space
					lastSentenceIndex = lastValidIndex+1;
				}
				
				//Save index of the last word which still is in the boundaries
				lastValidIndex = i;
			}
		    
			//Handle one line sentence
			if(textLength == i+1)
			{
				drawValue = txt.substring(lastSentenceIndex, i+1);
				c.drawText(drawValue, Common.hcenter(newWidth, drawValue, paint_text), Y, paint_text);
			}
		}
	}
	
	/**
	 * Adds an image to a notice
	 * @param bmp - image to add
	 */
	public void Image(Bitmap bmp)
	{
		Init();
		
		//Scale bitmap to fit into notice
		float scale = 80*Settings.Instance().screenDensity/bmp.getWidth();
		Bitmap bitmap = Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()*scale), Math.round(bmp.getHeight()*scale), true);
		
		//Default parameters
		int oldWidth = noticeBgWidth;
		int oldHeight = 0;
		
		//Get old parameters if there are any
		if(notice != null)
		{
			oldWidth = notice.getWidth();
			oldHeight = notice.getHeight();
		}
		
		//Calculate new parameters
		int newWidth = oldWidth;
		int newHeight = oldHeight + bitmap.getHeight();
		
		Bitmap oldNotice = null;
		
		if(notice != null)
			oldNotice = notice.copy(Graphics.BitmapFormat, true);
		
		//Prepare bitmap for drawing
		notice = Bitmap.createBitmap(newWidth, newHeight, Graphics.BitmapFormat);
		Canvas c = new Canvas(notice);
		
		if(oldNotice != null)
			c.drawBitmap(oldNotice,  0,  0, null);
		
		//Add new bitmap to notice
		c.drawBitmap(bitmap, Common.hcenter(newWidth, bitmap), oldHeight, null);
	}
	
	/**
	 * Initializes notice state.
	 * setupStates() should be called first.
	 * Text and Images are optional.
	 */
	public void Initialize()
	{	
		//Prepare new bitmap
		Bitmap prevBg = Bitmap.createBitmap(Settings.Instance().screenWidth, Settings.Instance().screenHeight, Graphics.BitmapFormat);
		Canvas prevc = new Canvas(prevBg);
		//Render previous state into new bitmap
		prevState.Draw(prevc);
		//blurring
		prevBg = Graphics.scaleImg(prevBg, 0.5f);
		prevBg = Graphics.scaleImg(prevBg, 0.5f);
		prevBg = Graphics.scaleImg(prevBg, 4.0f);
		
		prevBg_top = Graphics.cropImg(prevBg, 0, 0, Settings.Instance().screenWidth, verticalGap + BGy, 1.0f);
		prevBg_left = Graphics.cropImg(prevBg, 0, verticalGap + BGy, horizontalGap, Settings.Instance().screenHeight - (verticalGap + BGy)*2, 1.0f);
		prevBg_right = Graphics.cropImg(prevBg, Settings.Instance().screenWidth-horizontalGap, verticalGap + BGy, horizontalGap, Settings.Instance().screenHeight - (verticalGap+BGy), 1.0f);
		prevBg_bottom = Graphics.cropImg(prevBg, 0, Settings.Instance().screenHeight-(verticalGap + BGy), Settings.Instance().screenWidth, verticalGap + BGy, 1.0f);
		
		OkButton.ChangeFrame(0);
		YesButton.ChangeFrame(0);
		NoButton.ChangeFrame(0);
		
		//Prepare scrolling
		if(notice != null)
			if(notice.getHeight()+marginY > noticeBgHeight-buttonHeight) scrollableHeight = (noticeBgHeight-buttonHeight) - (notice.getHeight()+marginY);
			else scrollableHeight = 0;
		else scrollableHeight = 0;
		
		lastCoord = null;
		curPosition = new Vector(marginX, marginY);
		
		scrollPosition = 0;
		lastScrollPosition = 0;
		
		blurBGdrawn = false;
		
		Draw = true;
	}
	
	
	/**
	 * Initializes bitmaps for drawing. Should be called before adding text or images, because
	 * it avoids null exception
	 */
	private void Init()
	{
		if(noticeBg != null) return;
		
		buttonHeight = Graphics.NOTICE.get(3).getHeight();//*Settings.Instance().screenDensity);
		
		horizontalGap = Math.round(20*Settings.Instance().screenDensity);
		verticalGap = Math.round(30*Settings.Instance().screenDensity);
		
		noticeBgWidth = Math.round(320*Settings.Instance().screenDensity - horizontalGap*2);
		noticeBgHeight = Math.round(480*Settings.Instance().screenDensity - verticalGap*2);
		
		//Adjust for bigger screens
		BGy = (Settings.Instance().screenHeight - Graphics.MAP_1.get(0).getHeight())/2;
		
		//Adjust text size of different screen densities
		paint_text.setTextSize(paint_text.getTextSize()*Settings.Instance().screenDensity);
		
		//Prepare notice's state background
		noticeBg = Bitmap.createBitmap(noticeBgWidth, noticeBgHeight, Graphics.BitmapFormat);
		Canvas notc = new Canvas(noticeBg);
		notc.drawColor(Color.BLACK);
		
		OkButton.SetLocation(0, noticeBgHeight-buttonHeight);
		YesButton.SetLocation(0, noticeBgHeight-buttonHeight);
		NoButton.SetLocation(YesButton.getImgWidth(0), noticeBgHeight-buttonHeight);
		
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		Initialised = false;
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		if(lastCoord != null) lastCoord = null;
		
		//Check the OK button
		if(Common.isInBounds2(horizontalGap,
							  Settings.Instance().screenHeight - verticalGap - OkButton.getImgHeight(0) - BGy,
							  OkButton.getImgWidth(0),
							  OkButton.getImgHeight(0),
							  x, y) && !question)
		{
			Settings.Instance().stopFPSCount = false;
			
			if(transaction)
				TransactionManager.Instance().doTransaction(prevState, nextState);
			else
				TransactionManager.Instance().doSimple(nextState);
			
			OkButton.ChangeFrame(1);
			Draw = true;
		}
		//Yes button
		else if(Common.isInBounds2(horizontalGap,
				  Settings.Instance().screenHeight - verticalGap - YesButton.getImgHeight(0) - BGy,
				  YesButton.getImgWidth(0),
				  YesButton.getImgHeight(0),
				  x, y) && question)
		{
			Settings.Instance().stopFPSCount = false;
			//Change state
			if(transaction)
				TransactionManager.Instance().doTransaction(prevState, nextState);
			else
			{
				//Display yes message instead of changing states
				if(yesMSG != null)
				{
					yesMSG.display();
				}
				//Change state
				else
					TransactionManager.Instance().doSimple(nextState);
			}
			
			YesButton.ChangeFrame(1);
			Draw = true;
		}
		//No button
		else if(Common.isInBounds2(horizontalGap + YesButton.getImgWidth(0),
				  Settings.Instance().screenHeight - verticalGap - NoButton.getImgHeight(0) - BGy,
				  NoButton.getImgWidth(0),
				  NoButton.getImgHeight(0),
				  x, y) && question)
		{
			Settings.Instance().stopFPSCount = false;
			
			//Display no message instead of changing states
			if(noMSG != null)
				noMSG.display();
			else
				TransactionManager.Instance().doSimple(prevState);
			
			NoButton.ChangeFrame(1);
			Draw = true;
		}
		else 
		{
			OkButton.ChangeFrame(0);
			YesButton.ChangeFrame(0);
			NoButton.ChangeFrame(0);
			Draw = true;
		}
		
		
	}
	
	public void HandleTouchPress(float x, float y)
	{
		if(Common.isInBounds2(horizontalGap,
				  Settings.Instance().screenHeight - verticalGap - OkButton.getImgHeight(0) - BGy,
				  OkButton.getImgWidth(0),
				  OkButton.getImgHeight(0),
				  x, y) && !question)
		{
			OkButton.ChangeFrame(1);
			Draw = true;
		}
		//Yes button
		else if(Common.isInBounds2(horizontalGap,
				  Settings.Instance().screenHeight - verticalGap - YesButton.getImgHeight(0) - BGy,
				  YesButton.getImgWidth(0),
				  YesButton.getImgHeight(0),
				  x, y) && question)
		{
			YesButton.ChangeFrame(1);
			Draw = true;
		}
		//No button
		else if(Common.isInBounds2(horizontalGap + YesButton.getImgWidth(0),
				  Settings.Instance().screenHeight - verticalGap - NoButton.getImgHeight(0) - BGy,
				  NoButton.getImgWidth(0),
				  NoButton.getImgHeight(0),
				  x, y) && question)
		{
			NoButton.ChangeFrame(1);
			Draw = true;
		}
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			return true;
	    }
		
		return false;
	}
	
	public void HandleMovement(float x, float y)
	{
		//Initialize last coordinate if there was none
		if(lastCoord == null)
		{
			lastCoord = new Vector(0, y);
			return;
		}
		
		//Check if there was movement
		else if(Vector.getDistance(lastCoord, new Vector(lastCoord.x, y)) > 0)
		{
			//Check boundaries of movement
			//lastScrollPosition = scrollPosition;
			scrollPosition += (y - lastCoord.y);
			
			if(scrollPosition < scrollableHeight) scrollPosition = scrollableHeight;
			else if(scrollPosition > 0) scrollPosition = 0;
			
			//Save last coordinate
			lastCoord = new Vector(lastCoord.x, y);
			
			//Tell to redraw
			Draw = true;
		}
		
		//OK button
		if(Common.isInBounds2(horizontalGap,
				  Settings.Instance().screenHeight - verticalGap - OkButton.getImgHeight(0) - BGy,
				  OkButton.getImgWidth(0),
				  OkButton.getImgHeight(0),
				  x, y) && !question)
		{
			OkButton.ChangeFrame(1);
			Draw = true;
		}
		//Yes button
		else if(Common.isInBounds2(horizontalGap,
				  Settings.Instance().screenHeight - verticalGap - YesButton.getImgHeight(0) - BGy,
				  YesButton.getImgWidth(0),
				  YesButton.getImgHeight(0),
				  x, y) && question)
		{
			YesButton.ChangeFrame(1);
			NoButton.ChangeFrame(0);
			Draw = true;
		}
		//No button
		else if(Common.isInBounds2(horizontalGap + YesButton.getImgWidth(0),
				  Settings.Instance().screenHeight - verticalGap - NoButton.getImgHeight(0) - BGy,
				  NoButton.getImgWidth(0),
				  NoButton.getImgHeight(0),
				  x, y) && question)
		{
			NoButton.ChangeFrame(1);
			YesButton.ChangeFrame(0);
			Draw = true;
		}
		else
		{
			if(OkButton.getFrame() != 0)
			{
				OkButton.ChangeFrame(0);
				Draw = true;
			}
			if(YesButton.getFrame() != 0)
			{
				YesButton.ChangeFrame(0);
				Draw = true;
			}
			if(NoButton.getFrame() != 0)
			{
				NoButton.ChangeFrame(0);
				Draw = true;
			}	
		}
	}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{	
		//draw previous state's background
		if(!blurBGdrawn) //Optimization
		{
			Settings.Instance().Canvas.drawBitmap(prevBg_top, 0, 0, null);
			Settings.Instance().Canvas.drawBitmap(prevBg_left, 0, verticalGap + BGy, null);
			Settings.Instance().Canvas.drawBitmap(prevBg_right, Settings.Instance().screenWidth-horizontalGap, verticalGap + BGy, null);
			Settings.Instance().Canvas.drawBitmap(prevBg_bottom, 0, Settings.Instance().screenHeight-(verticalGap + BGy), null);
			blurBGdrawn = true;
		}
			
		Bitmap tmpBmp = Bitmap.createBitmap(noticeBg);
		
		if(notice != null)
		{
			Canvas c = new Canvas(tmpBmp);
			c.drawBitmap(notice, curPosition.x-2, curPosition.y+lastScrollPosition, null);
			
			//Draw top
			c.drawBitmap(Graphics.NOTICE.get(0), 0, 0, null);
			//Draw left
			c.drawBitmap(Graphics.NOTICE.get(1), 0, Graphics.NOTICE.get(0).getHeight(), null);
			//Draw right
			c.drawBitmap(Graphics.NOTICE.get(2), tmpBmp.getWidth()-Graphics.NOTICE.get(2).getWidth(), Graphics.NOTICE.get(0).getHeight(), null);
			
			//Draw button
			if(question)
			{
				YesButton.Draw(c);
				NoButton.Draw(c);
			}
			else
				OkButton.Draw(c);	
		}
		
		
		//Draw new appended bitmap
		Settings.Instance().Canvas.drawBitmap(tmpBmp, horizontalGap, verticalGap+BGy, null);
		
		//Settings.Instance().Canvas.drawText(Settings.Instance().Game.fps + "fps", 260, 60, paint);
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
		if(lastScrollPosition == scrollPosition) return;
		
		velocity = (lastScrollPosition - scrollPosition)*0.2f;
		
		if(velocity < 0) velocity = velocity*-1;
		
		if(lastScrollPosition < scrollPosition) lastScrollPosition+=velocity;
		else if(lastScrollPosition > scrollPosition) lastScrollPosition-=velocity;
		
		if((lastScrollPosition - scrollPosition) <= 1 && (lastScrollPosition - scrollPosition) >= -1)
			lastScrollPosition = scrollPosition;
		
		Draw = true;
	}
};




//
//========================================================== Win ============================================
//
//This state is drawn when all monsters die. In other words, when you win.
class WinScore extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	private Paint dataPaint_Total = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private KeyAnimation ButtonOK;
	
	private long totalScore, humanScore, gunScore, goldScore, killScore;
	//Used for animation 
	private MutableLong animtotalScore, animhumanScore, animgunScore, animgoldScore, animkillScore,
				 animhumans, animguns, curAnimVar;
	
	boolean animate = false;
	
	WinScore()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(20*Settings.Instance().screenDensity);
		
		dataPaint_Total.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint_Total.setAntiAlias(true);
		dataPaint_Total.setColor(0xFF81ecff);
		dataPaint_Total.setTextSize(30*Settings.Instance().screenDensity);
	}
	
	public void Initialize()
	{
		Settings S = Settings.Instance();
		
		humanScore = (S.totalHumans.getValue() - S.aliveHumans.getValue())*400;
		gunScore = (S.totalGuns.getValue() - S.deployedGuns.getValue()) * 50;
		goldScore = S.GOLD.getValue();
		killScore = S.killScore.getValue();
		totalScore =  gunScore + goldScore + killScore - humanScore;
		
		
		if(totalScore < 500)
			totalScore = 500;
		
		
		int max = (int) Math.floor(S.totalHumans.getValue() * 0.8f);
		int min = (int) Math.floor(S.totalHumans.getValue() * 0.4f);
		/*
		 * Save score
		 */
		int mapNum = ((World)S.Game.World).getMapNumber();
		if(Player.Instance().stats.get(mapNum).humans < S.aliveHumans.getValue())
		{
			Player.Instance().stats.get(mapNum).humans = S.aliveHumans.getValue().intValue();
			Player.Instance().stats.get(mapNum).score = totalScore;
			
			int star = 1;
			if(S.aliveHumans.getValue() >= max) 
				star = 3;
			else if(S.aliveHumans.getValue() >= min)
				star = 2;
			
			Player.Instance().stats.get(mapNum).stars = star;
		}
			
		if(mapNum+1 < S.numberOfMaps)
			Player.Instance().stats.get(mapNum+1).lock = false;

		//Unlock tiers
		if(mapNum == 1 && Player.Instance().tierLock == 1)
		{
			Player.Instance().tierLock = 2;
			((WinUnlocks)Settings.Instance().Game.WinUnlocks).unlocked = true;
		}
		else if(mapNum == 6 && Player.Instance().tierLock == 2)
		{
			Player.Instance().tierLock = 3;
			((WinUnlocks)Settings.Instance().Game.WinUnlocks).unlocked = true;
		}
		
		Player.Instance().SaveFile();
		
		animhumanScore = new MutableLong(0);
		animgunScore = new MutableLong(0);
		animgoldScore = new MutableLong(0);
		animkillScore = new MutableLong(0);
		animtotalScore = new MutableLong(0);
		animhumans = new MutableLong(0);
		animguns = new MutableLong(0);
		curAnimVar = animhumans;
		
		animate = true;
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(S.isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(S.isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(S.isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (S.screenWidth - Graphics.TITLES.get(5).getWidth())/2;
		TitleY = 30*S.screenDensity;
		
		ButtonOK = new KeyAnimation();
		
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok, S.screenScaleMenu));
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok_pressed, S.screenScaleMenu));
		float x = (S.screenWidth - ButtonOK.getImgWidth(0))/2;
		float y = (S.screenHeight - ButtonOK.getImgHeight(0)*1.3f);
		ButtonOK.SetLocation(x, y);
		
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		//OK
		if(Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{	
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.WinUnlocks);
		}
		
		ButtonOK.ChangeFrame(0);
	}
	
	public void HandleTouchPress(float x, float y)
	{
		if(Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{
			ButtonOK.ChangeFrame(1);
		}
		
		Draw = true;
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.WinUnlocks);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		Settings S = Settings.Instance();
		String str;
		//Clear canvas and draw background
		S.Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		
		S.Canvas.drawBitmap(Graphics.TITLES.get(5), TitleX, TitleY, paint);
		
		float Y = TitleY + Graphics.TITLES.get(5).getHeight() + 20*S.screenDensity;

		//Calculate and draw human score
		
		
		S.Canvas.drawText("Humans:", 30*S.screenDensity, Y+40*S.screenDensity, dataPaint);
		str = animhumans + "/" + S.totalHumans; //S.aliveHumans + "/" + S.totalHumans;
		S.Canvas.drawText(str, (S.screenWidth - dataPaint.measureText(str))/2, Y+40*S.screenDensity, dataPaint);
		str = "- " + animhumanScore;
		S.Canvas.drawText(str, S.screenWidth-dataPaint.measureText(str)-30*S.screenDensity, Y+40*S.screenDensity, dataPaint);
		
		//Calculate and draw gun score
		S.Canvas.drawText("Guns: ", 30*S.screenDensity, Y+40*2*S.screenDensity, dataPaint);
		str =   animguns + "/" + S.totalGuns;//S.deployedGuns + "/" + S.totalGuns;
		S.Canvas.drawText(str, (S.screenWidth - dataPaint.measureText(str))/2, Y+40*2*S.screenDensity, dataPaint);
		str = "+ " + animgunScore.getValue();
		S.Canvas.drawText(str, S.screenWidth-dataPaint.measureText(str)-30*S.screenDensity, Y+40*2*S.screenDensity, dataPaint);
		
		//Calculate and draw gold score
		
		S.Canvas.drawText("Gold: ", 30*S.screenDensity, Y+40*3*S.screenDensity, dataPaint);
		str = "" + animgoldScore;
		S.Canvas.drawText(str, (S.screenWidth - dataPaint.measureText(str))/2, Y+40*3*S.screenDensity, dataPaint);
		str = "+ " + animgoldScore;
		S.Canvas.drawText(str, S.screenWidth-dataPaint.measureText(str)-30*S.screenDensity, Y+40*3*S.screenDensity, dataPaint);
		
		//Calculate and draw killing score
		
		S.Canvas.drawText("Kill Score: ", 30*S.screenDensity, Y+40*4*S.screenDensity, dataPaint);
		str =  animkillScore + "";
		S.Canvas.drawText(str, (S.screenWidth - dataPaint.measureText(str))/2, Y+40*4*S.screenDensity, dataPaint);
		

		
		S.Canvas.drawText("Total: ", 30*S.screenDensity, Y+40*7*S.screenDensity, dataPaint_Total);
		str =  "" + animtotalScore;
		S.Canvas.drawText(str, (S.screenWidth - dataPaint.measureText(str))/2, Y+40*7*S.screenDensity, dataPaint_Total);
		
		ButtonOK.Draw();
		
		//Draw into phone canvas.
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
		if(!animate) return;
		
		int increment = Math.round(Settings.Instance().aliveHumans.getValue()*400/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Humans
		if(curAnimVar == animhumans)
			if(curAnimVar.getValue() + increment < Settings.Instance().aliveHumans.getValue())
				curAnimVar.setValue(curAnimVar.getValue()+increment);
			else
			{
				curAnimVar.setValue(Settings.Instance().aliveHumans.getValue());
				curAnimVar = animhumanScore;
			}
		
		increment = Math.round(humanScore/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Human score
		if(curAnimVar == animhumanScore)
			if(curAnimVar.getValue() + 3 < humanScore)
				curAnimVar.setValue(curAnimVar.getValue()+3);
			else
			{
				curAnimVar.setValue(humanScore);
				curAnimVar = animguns;
			}
		
		increment = Math.round(Settings.Instance().deployedGuns.getValue()*50/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Guns 
		if(curAnimVar == animguns)
			if(curAnimVar.getValue() + increment < Settings.Instance().deployedGuns.getValue())
				curAnimVar.setValue(curAnimVar.getValue()+increment);
			else
			{
				curAnimVar.setValue(Settings.Instance().deployedGuns.getValue());
				curAnimVar = animgunScore;
			}
		
		
		increment = Math.round(gunScore/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Guns score
		if(curAnimVar == animgunScore)
			if(curAnimVar.getValue() + increment < gunScore)
				curAnimVar.setValue(curAnimVar.getValue() + increment);
			else
			{
				curAnimVar.setValue(gunScore);
				curAnimVar = animgoldScore;
			}
		
		increment = Math.round(goldScore/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Gold
		if(curAnimVar == animgoldScore)
			if(curAnimVar.getValue() + increment < goldScore)
				curAnimVar.setValue(curAnimVar.getValue()+ increment);
			else
			{
				curAnimVar.setValue(goldScore);
				curAnimVar = animkillScore;
			}
		
		
		increment = Math.round(killScore/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Kill score
		if(curAnimVar == animkillScore)
			if(curAnimVar.getValue() + increment < killScore)
				curAnimVar.setValue(curAnimVar.getValue() + increment);
			else
			{
				curAnimVar.setValue(killScore);
				curAnimVar = animtotalScore;
			}
		
		increment = Math.round(totalScore/(Common.FPS_EQUALTOSECOND*2));
		if(increment < 1) increment = 1;
		//Total score
		if(curAnimVar == animtotalScore)
			if(curAnimVar.getValue() + increment < totalScore)
				curAnimVar.setValue(curAnimVar.getValue() + increment);
			else
			{
				curAnimVar.setValue(totalScore);
				animate = false;
			}
		
		Draw = true;
	}
};








//
//========================================================== Win ============================================
//
//This state is drawn when all monsters die. In other words, when you win.
class WinUnlocks extends BaseWorld
{
	private Paint paint = new Paint();
	private Paint dataPaint = new Paint();
	private Paint starPassive = new Paint();
	
	//Postition to draw background and title
	private float BGx, TitleY;
	private float BGy, TitleX;
	
	private KeyAnimation star1, star2, star3, curStar;
	private KeyAnimation ButtonOK;
	
	private boolean animate = false;
	public volatile boolean unlocked;
	private float scale;
	private int opacity;
	
	private int mapNum;
	
	WinUnlocks()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		dataPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		dataPaint.setAntiAlias(true);
		dataPaint.setColor(0xFF81ecff);
		dataPaint.setTextSize(20);
		
		starPassive.setAlpha((int)(255*0.5));
	}
	
	public void Initialize()
	{
		mapNum = ((World)Settings.Instance().Game.World).getMapNumber();
		
		scale = 6;
		opacity = 0;
		
		star1 = new KeyAnimation(true, false);
		star1.AddBitmap(Graphics.COMMON.get(2));
		star1.AddBitmap(Graphics.COMMON.get(3));
		star1.SetLocation(22*Settings.Instance().screenDensity+70*Settings.Instance().screenDensity, 282*Settings.Instance().screenDensity);
		star1.SetOpacity(opacity);
		star1.SetScale(scale);
		
		star2 = new KeyAnimation(true, false);
		star2.AddBitmap(Graphics.COMMON.get(2));
		star2.AddBitmap(Graphics.COMMON.get(3));
		star2.SetLocation(22*Settings.Instance().screenDensity+70*2*Settings.Instance().screenDensity, 282*Settings.Instance().screenDensity);
		star2.SetOpacity(opacity);
		star2.SetScale(scale);
		
		if(Player.Instance().stats.get(mapNum).stars < 2)
			star2.ChangeFrame(1);
		
		star3 = new KeyAnimation(true, false);
		star3.AddBitmap(Graphics.COMMON.get(2));
		star3.AddBitmap(Graphics.COMMON.get(3));
		star3.SetLocation(22*Settings.Instance().screenDensity+70*3*Settings.Instance().screenDensity, 282*Settings.Instance().screenDensity);
		star3.SetOpacity(opacity);
		star3.SetScale(scale);
		
		if(Player.Instance().stats.get(mapNum).stars < 3)
			star3.ChangeFrame(1);
		
		curStar = star1;
		
		animate = true;
		
		if(Initialised) return;
		
		//Set Background position
		BGx = 0;
		if(Settings.Instance().isDisplayResolution(240, 320))
			BGy = -54; //108/2
		else if(Settings.Instance().isDisplayResolution(320, 480))
			BGy = -45; //90/2
		else if(Settings.Instance().isDisplayResolution(480, 800))
			BGy = -27;	//54/2
		else 
			BGy = 0;
		
		//Set title position
		TitleX = (Settings.Instance().screenWidth - Graphics.TITLES.get(6).getWidth())/2;
		TitleY = 30*Settings.Instance().screenDensity;
		
		ButtonOK = new KeyAnimation();
		
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok, Settings.Instance().screenScaleMenu));
		ButtonOK.AddBitmap(Graphics.loadImg(R.drawable.bg_button_ok_pressed, Settings.Instance().screenScaleMenu));
		float x = (Settings.Instance().screenWidth - ButtonOK.getImgWidth(0))/2;
		float y = (Settings.Instance().screenHeight - ButtonOK.getImgHeight(0)*1.3f);
		ButtonOK.SetLocation(x, y);
		
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
		//OK
		if(!animate && Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{	
			if(mapNum == 9 && (Player.Instance().noticeFlags & Common.FLAG_NOTICE7) == 0)
			{
				Settings S = Settings.Instance();
				Notice notice = (Notice)S.Game.Notice;
				Player P = Player.Instance();
				
				
				P.noticeFlags = (byte)(P.noticeFlags | Common.FLAG_NOTICE7);
				P.SaveFile();
				
				notice.setupStates(this, S.Game.MapSelect, true, false);
				notice.Text("Finally everything is over: humans are alive, monsters are dead. Leader of those creatures told us the reason for attacking us. As it seems, there was a world government cover-up.");
				notice.Text("Long time ago, monsters and humans lived together. These so called monsters are actually people, but mutated people. Some fat, angry, rich bastard decided that they look too ugly to be people, so he made them disappear (repressed them to live in caves by threatening) and rewrote the history. This whole situation made underground people angry and disgusted with the surface people thus leading to a war.");
				TransactionManager.Instance().doSimple(notice);
			}
			else
				TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.MapSelect);
		}
		
		ButtonOK.ChangeFrame(0);
	}
	
	public void HandleTouchPress(float x, float y)
	{
		if(!animate && Common.isInBounds2(ButtonOK.getLocation().x, ButtonOK.getLocation().y, ButtonOK.getImgWidth(0), ButtonOK.getImgHeight(0), x, y))
		{
			ButtonOK.ChangeFrame(1);
		}
		
		Draw = true;
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			if(!animate) TransactionManager.Instance().doTransaction(this, Settings.Instance().Game.MapSelect);
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		Settings S = Settings.Instance();
		//Clear canvas and draw background
		S.Canvas.drawBitmap(Graphics.MENU.get(0), BGx, BGy, paint);
		//Title: Unlocks
		S.Canvas.drawBitmap(Graphics.TITLES.get(6), TitleX, TitleY, paint);
		
		//Unlocks
		if(mapNum == 1 && unlocked)
		{
			S.Canvas.drawText("Upagrading Tier 1", (Settings.Instance().screenWidth-dataPaint.measureText("Upagrading Tier 1"))/2, 140, dataPaint);
			S.Canvas.drawText("Unlocked!", (Settings.Instance().screenWidth-dataPaint.measureText("Unlocked!"))/2, 170, dataPaint);
		}
		else if(mapNum == 6 && unlocked)
		{
			S.Canvas.drawText("Upagrading Tier 2", (Settings.Instance().screenWidth-dataPaint.measureText("Upagrading Tier 2"))/2, 140, dataPaint);
			S.Canvas.drawText("Unlocked!", (Settings.Instance().screenWidth-dataPaint.measureText("Unlocked!"))/2, 170, dataPaint);
		}
		
		//Background for holding stars
		S.Canvas.drawBitmap(Graphics.MENU.get(2), 0, 200*S.screenDensity, null);
		
		
		for(int i = 0; i < 3; i++)
			S.Canvas.drawBitmap(Graphics.COMMON.get(3), 60*S.screenDensity+70*i*S.screenDensity, 250*S.screenDensity, starPassive);
		
		star1.Draw();
		star2.Draw();
		star3.Draw();
		
		ButtonOK.Draw();
		
		//Draw into phone canvas.
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
		
		Draw = false;
	}

	//===================== Physics() =============================
	public void Physics()
	{
		if(!animate) return;
		
		if(opacity < 255) 
			if(opacity + 15 < 255) opacity+=15;
			else opacity = 255;
		
		if(!(scale <= 1))
			if(scale - 0.2f > 1) scale-=0.2f;
			else scale = 1;
		else
		{	
			if(curStar == star1)
			{
				opacity = 0;
				scale = 6;
				curStar = star2;
				Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_star);
			}
			else if(curStar == star2)
			{
				opacity = 0;
				scale = 6;
				curStar = star3;
				Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_star);
			}
			else
			{
				animate = false;
				Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_star);
			}
		}
			
		curStar.SetOpacity(opacity);
		curStar.SetScale(scale);
		
		Draw = true;
	}
};

