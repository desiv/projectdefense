/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */

package com.projdef.projectdefense;

import com.projdef.projectdefense.Utilities.FpsCounter;
import com.projdef.projectdefense.Utilities.Grid;
import com.projdef.projectdefense.Utilities.Sound;
import com.projdef.projectdefense.Manager.SpawnManager;
import com.projdef.projectdefense.Manager.EntityManager;
import java.util.Random;

import org.apache.commons.lang3.mutable.MutableLong;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

/**
 * 
 * @author Marius Savickas 
 * 
 *
 */
public class Settings
{	
	public boolean DEBUG = false;
	public volatile Bitmap BBuffer;
	public volatile Canvas Canvas;
	public volatile Context Context;
	public BitmapFactory.Options BitmapOptions;
	public Game Game;
	public ProjectDefenseActivity Activity;
	public Grid Grid;
	public SpawnManager SpawnManager;
	public EntityManager EntityManager;
	public Sound Sound;
	public Random RANDOM = new Random(19504446);
	
	public boolean AntiAlias = false;
	public boolean tierUnlocked = false;
	public boolean stopFPSCount = false;
	public boolean torchBuilt = false;
	public boolean turretBuilt = false;

	//Screen width and height
	public int screenWidth;
	public int screenHeight;
	public float screenDensity;
	public int screenDensityDpi;
	public float screenScaleSprites;
	public float screenScaleMap;
	public float screenScaleMenu;
	public float BGyOffset;
	
	public int numberOfMaps;
	
	//0 value is the default value defined by map data.
	public MutableLong GOLD 			= new MutableLong(0);
	public MutableLong aliveHumans 		= new MutableLong(0);
	public MutableLong killScore 		= new MutableLong(0);
	public MutableLong totalkillScore	= new MutableLong(0);
	public MutableLong KILLS 			= new MutableLong(0);
	public MutableLong deployedGuns 	= new MutableLong(0);
	public MutableLong totalGuns 		= new MutableLong(0);
	public MutableLong aliveZombies 	= new MutableLong(0);
	public MutableLong totalZombies 	= new MutableLong(0);
	public MutableLong totalHumans 		= new MutableLong(0);
	public MutableLong releasedZombies 	= new MutableLong(0);
	
	//Starting prices
	public int COST_BULLET_LEVEL_1;
	public int COST_FIRE_LEVEL_1;
	
	public int COST_BULLET_LEVEL_2;
	public int COST_BULLET_LEVEL_3;
	
	public int COST_ELECTR_LEVEL_2;
	public int COST_ELECTR_LEVEL_3;
	
	public int COST_LASER_LEVEL_2;
	public int COST_LASER_LEVEL_3;
	
	public int COST_FIRE_LEVEL_2;
	public int COST_FIRE_LEVEL_3;
	
	public int COST_POISON_LEVEL_2;
	public int COST_POISON_LEVEL_3;
	
	public int COST_FREEZE_LEVEL_2;
	public int COST_FREEZE_LEVEL_3;
	
	//Score per kill
	//Score PK = Initial value * level
	public int SCORE_PK_T;
	public int SCORE_PK_B;
	public int SCORE_PK_FT;
	public int SCORE_PK_FB;
	public int SCORE_PK_ST;
	public int SCORE_PK_SB;
	public int SCORE_PK_SPT;
	public int SCORE_PK_SPB;
	public int SCORE_PK_BOSS;
	
	//Area taken by monster
	public int ENTITY_ENEMY_DIAMETER;
	public int ENTITY_GUN_INPUT_BUILD_DISTANCE;
	public long ENTITY_TORCH_SHOT_DELAY;
	public int GOAL_REACH_DISTANCE;
	public int ENTITY_FIRE_DIE_DISTANCE;
	public int ENTITY_FIRE_SCALEANIMATION_DISTANCE;
	
	public int ENTITY_GUN_ROTATION_PREDICT_LOC = 10;
	public int ENTITY_GUN_TURRET_ROTATION_PREDICT_LOC = 7;
	
	
	public int SPAWN_DELAY_FIRST;
	public int SPAWN_DELAY_WAVE;
	public int SPAWN_DELAY_FOE;
	
	public boolean isDisplayResolution(int x, int y)
	{
		if(x == screenWidth && y == screenHeight)
			return true;
		
		return false;
	}
	
	public void CostIncrease(int increase)
	{
		Settings.Instance().COST_BULLET_LEVEL_1 	+= increase;
		Settings.Instance().COST_FIRE_LEVEL_1 		+= increase;
		
		Settings.Instance().COST_BULLET_LEVEL_2		+= increase;
		Settings.Instance().COST_BULLET_LEVEL_3		+= increase;
		
		Settings.Instance().COST_ELECTR_LEVEL_2		+= increase;
		Settings.Instance().COST_ELECTR_LEVEL_3		+= increase;
		
		Settings.Instance().COST_LASER_LEVEL_2		+= increase;
		Settings.Instance().COST_LASER_LEVEL_3		+= increase;
		
		Settings.Instance().COST_FIRE_LEVEL_2		+= increase;
		Settings.Instance().COST_FIRE_LEVEL_3		+= increase;
		
		Settings.Instance().COST_POISON_LEVEL_2		+= increase;
		Settings.Instance().COST_POISON_LEVEL_3		+= increase;
		
		Settings.Instance().COST_FREEZE_LEVEL_2		+= increase;
		Settings.Instance().COST_FREEZE_LEVEL_3		+= increase;
	}
	
	public int getRandom(int range)
	{
		RANDOM.setSeed(FpsCounter.Instance().getFpsCount());
		return RANDOM.nextInt(range);
	}
	
	//Singleton holder
	private static class SingletonHolder 
	{
		public static final Settings instance = new Settings();
	}
	
	public static Settings Instance()
	{
		return SingletonHolder.instance;
	}
};