/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.view.KeyEvent;

import com.projdef.projectdefense.Base.BaseWorld;

/**
 * Class to animate transactions between states
 * @author Marius Savickas
 *
 */
public class Transaction extends BaseWorld
{
	private Paint paint = new Paint();
	private BaseWorld prevState;
	private BaseWorld nextState;
	private Bitmap prevBuffer;
	private Bitmap nextBuffer;
	private int opacity = 255;
	private int tranState = 0;
	private boolean isSet = false;
	
	Transaction()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		paint.setAlpha(255);
	}
	
	public void setNextState(BaseWorld prev, BaseWorld next)
	{
		prevState = prev;
		nextState = next;
		isSet = true;
	}
	
	public void Initialize()
	{		
		opacity = 255;	
		tranState = 0;
		
		prevBuffer = Bitmap.createBitmap(Settings.Instance().screenWidth, Settings.Instance().screenHeight, Config.ARGB_8888);
		nextBuffer = Bitmap.createBitmap(Settings.Instance().screenWidth, Settings.Instance().screenHeight, Config.ARGB_8888);
		
		//Render previous state
		prevState.Draw = true;
		prevState.Draw(new Canvas(prevBuffer));
		prevState.Draw = true;
		
		Initialised = true;
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		prevState = null;
		nextState = null;
		Initialised = false;
		isSet = false;
		opacity = 255;
		tranState = 0;
		prevBuffer = null;
		nextBuffer = null;
	}
	
	//================= HandleInput() =============================
	public void HandleTouchRelease(float x, float y)
	{
	}
	
	public void HandleTouchPress(float x, float y)
	{
	}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			return true;
	    }
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		return true;
	}
	
	//======================== Draw() =============================	
	public void Draw(Canvas canvas)
	{
		if(!isSet || !Initialised) return;
		//Fill all screen with black
		Settings.Instance().Canvas.drawColor(Color.BLACK);
		
		//Draw bitmap
		if(tranState == 0)Settings.Instance().Canvas.drawBitmap(prevBuffer ,0, 0, paint);
		else Settings.Instance().Canvas.drawBitmap(nextBuffer, 0, 0, paint);
		
		//Draw into phone canvas.
		canvas.drawBitmap(Settings.Instance().BBuffer, 0, 0, null);
	}

	//===================== Physics() =============================
	public void Physics()
	{
		//fade out
		if(tranState == 0)
		{
			if(opacity != 0) opacity -= 30;
			//Update pixels with new opacity
			if(opacity == 0) 
			{	
				paint.setAlpha(0);
				tranState = 1;
				
				//Prepare next state in the black screen
				//Reset draw flag
				nextState.Draw = true;
				//Initialize next state
				nextState.Initialize();
				//Render next screen into next state's buffer
				nextState.Draw(new Canvas(nextBuffer));
				//Reset it again
				nextState.Draw = true;
			}
			//This will give additional dark frame, making transaction smoother on process intense transactions
			else if(opacity < 0)
			{
				opacity = 0;
				paint.setAlpha(opacity);
			}
			//Update pixels with new opacity
			else paint.setAlpha(opacity);
		}
		//fade in
		else if(tranState == 1)
		{
			opacity += 30;
			//If it's full opacity
			if(opacity >= 255)
			{
				//Update opacity
				paint.setAlpha(255);
				//Change state
				Settings.Instance().Game.ChangeState(nextState);
			}
			//Update opacity
			else paint.setAlpha(opacity);
		}
	}
};