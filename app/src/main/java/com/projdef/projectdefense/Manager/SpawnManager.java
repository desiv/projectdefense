/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Manager;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Wave;

public class SpawnManager
{
	private World World;
	
	private Wave WaveArray[];
	public int indexWave = 0;
	public int numberOfWaves = 0;
	public int indexFoes = 0;
	
	private boolean spawnFirst;
	private boolean spawnLast;
	private boolean spawnNextWave;
	private Timer spawnDelayFirst = new Timer();
	private Timer spawnDelayWave = new Timer();
	private Timer spawnDelayFoe = new Timer();
	
	public void Initialize(World w, int numberOfWaves, Wave wave[])
	{
		indexWave = 0;
		indexFoes = 0;
		
		World = w;
		WaveArray = new Wave[numberOfWaves];
		this.numberOfWaves = numberOfWaves;
		WaveArray = wave;
		
		spawnFirst = false;
		spawnLast = false;
		spawnNextWave = false;
		
		spawnDelayFirst.updateFpsStamp();
		spawnDelayWave.updateFpsStamp();
		spawnDelayFoe.updateFpsStamp();
	}
	
	
	public void Unitilaize()
	{
		World = null;
		
		spawnDelayFirst.releaseMemory(); 
		spawnDelayFirst = null;
		
		spawnDelayWave.releaseMemory();
		spawnDelayWave = null;
		
		spawnDelayFoe.releaseMemory();
		spawnDelayFoe = null;
	}
	
	//Handles spawns
	public void onSpawn()
	{
		//Delay to first spawn
		if(!spawnDelayFirst.ifFpsPassed(Settings.Instance().SPAWN_DELAY_FIRST) &&  spawnFirst == false || spawnLast == true)
			return;
		
		if(!spawnFirst) OnScreenMsgManager.Instance().CreateMessageQueue("Monsters are coming!", 2, 14*Settings.Instance().screenDensity);
		
		//First monster has spawned. If that's the case - we don't need to delay beginning
		spawnFirst = true;
		
		//Delay between spawns
		if(!spawnDelayWave.ifFpsPassed(Settings.Instance().SPAWN_DELAY_WAVE) && spawnNextWave == true)
			return;
		
		//Delay has passed and we would like to proceed with the monsters
		spawnNextWave = false;
			
		//Delay between monsters
		if(spawnDelayFoe.ifFpsPassed(Settings.Instance().SPAWN_DELAY_FOE) && spawnLast == false)
		{
			World.CreateEnemies(WaveArray[indexWave].Foes[indexFoes].level, WaveArray[indexWave].Foes[indexFoes].type);
			//Advance to the next foe
			indexFoes++;
			
			//If we have reached our final foe
			if(indexFoes >= WaveArray[indexWave].numberOfFoes)
			{
				//Advance to the next wave
				indexWave++;
				//Reset foe index
				indexFoes = 0;
				
				//If this is the last wave
				if(indexWave >= numberOfWaves)
				{
					spawnLast = true;
					return;
				}
				
				//Monsters have ended, next wave is coming
				spawnNextWave = true;
				spawnDelayWave.updateFpsStamp();
			}
			
		}
		
	}
	
	public void reset()
	{
		indexWave = 0;
		indexFoes = 0;
		
		spawnFirst = false;
		spawnLast = false;
		spawnNextWave = false;
		
		spawnDelayFirst.updateFpsStamp();
		spawnDelayWave.updateFpsStamp();
		spawnDelayFoe.updateFpsStamp();
	}
};