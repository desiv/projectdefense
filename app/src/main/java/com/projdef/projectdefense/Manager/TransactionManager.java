/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Manager;

import java.util.LinkedList;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Base.BaseWorld;
import com.projdef.projectdefense.Transaction;

/**
 * Class to manage transactions between states
 * @author Marius Savickas
 *
 */
public class TransactionManager
{
	LinkedList<States> queue = new LinkedList<States>();
	
	public boolean Process()
	{
		if(queue.isEmpty()) return false;
		else
		{
			States S = queue.getFirst();
			
			if(S.trans)
			{
				((Transaction)Settings.Instance().Game.Transaction).setNextState(S.prev, S.next);
				Settings.Instance().Game.ChangeState(Settings.Instance().Game.Transaction);
			}
			else
			{
				Settings.Instance().Game.ChangeState(S.next);
			}
			
			queue.removeFirst();
			return true;
		}

	}
	
	public void reset()
	{
		queue.clear();
	}
	
	public void doTransaction(BaseWorld prev, BaseWorld next)
	{
		queue.add(new States(prev, next, true));
	}
	
	public void doSimple(BaseWorld next)
	{
		queue.add(new States(null, next, false));
	}
	
	/**
	 * Structure to hold states
	 */
	private class States
	{
		public BaseWorld prev;
		public BaseWorld next;
		boolean trans;
		
		public States(BaseWorld prev, BaseWorld next, boolean transactions)
		{
			this.prev = prev;
			this.next = next;
			this.trans = transactions;
		}
	}
	
	/*
	 * Singleton
	 */
	private static class SingletonHolder
	{
		private static final TransactionManager instance = new TransactionManager();
	}
	
	public static TransactionManager Instance()
	{
		return SingletonHolder.instance;
	}
}