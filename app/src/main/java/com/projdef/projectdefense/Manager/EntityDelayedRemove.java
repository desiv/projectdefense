/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Manager;

/*
 * This Class is used to remove Entities without crashing the game. Acts like a delayed removal.
 */

import java.util.ArrayList;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;
import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Entity.Lightning;

public class EntityDelayedRemove
{
	private ArrayList<BaseEntity> ListEntities = new ArrayList<BaseEntity>();
	private ArrayList<Lightning> ListLightning = new ArrayList<Lightning>();
	
	private EntityDelayedRemove() {}
	
	//Initialize singleton
	private static class SingletonHolder
	{
		public static final EntityDelayedRemove instance = new EntityDelayedRemove();
	}
	
	public static EntityDelayedRemove Instance()
	{
		return SingletonHolder.instance;
	}
	
	/**
	 * Executes removal of Entities.
	 * @param W Instance of world
	 */
	public void Process(World W)
	{
		for(BaseEntity B: ListEntities)
		{			
			String className = B.getClass().getSimpleName();
			if(className.contentEquals("Fire"))
			{
				B.releaseMemory();
				W.FireList.remove(B);
				//EntityManager.Instance().Unregister(B);
			}
			else if(className.contentEquals("Bullet"))
			{
				B.releaseMemory();
				W.BulletList.remove(B);
				//EntityManager.Instance().Unregister(B);
			}
			else if(className.contentEquals("Enemy"))
			{
				B.releaseMemory();
				W.EnemyList.remove(B);
				//Remove Manager List
				Settings.Instance().EntityManager.Unregister(B);
			}
			else 
			{
				B.releaseMemory();
				W.GunList.remove(B);
				//Remove Manager List
				Settings.Instance().EntityManager.Unregister(B);
			}
			
			
			
		}
		
		
		for(Lightning L: ListLightning)
		{
			W.LightningList.remove(L);
		}
		
		ListEntities.clear();
		ListLightning.clear();
	}
	
	/**
	 * Add Entity to the list for later removal.
	 * @param b Entity
	 */
	public void Remove(BaseEntity b)
	{
		ListEntities.add(b);
	}
	
	public void Remove(Lightning l)
	{
		ListLightning.add(l);
	}
	
};