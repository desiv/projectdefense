/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Manager;

import java.util.ArrayList;
import java.util.LinkedList;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Utilities.Common;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;

public class OnScreenMsgManager
{
	private ArrayList<WORDS> listWords = new ArrayList<WORDS>();
	private ArrayList<WORDS> removelist = new ArrayList<WORDS>();
	
	private LinkedList<WORDS> queueWordsList = new LinkedList<WORDS>();
	WORDS queueWord = null;
	
	/**
	 * Processes messages
	 */
	public void Process()
	{
		/*
		 * Process queued messages
		 */
		if(queueWord == null && !queueWordsList.isEmpty())
		{
			//Get next for processing
			queueWord = queueWordsList.getFirst();
			queueWordsList.removeFirst();
		}
		
		if(queueWord != null)
		{
			//Reduce time on Messages
			if(queueWord.animationState == 0)
			{
				queueWord.alpha+=40;
				
				if(queueWord.alpha >= 255)
				{
					queueWord.alpha = 255;
					queueWord.paint.setAlpha(queueWord.alpha);
					queueWord.animationState = 1;
				}
				else queueWord.paint.setAlpha(queueWord.alpha);
			}
			else if(queueWord.animationState == 1)
			{
				queueWord.displayTime--;
				if(queueWord.displayTime <= 0) queueWord.animationState = 2;
			}
			else if(queueWord.animationState == 2)
			{
				queueWord.alpha-=15;
				//Set to remove words when the opacity is lower than 0
				if(queueWord.alpha <= 0)
				{
					queueWord.alpha = 0;
					queueWord.paint.setAlpha(queueWord.alpha);
					queueWord = null;
				}
				else queueWord.paint.setAlpha(queueWord.alpha);
			}
		}

		
		/*
		 * Process all messages
		 */
		//Remove ceased messages
		for(WORDS w: removelist)
		{
			listWords.remove(w);
		}
		
		removelist.clear();
		
		//Reduce time on Messages
		for(WORDS w: listWords)
		{
			if(w.animationState == 0)
			{
				w.alpha+=40;
				
				if(w.alpha >= 255)
				{
					w.alpha = 255;
					w.paint.setAlpha(w.alpha);
					w.animationState = 1;
				}
				else w.paint.setAlpha(w.alpha);
			}
			else if(w.animationState == 1)
			{
				w.displayTime--;
				if(w.displayTime <= 0) w.animationState = 2;
			}
			else if(w.animationState == 2)
			{
				w.alpha-=15;
				//Set to remove words when the opacity is lower than 0
				if(w.alpha <= 0)
				{
					w.alpha = 0;
					w.paint.setAlpha(w.alpha);
					removelist.add(w);
				}
				else w.paint.setAlpha(w.alpha);
			}
		}
	}
	
	/**
	 * Draws messages
	 */
	public void Draw()
	{
		if(queueWord != null)
			Settings.Instance().Canvas.drawBitmap(queueWord.words, queueWord.x, queueWord.y, queueWord.paint);
		
		for(WORDS w:listWords)
		{
			Settings.Instance().Canvas.drawBitmap(w.words, w.x, w.y, w.paint);
		}
	}
	
	/**
	 * Creates a message to be displayed on screen
	 * @param words - text to be displayed
	 * @param displayTime - time to be displayed in seconds
	 */
	public void CreateMessage(String words, int displayTime, float scale)
	{	
		WORDS w = new WORDS();
		w.words = MakeBitmap(words, scale);
		w.displayTime = displayTime*Common.FPS_EQUALTOSECOND;
		
		w.paint = new Paint();
		w.paint.setAlpha(0);
		w.alpha = w.paint.getAlpha();
		
		int lenght = w.words.getWidth();
		
		w.x = (Settings.Instance().screenWidth - lenght)/2;
		w.y = 140;
		w.animationState = 0;
		listWords.add(w);
	}
	
	
	/**
	 * Creates a message to be displayed on screen
	 * @param words - text to be displayed
	 * @param displayTime - time to be displayed in seconds
	 */
	public void CreateMessageQueue(String words, int displayTime, float scale)
	{	
		WORDS w = new WORDS();
		w.words = MakeBitmap(words, scale);
		w.displayTime = displayTime*Common.FPS_EQUALTOSECOND;
		
		w.paint = new Paint();
		w.paint.setAlpha(0);
		w.alpha = w.paint.getAlpha();
		
		int lenght = w.words.getWidth();
		
		w.x = (Settings.Instance().screenWidth - lenght)/2;
		w.y = 140*Settings.Instance().screenDensity + Settings.Instance().BGyOffset;
		w.animationState = 0;
		queueWordsList.add(w);
	}
	
	/**
	 * Creates a message to be displayed on screen with arbitrary coordinates
	 * @param words	- text to be displayed
	 * @param x
	 * @param y
	 * @param displayTime - time to be displayed
	 * @param paint	- style
	 */
	public void CreateMessage(String words, float x, float y, int displayTime, Paint paint)
	{	
		WORDS w = new WORDS();
		w.words = MakeBitmap(words, 1);
		w.displayTime = displayTime;
		
		//Adjust text size for different screen densities
		w.paint = new Paint(paint);
		w.paint.setTextSize(w.paint.getTextSize()*Settings.Instance().screenDensity);
		//Set hidden for animation
		w.paint.setAlpha(0);
		w.alpha = w.paint.getAlpha();
		w.x = x;
		w.y = y;
		w.animationState = 0;
		listWords.add(w);
	}
	
	public Bitmap MakeBitmap(String str, float size)
	{
		int width = 0;
		int height = 0;
		int spaceWidth = (int)(Graphics.MAP_ALPHABET.get(0).getWidth()/2);
		
		//Count width of the bitmap
		for(int i = 0; i < str.length(); i++)
		{
			char letter = str.charAt(i);
			int index = (int)letter;
			//Numbers
			if((int)letter >= 48 && (int)letter <= 57)
			{
				index-=48;
			}
			
			//Colon
			else if((int)letter == 58) index = 10;
			//Exclamation mark
			else if((int)letter == 33) index = 11;
			//Question mark
			else if((int)letter == 63) index = 12;
			//Letters: lowercase
			else if((int)letter >= 97 && (int)letter <= 122)
			{
				index = (index - 97) + 13;
			}
			//Letters: uppercase
			else if((int)letter >= 65 && (int)letter <= 90)
			{
				index = (index - 65) + 13;
			}
			else index = 0;
			
			//Space
			if((int)letter == 32) width+=spaceWidth;
			else
			{
				width += Graphics.MAP_ALPHABET.get(index).getWidth();
				if(height < Graphics.MAP_ALPHABET.get(index).getHeight())
					height = Graphics.MAP_ALPHABET.get(index).getHeight();
			}
		}
		
		
		Bitmap b_out = Bitmap.createBitmap(width,  height,  Config.ARGB_8888);
		Canvas canvas = new Canvas(b_out);
		
		
		int X = 0;
		//Draw letters
		for(int i = 0; i < str.length(); i++)
		{
			char letter = str.charAt(i);
			int index = (int)letter;
			//Numbers
			if((int)letter >= 48 && (int)letter <= 57)
			{
				index-=48;
			}
			
			//Colon
			else if((int)letter == 58) index = 10;
			//Exclamation mark
			else if((int)letter == 33) index = 11;
			//Question mark
			else if((int)letter == 63) index = 12;
			//Letters: lowercase
			else if((int)letter >= 97 && (int)letter <= 122)
			{
				index = (index - 97) + 13;
			}
			//Letters: uppercase
			else if((int)letter >= 65 && (int)letter <= 90)
			{
				index = (index - 65) + 13;
			}
			else index = 0;
			
			if((int)letter == 32) X+=spaceWidth;
			else
			{
				//Handle for a colon
				if((int)letter == 58) canvas.drawBitmap(Graphics.MAP_ALPHABET.get(index), X,  (height - Graphics.MAP_ALPHABET.get(index).getHeight())/2, null);
				//Draw normally
				else canvas.drawBitmap(Graphics.MAP_ALPHABET.get(index), X,  0, null);
				//Increase width
				X += Graphics.MAP_ALPHABET.get(index).getWidth();
			}
		}
		
		float scale = size*str.length()/b_out.getWidth();
		
		return Graphics.scaleImg(b_out, scale);
	}
	
	public void clear()
	{
		 listWords.clear();
		 removelist.clear();
		 queueWordsList.clear();
		 queueWord = null;
	}
	
	/*
	 * Singleton
	 */
	private static class Singleton
	{
		public static final OnScreenMsgManager instance = new OnScreenMsgManager();
	}
	
	public static OnScreenMsgManager Instance()
	{
		return Singleton.instance;
	}
}

/**
 * Container for words
 */
class WORDS
{
	public Bitmap words;
	public int displayTime, animationState, alpha; //animationState: 0 - appear, 1 - count time, 2 - disappear
	public Paint paint;
	public float x, y;
}