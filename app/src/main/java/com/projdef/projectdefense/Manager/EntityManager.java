/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */

package com.projdef.projectdefense.Manager;

import java.util.HashMap;

import android.annotation.SuppressLint;
import com.projdef.projectdefense.Base.BaseEntity;

public class EntityManager
{
	//Private constructor prevents instantiation from other classes
	public EntityManager(){}
	
	//Map of all the entities created
	@SuppressLint("UseSparseArrays")
	private HashMap<Integer, Object> entityMap = new HashMap<Integer, Object>(256);
	
	//Every entity is registered in the map
	public void Register(Object newEntity)
	{
		entityMap.put(((BaseEntity)newEntity).getID(), newEntity);
	}
	
	//Remove entity
	public void Unregister(Object entity)
	{
		entityMap.remove(((BaseEntity)entity).ID);
	}
	
	//Get entities by id
	public Object getEntityByID(int id)
	{
		return entityMap.get(id);
	}

	public void reset()
	{
		entityMap.clear();
	}
	
};