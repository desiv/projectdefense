/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.content.Context;

import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.FileParser;
import com.projdef.projectdefense.Utilities.FileWriter;


/**
 * Class for keeping players achievements
 * @author Marius Savickas
 *
 * player.dat file structure (numbers represent bytes):
 * 
 * 1 tier
 * 1 number of maps
 * 
 * *** MAP structure ***
 * 1 lock flag
 * 1 stars
 * 4 score
 * 
 * *** END of iterating maps
 * 
 * 1 notice flags (goes in ascending order: from least significant bit to most significant bit)
 * 1 tutorial flags (goes in ascending order: from least significant bit to most significant bit)
 * 1 sound flag
 */
public class Player
{
	//Values: 1 - only building is available, 2 - 1st upgrade available, 3 - everything is available
	public int tierLock = 0;
	public int mapQuantity = 0;
	public byte noticeFlags = 0;
	public byte tutorialFlags = 0;
	public byte soundFlag = 0;
	public ArrayList<MapStats> stats = new ArrayList<MapStats>();
	private String name;
	
	public boolean CreateFile(String name)
	{
		reset();
		this.name = name;
		tierLock = 1;
		mapQuantity = Settings.Instance().numberOfMaps;
		
		//First map unlocked
		stats.add(new MapStats(false, 0, 0, 0));
		for(int i = 1; i < mapQuantity; i++)
		{
			stats.add(new MapStats(true, 0, 0, 0));
		}
		
		noticeFlags = 0;
		tutorialFlags = 0;
		soundFlag = 0x01;
		
		SaveFile();
		
		return true;
	}
	
	public boolean SaveFile()
	{
		byte[] buffer = null;
		OutputStream oStream = null;

		FileWriter Writer = new FileWriter();
		
		Writer.WriteByte((byte)tierLock);
		Writer.WriteByte((byte)mapQuantity);
		
		for(int i = 0; i < mapQuantity; i++)
		{
			int lockFlag = Common.FALSE;
			if(stats.get(i).lock) lockFlag = Common.TRUE;
			
			Writer.WriteByte((byte)lockFlag);
			Writer.WriteByte((byte)stats.get(i).stars);
			Writer.WriteByte((byte)stats.get(i).humans);
			Writer.WriteDWord((int)stats.get(i).score);
		}
		
		//Notice bytes
		Writer.WriteByte(noticeFlags);
		Writer.WriteByte(tutorialFlags);
		Writer.WriteByte(soundFlag);
		
		buffer = Writer.getBuffer();
		
		//Create file
		try {
			
			oStream = Settings.Instance().Context.openFileOutput(name, Context.MODE_PRIVATE);
			oStream.write(buffer);
			
		} catch (IOException e) {
			e.printStackTrace();
			Settings.Instance().Activity.finish();
		}
		
		
		return true;
	}
	
	
	public boolean SaveFile(Context context)
	{
		byte[] buffer = null;
		OutputStream oStream = null;

		FileWriter Writer = new FileWriter();
		
		Writer.WriteByte((byte)tierLock);
		Writer.WriteByte((byte)mapQuantity);
		
		for(int i = 0; i < mapQuantity; i++)
		{
			int lockFlag = Common.FALSE;
			if(stats.get(i).lock) lockFlag = Common.TRUE;
			
			Writer.WriteByte((byte)lockFlag);
			Writer.WriteByte((byte)stats.get(i).stars);
			Writer.WriteByte((byte)stats.get(i).humans);
			Writer.WriteDWord((int)stats.get(i).score);
		}
		//Notice bytes
		Writer.WriteByte(noticeFlags);
		Writer.WriteByte(tutorialFlags);
		Writer.WriteByte(soundFlag);
		
		buffer = Writer.getBuffer();
		
		//Create file
		try {
			
			oStream = context.openFileOutput(name, Context.MODE_PRIVATE);
			oStream.write(buffer);
			
		} catch (IOException e) {
			e.printStackTrace();
			Settings.Instance().Activity.finish();
		}
		
		return true;
	}
	
	public boolean LoadFile(String name)
	{
		reset();
		this.name = name;
		
		byte[] buffer = null;
		InputStream iStream = null;
		
		//Open file
		try {
			iStream = Settings.Instance().Context.openFileInput(name);
		} catch (FileNotFoundException e) {
			return false;
		}
		
		//Get bytes
		try{
			buffer = new byte[iStream.available()];
			iStream.read(buffer);
		} catch(IOException e){
			e.printStackTrace();
			Settings.Instance().Activity.finish();
		}
		
		//Check if it got loaded
		if(buffer.length <= 0) return false;
		
		//Prepare reader
		FileParser Reader = new FileParser(buffer);
		
		//Which tiers are locked
		tierLock = Reader.ReadByte();
		mapQuantity = Reader.ReadByte();
						
		if(mapQuantity != Settings.Instance().numberOfMaps)
			return false;
				
		//Load map statistics
		for(int i = 0; i < mapQuantity; i++)
		{
			boolean lockFlag = false;
			byte stars = 0;
			byte humans = 0;
			long score = 0;
			
			if(Reader.ReadByte() == Common.TRUE)
				lockFlag = true;
			
			stars = (byte)Reader.ReadByte();
			humans = (byte)Reader.ReadByte();
			score = Reader.ReadDWord();
			
			stats.add(new MapStats(lockFlag, stars, humans, score));
		}
		
		noticeFlags = Reader.ReadByteSigned();
		tutorialFlags = Reader.ReadByteSigned();
		soundFlag = Reader.ReadByteSigned();
		
		return true;
	}
	
	public void reset()
	{
		tierLock = 1;
		mapQuantity = 0;
		stats.clear();
	}
	
	
	public class MapStats
	{
		public boolean lock;
		public int stars, humans;
		public long score;
		
		MapStats(boolean lock, int stars, int humans, long score)
		{
			this.lock = lock;
			this.stars = stars;
			this.humans = humans;
			this.score = score;
		}
	}
	
	//Singleton holder
	private static class SingletonHolder 
	{
		public static final Player instance = new Player();
	}
	
	public static Player Instance()
	{
		return SingletonHolder.instance;
	}
}