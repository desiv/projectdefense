/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Base;

import com.projdef.projectdefense.Utilities.Telegram;



//Interface for states
public abstract class BaseState<object>
{
	public abstract void Enter(object AI);
	public abstract void Execute(object AI);
	public abstract void Exit(object AI);
	public abstract boolean onMessage(object AI, Telegram msg);
};