/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Base;

import android.graphics.Canvas;
import android.view.KeyEvent;

public abstract class BaseWorld
{
	//Flags
	protected boolean Initialised = false;
	public boolean Draw = true;
	//Draw states screen
	public abstract void Draw(Canvas canvas);
	//Simulate states physics
	public abstract void Physics();
	public abstract void HandleTouchRelease(float x, float y);
	public abstract void HandleTouchPress(float x, float y);
	public abstract void HandleMovement(float x, float y);
	public abstract boolean HandlePhysicalKey(int keyCode, KeyEvent event);
	public abstract boolean HandleExit();
	//Function called on state enter
	public abstract void Initialize();
	//Called on changing state exit
	public abstract void Exit();
};