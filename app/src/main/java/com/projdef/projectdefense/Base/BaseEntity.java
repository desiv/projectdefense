/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Base;

import com.projdef.projectdefense.Graphics.Sprite;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.Telegram;

public abstract class BaseEntity extends Sprite
{
	protected static int nextUniqueValue = 0;
	public int ID = 0;
	protected int Level = 0;
	protected float PhyAtk = 0;
	protected float MagAtk = 0;
	protected float PhyDef = 0;
	protected float MagDef = 0;
	protected float Speed = 1;
	protected int Range = 0;
	protected int Cost = 0;
	protected long Score = 0;
	protected float HP = 0;
	protected float MaxHP = 0;
	protected boolean Resistant = false;
	protected boolean Alive = true;
	protected boolean AI = false;
	
	
	protected BaseEntity(){}
	
	public abstract void UpdateAI();
	public abstract void HandleMessage(Telegram msg);
	
	protected void setID() 
	{
		nextUniqueValue++;
		
		//Skip used IDs
		if(nextUniqueValue == Common.ENTITY_FIRE_ID) nextUniqueValue++;
		if(nextUniqueValue == Common.ENTITY_BULLET_ID) nextUniqueValue++;
		
		ID = nextUniqueValue;
	}
	
	public void setFireID() {ID = Common.ENTITY_FIRE_ID;}
	
	public void setBulletID() {ID = Common.ENTITY_BULLET_ID;}
	
	public int getID() {return ID;}
	
	public int getRange() {return Range;}
	
	public int getCost() {return Cost;}
	
	public int getLvl() {return Level;}
	
	public float getHP() {return HP;}
	
	public float getMaxHP() {return MaxHP;}
};