/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;


import com.projdef.projectdefense.Map;
import com.projdef.projectdefense.MessageDispatcher;
import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;
import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Base.BaseState;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.ExtraInfo;
import com.projdef.projectdefense.Utilities.Telegram;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Vector;

public class Gun extends BaseEntity
{
	World World;
	BaseState<Gun> AIcurrentState; //AI. Current state that gun is simulating
	Vector[] BuildingPositions;
	ArrayList<Enemy> enemiesNearList = new ArrayList<Enemy>(); //List of enemies that are near this entity
	ArrayList<Enemy> enemiesInCell = new ArrayList<Enemy>();
	Timer lastShot = new Timer();
	
	int Freeze		= 0;
	int Biological 	= 0;
	int Poison	 	= 0;
	int Electricity	= 0;;
	int ElectricChargeCount = 0;
	
	//Flames to be fired: Poison, Freeze, Fire
	int Flame1 		= 0;
	int Flame2 		= 0;
	
	//Turret/Torch
	int Type  		= 0;
	int TurretType  = 0;
	
	
	int FiringOffset = 0;
	
	int[] cells;
	
	public int soundStreamID;
	public boolean soundPlaying = false;
	protected boolean attacking = false;
	//================================== Constructor ==============================================
	public Gun(World world){World = world;}
	
	public void Initialize(Map map, int indexPos, int type)
	{
		//Generate ID
		setID();
		
		//Register entity
		Settings.Instance().EntityManager.Register(this);
		
		//Initialize gun settings
		Type 			= type;
		//Starting with level 1 and upgrade later to upper levels.
		Level 			= 1;
		
		//Loading map info
		BuildingPositions = map.getBuildingPos();
		
		//Initializing sprite
		switch(type)
		{
		case Common.ENTITY_TYPE_GUN_TURRET:
			CreateSprite(Graphics.GUN_TURRET_P_1,		//Bitmap array
						 0,								//Start of index
						 BuildingPositions[indexPos].x,
						 BuildingPositions[indexPos].y,
						 Graphics.GUN_TURRET_P_1.get(0).getWidth()/2,	//center x
						 Graphics.GUN_TURRET_P_1.get(0).getHeight()/2,	//center y
						 1f);						//scale
			TurretType = Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET;
			PhyAtk 		= 15f;
			MagAtk 		= 3f;
			Range 		= (int)(80*Settings.Instance().screenDensity);
			FiringOffset = Math.round(20*Settings.Instance().screenDensity);
			Settings.Instance().turretBuilt = true;
			break;
			
		case Common.ENTITY_TYPE_GUN_TORCH:
			CreateSprite(Graphics.GUN_TORCH_1,
						 0,
						 BuildingPositions[indexPos].x,
						 BuildingPositions[indexPos].y,
						 Graphics.GUN_TORCH_1.get(0).getWidth()/2,
						 Graphics.GUN_TORCH_1.get(0).getHeight()/2,
						 1f);
			Flame1 		= Common.ENTITY_TYPE_FIRE;
			Flame2 		= 0;
			Range 		= (int)(65*Settings.Instance().screenDensity);
			FiringOffset = Math.round(20*Settings.Instance().screenDensity);
			Settings.Instance().torchBuilt = true;
			break;
		};
		
		//Increment deployedGuns vlaue
		Settings.Instance().deployedGuns.setValue(Settings.Instance().deployedGuns.getValue()+1);
		
		//Initialize AI
		AIcurrentState = new State_Idle();
		AIcurrentState.Enter(this);
		
		Rotate(Settings.Instance().getRandom(360));
		
		loadSound();
		//Get grid locations
		cells = Settings.Instance().Grid.getCells(BuildingPositions[indexPos].x, BuildingPositions[indexPos].y, Range);
	}
	
	public void loadSound()
	{
		switch(Type)
		{
		case Common.ENTITY_TYPE_GUN_TORCH:
			soundStreamID = Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_torch_seg, -1, false);
			Settings.Instance().Sound.pause(soundStreamID, false);
			break;
		}
				
	}
	
	
	public void unloadSound()
	{
		Settings.Instance().Sound.stop(soundStreamID, false);
		soundStreamID = 0;
	}
	
	//Register enemy in the list
	private void RegisterEnemyNear(Enemy e)
	{
		enemiesNearList.add(e);
	}
	
	private boolean isEnemyInRange()
	{
		enemiesNearList.clear();
		enemiesInCell.clear();
		
		for(int i = cells.length-1; i >= 0; i--)
		{
			if(Settings.Instance().Grid.grid[cells[i]].list.isEmpty())
				continue;
			else
			{
				//Add objects that are in the cell
				for(Enemy E: Settings.Instance().Grid.grid[cells[i]].list)
					enemiesInCell.add(E);
			}
		}
		
		if(enemiesInCell.isEmpty()) return false;
		
		
		//Register all the enemies that are near this entity
		for(Enemy B : enemiesInCell)
		{
			//calculate distance
			if(Vector.getDistance(getPosition(), B.getPosition()) <= Range)
			{
				RegisterEnemyNear(B);
				
				//If it's an electric type turret - get all of the enemies, otherwise 1 would be enough
				if(TurretType != Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR)
					return true;
			}
		}
		
		//If there is any of the enemies near.
		if(enemiesNearList.size() > 0)return true;
		
		return false;
	}
	
	public void ChangeState(BaseState<Gun> newState)
	{
		AIcurrentState.Exit(this);
		AIcurrentState = newState;
		AIcurrentState.Enter(this);
	}
	
	public void UpdateAI()
	{
		AIcurrentState.Execute(this);
	}
	
	/**
	 * Increases level and statistics to the Gun Object. Put null to leave everything to default
	 * @param o1 Turret: type, Torch: Flame 1
	 * @param o2 Turret: nothing, Torch: Flame 2
	 */
	public void UpgradeGun(Object o1, Object o2)
	{
		//Do not proceed if Level does not meet the requirements.
		if(Level >= Common.LVL_MAX) return;
		
		//Upgrade. Only 2 times we need to upgrade
		if(Level == 1)
		{
			switch(Type)
			{
			case Common.ENTITY_TYPE_GUN_TURRET:
				switch((Integer)o1)
				{
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_BULLET_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					ChangeFrame(3);
					Level++;
					TurretType = (Integer)o1;
					PhyAtk += 5f;
					MagAtk += 2f;
					Range *= 1.20f;
					FiringOffset = Math.round(15*Settings.Instance().screenDensity);
					World.Map.UpgradeGun(this, Common.BUILD_TIME_BULLET_LEVEL_2);
					break;
						
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_ELECTR_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					Level++;
					TurretType = (Integer)o1;
					//Chage animation
					ChangeFrame(1);
					
					PhyAtk += 0;
					MagAtk += 0f;
					Range *= 1.20f;	
					ElectricChargeCount = 2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_ELECTR_LEVEL_2);
					break;
					
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_LASER:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_LASER_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					Level++;
					TurretType = (Integer)o1;
					//Chage animation
					ChangeFrame(2);
					
					PhyAtk = 0.9f;
					MagAtk = 0.05f;
					Range *= 1.20f;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_LASER_LEVEL_2);
					break;
				}
				break;
				
			case Common.ENTITY_TYPE_GUN_TORCH:
				ChangeFrame(1);
				
				switch((Integer)o1)
				{
				case Common.ENTITY_TYPE_FIRE:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_FIRE_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.20f;
					FiringOffset = Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_FIRE_LEVEL_2);
					break;
					
				case Common.ENTITY_TYPE_POISON:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_POISON_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.20f;
					FiringOffset =  Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_POISON_LEVEL_2);
					break;
					
				case Common.ENTITY_TYPE_FREEZE:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_FREEZE_LEVEL_2)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.20f;
					FiringOffset = Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_FREEZE_LEVEL_2);
					break;
				}
				
				break;
			}
		}
		else if(Level == 2)
		{
			switch(Type)
			{
			case Common.ENTITY_TYPE_GUN_TURRET:
				switch(TurretType)
				{
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_BULLET_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					ChangeFrame(4);
					Level++;
					PhyAtk += 10f;
					MagAtk += 5f;
					Range *= 1.20f;
					FiringOffset = Math.round(25*Settings.Instance().screenDensity);
					World.Map.UpgradeGun(this, Common.BUILD_TIME_BULLET_LEVEL_3);
					break;
						
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_ELECTR_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					ChangeFrame(5);
					Level++;
					PhyAtk += 2f;
					MagAtk += 1f;
					Range *= 1.20f;	
					ElectricChargeCount = 4;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_ELECTR_LEVEL_3);
					break;
					
				case Common.ENTITY_TYPE_GUN_TURRET_TYPE_LASER:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_LASER_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					ChangeFrame(6);
					Level++;
					PhyAtk = 2.0f;
					MagAtk = 0.5f;
					Range *= 1.20f;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_LASER_LEVEL_3);
					break;
				}
				break;
				
			case Common.ENTITY_TYPE_GUN_TORCH:
				ChangeFrame(2);
				
				switch((Integer)o2)
				{
				case Common.ENTITY_TYPE_FIRE:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_FIRE_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.30f;
					FiringOffset = Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_FIRE_LEVEL_3);
					break;
					
				case Common.ENTITY_TYPE_POISON:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_POISON_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.20f;
					FiringOffset = Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_POISON_LEVEL_3);
					break;
					
				case Common.ENTITY_TYPE_FREEZE:
					//Reduce money
					//If there is less money - abort upgrading
					if(!World.ReduceMoney(Settings.Instance().COST_FREEZE_LEVEL_3)) return;
					
					Settings.Instance().CostIncrease(Common.COST_INCREASE);
					
					attacking = false;
					soundPlaying = false;
					Settings.Instance().Sound.pause(soundStreamID, false);
					
					Level++;
					Range *= 1.20f;
					FiringOffset = Math.round(20*Settings.Instance().screenDensity);
					if(o1 != null) Flame1 = (Integer)o1;
					if(o2 != null) Flame2 = (Integer)o2;
					World.Map.UpgradeGun(this, Common.BUILD_TIME_FREEZE_LEVEL_3);
					break;
				}
				
				break;
			}
		}
		
		
		cells = Settings.Instance().Grid.getCells(getPosition().x, getPosition().y, Range);
	}
	
	public int getType(){return Type;}
	public int getTurretType(){return TurretType;}
	
	
	public void releasememory()
	{
		World = null;
		BuildingPositions = null;
		
		
		enemiesNearList.clear(); //List of enemies that are near this entity
		enemiesNearList = null;
		
		enemiesInCell.clear();
		enemiesInCell = null;
		
		lastShot.releaseMemory();
		lastShot = null;
		
		releaseMemory();
	}
	
	
	
	
	
	//========================================================================================
	/////////////////////////////////////// AI ///////////////////////////////////////////////
	//========================================================================================
	
	//========================================================================================
	///////////////////////////////// HandleMessage //////////////////////////////////////////
	//========================================================================================
	public void HandleMessage(Telegram msg)
	{
		//Call onMessage function
		if(!AIcurrentState.onMessage(this, msg)){//error has been thrown. write a global message
		}	
	}
	
	
	
	//========================================================================================
	///////////////////////////////// State_Idle /////////////////////////////////////////////
	//========================================================================================
	class State_Idle extends BaseState<Gun>
	{
		//=================== onMessage() ==================
		//==================================================
		public boolean onMessage(Gun gun, Telegram msg)
		{
			switch(msg.Msg){}
			return true;
		}
		
		//=================== Enter() ==================
		//==============================================
		public void Enter(Gun gun)
		{
			Settings.Instance().Sound.pause(soundStreamID, true);
			soundPlaying = false;
		}	
		
		//=================== Execute() ==================
		//================================================
		public void Execute(Gun gun)
		{	
			//Re-check if there are enemies in the field of sight
			if(isEnemyInRange())
			{
				//Change state to turn
				ChangeState(new State_Shoot());
				return;
			}
		}
		
		
		//=================== Exit() ==================
		//================================================
		public void Exit(Gun gun){}

	};
	
	
	
	
	//========================================================================================
	///////////////////////////////// State_Shoot /////////////////////////////////////////////
	//========================================================================================
	class State_Shoot extends BaseState<Gun>
	{
		private double Distance;
		private Enemy nearestEnemy;
		
		//Memory pool
		Vector FacingPos;
		Vector EnemyDirection;
		Vector lastEPos;
		
		//=================== onMessage() ==================
		//==================================================
		public boolean onMessage(Gun gun, Telegram msg)
		{
			switch(msg.Msg)
			{
			case Common.ENTITY_MESSAGE_IAM_DEAD:
					ChangeState(new State_Idle());
					break;
			}
			
			return true;
		}
		
		//=================== Enter() ==================
		//==============================================
		public void Enter(Gun gun)
		{
			//Initialize
			Distance = 0;
			
			FacingPos = new Vector();
			EnemyDirection = new Vector();
			lastEPos = null;
		}
		
		
		//=================== Execute() ==================
		//================================================
		public void Execute(Gun gun)
		{
			attacking = false;
			//Check to see if there is still someone near
			//If not, change the state to idle
			if(enemiesNearList.size() <= 0) 
			{
				Settings.Instance().Sound.pause(soundStreamID, true);
				soundPlaying = false;
				ChangeState(new State_Idle()); 
				return;
			}
			//First element in the list is the nearest
			else {nearestEnemy = enemiesNearList.get(0);}
			
			//if the object is null - exit
			if(nearestEnemy == null) 
			{
				Settings.Instance().Sound.pause(soundStreamID, true);
				soundPlaying = false;
				ChangeState(new State_Idle());
				return;
			}
			
			//if the distance is huge. go idle
			Distance = Vector.getDistance(getPosition(), nearestEnemy.getPosition());
			if(Distance > Range || Distance < 0) 
			{
				Settings.Instance().Sound.pause(soundStreamID, true);
				soundPlaying = false;
				ChangeState(new State_Idle());
				return;
			}
			
			//if enemy is dead
			if(nearestEnemy.isAlive() == false) 
			{
				Settings.Instance().Sound.pause(soundStreamID, true);
				soundPlaying = false;
				ChangeState(new State_Idle()); 
				return;
			}
	
			
			//If the type of a turret is electric - skip the rotation.
			if(TurretType == Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR)
			{
				if((lastShot.fpsCountNow() - lastShot.getFpsStamp()) > Common.ENTITY_GUN_SHOT_DELAY)
				{
					if(!isEnemyInRange()) return;
					
					//if((Player.Instance().soundFlag & Common.FLAG_SOUND) != 0)
						Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_thunder);
					
					//Create parameters for the shot
					ExtraInfo info = new ExtraInfo();
					
					info.PhyATK 		= PhyAtk;
					info.MagATK 		= MagAtk;
					
					@SuppressWarnings("unchecked")
					ArrayList<Enemy> listCopy = (ArrayList<Enemy>) enemiesNearList.clone();
					int count = 0;
					for(Enemy E: listCopy)
					{
						//Reduce lightning count per one shot.
						count++;
						if(count > ElectricChargeCount) break;
						
						MessageDispatcher.Instance().SendMessage(0, E.getID(), getID(), Common.ENTITY_MESSAGE_HIT, info);
						World.CreateLightning(getPosition(), E.getPosition(), (int)(2*Settings.Instance().screenDensity));
					}
					
					//Update time
					lastShot.updateFpsStamp();
				}
				return;
			}
			
			
			///////////////////////////////// ROTATE ///////////////////////////////////
			//Turn to the enemy
			//Get angle between vectors
			FacingPos.set(getPosition().velocityX, getPosition().velocityY);
			EnemyDirection = nearestEnemy.getPosition();						//Get enemy position
			
			//Predict monster future location
			if(lastEPos != null && Type == Common.ENTITY_TYPE_GUN_TORCH && nearestEnemy.FrozenPeriod <= 0)
				EnemyDirection = EnemyDirection.add(Vector.Normalize(lastEPos, EnemyDirection).multiply(Settings.Instance().ENTITY_GUN_ROTATION_PREDICT_LOC));
			else if(lastEPos != null && Type == Common.ENTITY_TYPE_GUN_TURRET && nearestEnemy.FrozenPeriod <= 0)
				EnemyDirection = EnemyDirection.add(Vector.Normalize(lastEPos, EnemyDirection).multiply(Settings.Instance().ENTITY_GUN_TURRET_ROTATION_PREDICT_LOC));
			//Save previous location
			lastEPos = new Vector(nearestEnemy.getPosition());
			
			EnemyDirection = Vector.Normalize(getPosition(), EnemyDirection);	//Normalize a vector, getting a dirrection
			
			double angle = Vector.getAngleBetweenVectors(FacingPos, EnemyDirection);
			
			//Change angle to degrees
			angle = Math.toDegrees(angle);
			
			//Determine the rotation. clockwise or countercw
			int sign = Vector.getSign(FacingPos, EnemyDirection);
			
			//How to turn in a shooting range
			if(angle < Common.ENTITY_GUN_ROTATION_ANGLE_OK)
			{
				//Turn
				Rotate(angle*sign);
			}
			//How to turn in out off shooting range
			else
			{
				Rotate(Common.ENTITY_GUN_ROTATION_SPEED*sign);
			}
			
			
			/////////////////// SHOOT //////////////////////////////
			
			//If the turret is a laser
			if(TurretType == Common.ENTITY_TYPE_GUN_TURRET_TYPE_LASER && angle < Common.ENTITY_GUN_ROTATION_ANGLE_OK)
			{
				//Create parameters for the shot
				ExtraInfo info = new ExtraInfo();
				
				info.PhyATK = PhyAtk;
				info.MagATK = MagAtk;
				
				
				World.CreateLaser(getPosition().add(getPosition().getNormalized().multiply(5)), nearestEnemy.getPosition(), 4);
				MessageDispatcher.Instance().SendMessage(0, nearestEnemy.getID(), getID(), Common.ENTITY_MESSAGE_HIT, info);
				return;
			}
			
			
			switch(Type)
			{
			case Common.ENTITY_TYPE_GUN_TURRET:
				//Check to see if the delay has passed
				if((lastShot.fpsCountNow() - lastShot.getFpsStamp()) > Common.ENTITY_GUN_SHOT_DELAY &&
					angle < Common.ENTITY_GUN_ROTATION_ANGLE_OK)
				{
					//Shoot
					//Create parameters for the shot
					ExtraInfo info = new ExtraInfo();
					
					info.PhyATK 		= PhyAtk;
					info.MagATK 		= MagAtk;
					info.Range 			= Range;
					
					switch(TurretType)
					{
					case Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET:
						World.CreateBullet(getPosition().x + getPosition().velocityX * FiringOffset, 		//Calculated position of bullet (front of gun)
										   getPosition().y + getPosition().velocityY * FiringOffset,		//
										   getPosition().velocityX, getPosition().velocityY,	//Speed
										   getDegree(),											//Rotation
										   info);												//Additional info
						break;
						
					}
					//Update time
					lastShot.updateFpsStamp();
					//Animation
				}
				break;
				
			case Common.ENTITY_TYPE_GUN_TORCH:
				if(angle < Common.ENTITY_GUN_ROTATION_ANGLE_OK)
				{
					//Settings.Instance().Sound.resume(soundStreamID, true);
					attacking = true;
				}
				
				//Check to see if the delay has passed
				if((lastShot.fpsCountNow() - lastShot.getFpsStamp()) > Settings.Instance().ENTITY_TORCH_SHOT_DELAY &&
				    angle < Common.ENTITY_GUN_ROTATION_ANGLE_OK)
				{
					
					
					Vector Flame = getPosition().clone();
					
					//Level = 3;
					if(Level == 3)
					{
						float degree = Float.valueOf(getDegree());
						degree-= 15;
						Flame.velocityX = (float)Math.cos(degree*(Math.PI/180));
						Flame.velocityY = (float)Math.sin(degree*(Math.PI/180))*-1;
					}
					
					//Create fire
					World.CreateFire(Flame.x + Flame.velocityX * FiringOffset, 
									 Flame.y + Flame.velocityY * FiringOffset,
									 getPosition().velocityX, getPosition().velocityY,
							         getDegree(),
							         Level,
							         Flame1);
					
					if(Level == 3)
					{
						Flame = getPosition().clone();
						float degree = Float.valueOf(getDegree());
						degree+= 15;
						Flame.velocityX = (float)Math.cos(degree*(Math.PI/180));
						Flame.velocityY = (float)Math.sin(degree*(Math.PI/180))*-1;
						
						//Create second fire
						World.CreateFire(Flame.x + Flame.velocityX * 20, 
										 Flame.y + Flame.velocityY * 20,
										 getPosition().velocityX, getPosition().velocityY,
								         getDegree(),
								         Level,
								         Flame2);
					}
					
					
					lastShot.updateFpsStamp();
				}
				break;
			};
			
			//Give it a boost
			if(soundPlaying && soundStreamID != 0)
				Settings.Instance().Sound.resume(soundStreamID, false);
			
			if(!soundPlaying && attacking && soundStreamID != 0)
			{
				Settings.Instance().Sound.resume(soundStreamID, false);
				soundPlaying = true;
			}
			else if(soundPlaying && !attacking && soundStreamID != 0)
			{
				Settings.Instance().Sound.pause(soundStreamID, false);
				soundPlaying = false;
			}
			else if(soundStreamID == 0)
				loadSound();
		}
		
		
		//=================== Exit() ==================
		//================================================
		public void Exit(Gun gun)
		{
		}
		
	
	};
};