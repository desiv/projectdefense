/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;

import com.projdef.projectdefense.MessageDispatcher;
import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;
import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Base.BaseState;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Manager.EntityDelayedRemove;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.ExtraInfo;
import com.projdef.projectdefense.Utilities.Telegram;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Vector;

public class Bullet extends BaseEntity
{
	enum EnumTest
        {
        one,
        tew,
        three
        }

	private World World;
	
	//AI
	private BaseState<Bullet> AIcurrentState;
	private Timer startTime = new Timer();
	private ExtraInfo info;
	private int speed = 8;
	
	private ArrayList<Enemy> NearEnemies = new ArrayList<Enemy>();
	private ArrayList<Enemy> enemiesInCell = new ArrayList<Enemy>();
	
	int[] cells;
	
	public Bullet(World w) {World = w;}
	
	//Initialize entity
	public void Initialize(float x, float y, float VelocityX, float VelocityY, float rotation, Object extra)
	{	
		//Generate bullet ID
		setBulletID();

        EnumTest a = EnumTest.one;
        EnumTest b = EnumTest.tew;
        a = b;
        if (a != EnumTest.one)
            {

            }


		//Register to entity manager
		//EntityManager.Instance().Register(this);
		
		info = (ExtraInfo)extra;
		
		AIcurrentState = new State_Travel();
		AIcurrentState.Enter(this);
		
		Range = Settings.Instance().ENTITY_ENEMY_DIAMETER;
		
		if(Settings.Instance().screenDensity > 1.0f)
			Range = Math.round(Settings.Instance().ENTITY_ENEMY_DIAMETER*Settings.Instance().screenDensity);
		
		//Creates sprite
		CreateSprite(Graphics.BULLET_1,			//Animation array
					 0,							//Animation start
					 x, y,						//Start X and Y
					 Graphics.BULLET_1.get(0).getWidth()/2,			//Center X of bitmap
					 Graphics.BULLET_1.get(0).getHeight()/2,		//Center Y of bitmap
					 1f);						//Scale
		
		//Change speed.
		ChangeVelocity(VelocityX, VelocityY);
		//Rotate
		Rotate(rotation);
		
		speed = Math.round(8*Settings.Instance().screenDensity);
		
		startTime.updateTimeStamp();
		
		cells  = Settings.Instance().Grid.getCells(x, y, Range);
	}
	
	public void UpdateAI()
	{
		AIcurrentState.Execute(this);
	}
	
	
	public void ChangeState(BaseState<Bullet> newState)
	{
		AIcurrentState.Exit(this);
		
		AIcurrentState = newState;
		
		AIcurrentState.Enter(this);
	}
	
	
	
	
	
	

	//========================================================================================
	/////////////////////////////////////// AI ///////////////////////////////////////////////
	//========================================================================================
	
	//========================================================================================
	///////////////////////////////// HandleMessage //////////////////////////////////////////
	//========================================================================================
	public void HandleMessage(Telegram msg)
	{
		//TODO: Check to see if it's valid
		//Call onMessage function
		if(!AIcurrentState.onMessage(this, msg)){//error has been thrown. write a global message
		}	
	}
	
	
	
	//========================================================================================
	///////////////////////////////// State_Idle /////////////////////////////////////////////
	//========================================================================================
	private class State_Travel extends BaseState<Bullet>
	{	
		//Position on creation
		private Vector P = new Vector();
		//Current position
		private Vector newP = new Vector();
		
		public void Enter(Bullet bullet)
		{
			//Set animation
			//ChangeAnimationSpeed(5);
			//onAnimationLoop(0, 4, true);
			
			P.x = getPosition().x;
			P.y = getPosition().y;
		}
		
		
		public void Execute(Bullet bullet)
		{
			//Sprite movement
			MoveXY(getPosition().x + getPosition().velocityX*speed,
					          getPosition().y + getPosition().velocityY*speed);
		
			
			//Dying
			//Calculate the distance
			if(Vector.getDistance(P, newP) > info.Range+10)
			{
				//If the distance is far enough - die.
				ChangeState(new State_Die());
				return;
			}
			
			cells  = Settings.Instance().Grid.getCells(getPosition().x, getPosition().y, Range);
			
			//Calculate distance travelled
			newP.x += getPosition().velocityX*speed;
			newP.y += getPosition().velocityY*speed;
			
						
			//Re-check if bullet has hit a monster
			if(isEnemyHit())
			{
				//Damage enemy
				for(Enemy e: NearEnemies)
					//Generate additional info
					//Send hit message to enemy's entity
					MessageDispatcher.Instance().SendMessage(0, e.ID, ID, Common.ENTITY_MESSAGE_HIT, info);
				
				World.CrateExplosion(getPosition());
				ChangeState(new State_Die());
			}
		}
		
		
		public void Exit(Bullet bullet){}
		public boolean onMessage(Bullet bullet, Telegram msg){return true;}
		
		
		private boolean isEnemyHit()
		{
			NearEnemies.clear();
			enemiesInCell.clear();
			
			for(int i = 0; i < cells.length; i++)
			{
				if(Settings.Instance().Grid.grid[cells[i]].list.isEmpty())
					continue;
				else
				{
					//Add objects that are in the cell
					for(Enemy E: Settings.Instance().Grid.grid[cells[i]].list)
						enemiesInCell.add(E);
				}
			}
			
			if(enemiesInCell.isEmpty()) return false;
			
			//Register all the enemies that are near this entity
			for(Enemy E :enemiesInCell)
			{
			   //Calculate distance
				if(Vector.getDistance(getPosition(), E.getPosition()) <= Range)
					NearEnemies.add(E);
			}
			
			//If there is any of the enemies near.
			if(NearEnemies.size() > 0)return true;
			
			return false;
		}
		
	};
	
		
		
	
	private class State_Die extends BaseState<Bullet>
	{
		public boolean onMessage(Bullet bullet, Telegram msg)
		{
			return true;
		}
		
		public void Enter(Bullet bullet)
		{
			//if((Player.Instance().soundFlag & Common.FLAG_SOUND) != 0)
			Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_explosion);
			EntityDelayedRemove.Instance().Remove(bullet);
			
			//Free up memory
			World = null;
			AIcurrentState = null;
			startTime = null;
			info = null;
			
			NearEnemies.clear();
			NearEnemies = null;
			
			enemiesInCell.clear();
			enemiesInCell = null;
		}
		
		public void Execute(Bullet bullet)
		{
		}
		
		public void Exit(Bullet bullet){}
		
	};
	
};