/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;

import com.projdef.projectdefense.Map;
import com.projdef.projectdefense.MessageDispatcher;
import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Base.BaseState;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Manager.EntityDelayedRemove;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.ExtraInfo;
import com.projdef.projectdefense.Utilities.Telegram;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Vector;



public class Enemy extends BaseEntity
{	
	//Information about the sprite
	//public Sprite EnemySprite = new Sprite();
	
	//AI. Current state that enemy is simulating
	private BaseState<Enemy> AIcurrentState;
	
	//Map information
	public ArrayList<Vector> Path;
	//How many steps
	public int PathSize;
	
	public Vector _startPos = new Vector();
	public Vector _endPos = new Vector();
	
	private int cell;
	
	//Frames/time
	public int FreezePeriod			= 0;
	public int FrozenPeriod 		= 0;
	private int PoisonPeriod	 	= 0;
	private int EntityType 	 		= 0;
	
	private Timer PoisonDelay = new Timer();
	
	
	public Enemy(){}
	
	public void Initialize(Map map, float x, float y, int lvl, int type)
	{
		//Generate ID
		setID();
		
		//Register entity
		Settings.Instance().EntityManager.Register(this);
		
		cell = Settings.Instance().Grid.getCell(x, y);
		Settings.Instance().Grid.add(cell, this);
		
		//Initialize stats
		Level = lvl;
		Alive = true;
		EntityType = type;
		
		switch(EntityType)
		{
		//Weak against burning, tough against guns shots
		case Common.ENTITY_TYPE_ENEMY_TEAR:	
			switch(Level)
			{
			case 1:
				HP 		= 100f;
				MaxHP 	= 100f;
				//Damage absorption 0.0 - 1.0 equivalent to 0% - 100%
				PhyDef 	= 0.9f;
				MagDef 	= 0.00f;
				//How much will the player get for killing it
				Cost 	= 10;
				Score 	= Settings.Instance().SCORE_PK_T*Level;
				break;
				
			case 2:
				HP 		= 200f;
				MaxHP 	= 200f;
				//Damage absorption 0.0 - 1.0 equivalent to 0% - 100%
				PhyDef 	= 0.9f;
				MagDef 	= 0.00f;
				//How much will the player get for killing it
				Cost 	= 20;
				Score 	= Settings.Instance().SCORE_PK_T*Level;
				break;
				
			case 3:
				HP 		= 400f;
				MaxHP 	= 400f;
				//Damage absorption 0.0 - 1.0 equivalent to 0% - 100%
				PhyDef 	= 0.9f;
				MagDef 	= 0.00f;
				//How much will the player get for killing it
				Cost 	= 40;
				Score 	= Settings.Instance().SCORE_PK_T*Level;
				break;
			}
			break;
			
		//Weak against gun shots, tough against burning
		case Common.ENTITY_TYPE_ENEMY_BURN:	
			switch(Level)
			{
			case 1:
				HP 		= 50f;
				MaxHP 	= 50f;
				PhyDef 	= 0.0f;
				MagDef 	= 0.85f;
				Cost 	= 5;
				Score 	= Settings.Instance().SCORE_PK_B*Level;
				break;
				
			case 2:
				HP 		= 200;
				MaxHP 	= 200f;
				PhyDef 	= 0.0f;
				MagDef 	= 0.85f;
				Cost 	= 10;
				Score 	= Settings.Instance().SCORE_PK_B*Level;
				break;
				
			case 3:
				HP 		= 400f;
				MaxHP 	= 400f;
				PhyDef 	= 0.0f;
				MagDef 	= 0.85f;
				Cost 	= 20;
				Score 	= Settings.Instance().SCORE_PK_B*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
			switch(Level)
			{
			case 1:
				HP 		= 250f;
				MaxHP 	= 250f;
				PhyDef 	= 0.9f;
				MagDef 	= 0.0f;
				Cost 	= 60;
				Score 	= Settings.Instance().SCORE_PK_ST*Level;
				break;
				
			case 2:
				HP 		= 500f;
				MaxHP 	= 500f;
				PhyDef 	= 0.9f;
				MagDef 	= 0.0f;
				Cost 	= 120;
				Score 	= Settings.Instance().SCORE_PK_ST*Level;
				break;
				
			case 3:
				HP 		= 900f;
				MaxHP 	= 900f;
				PhyDef 	= 0.9f;
				MagDef 	= 0.0f;
				Cost 	= 200;
				Score 	= Settings.Instance().SCORE_PK_ST*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
			switch(Level)
			{
			case 1:
				HP 		= 250;
				MaxHP 	= 250f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.80f;
				Cost 	= 50;
				Score 	= Settings.Instance().SCORE_PK_SB*Level;
				break;
			
			case 2:
				HP 		= 600f;
				MaxHP 	= 600f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.80f;
				Cost 	= 100;
				Score 	= Settings.Instance().SCORE_PK_SB*Level;
				break;
				
			case 3:
				HP 		= 800f;
				MaxHP 	= 800f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.80f;
				Cost 	= 170;
				Score 	= Settings.Instance().SCORE_PK_SB*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
			switch(Level)
			{
			case 1:
				HP 		= 60f;
				MaxHP 	= 60f;
				PhyDef 	= 0.8f;
				MagDef 	= 0.00f;
				Speed 	= 1.8f;
				Cost 	= 30;
				Score 	= Settings.Instance().SCORE_PK_FT*Level;
				break;
			
			case 2:
				HP 		= 125f;
				MaxHP 	= 125f;
				PhyDef 	= 0.8f;
				MagDef 	= 0.00f;
				Speed 	= 1.8f;
				Cost 	= 60;
				Score 	= Settings.Instance().SCORE_PK_FT*Level;
				break;
				
			case 3:
				HP 		= 300f;
				MaxHP 	= 300f;
				PhyDef 	= 0.8f;
				MagDef 	= 0.00f;
				Speed 	= 1.8f;
				Cost 	= 90;
				Score 	= Settings.Instance().SCORE_PK_FT*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
			switch(Level)
			{
			case 1:
				HP 		= 60f;
				MaxHP 	= 60f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.7f;
				Speed 	= 1.8f;
				Cost 	= 20;
				Score 	= Settings.Instance().SCORE_PK_FB*Level;
				break;
				
			case 2:
				HP 		= 125f;
				MaxHP 	= 125f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.7f;
				Speed 	= 1.8f;
				Cost 	= 40;
				Score 	= Settings.Instance().SCORE_PK_FB*Level;
				break;
				
			case 3:
				HP 		= 300f;
				MaxHP 	= 300f;
				PhyDef 	= 0.00f;
				MagDef 	= 0.7f;
				Speed 	= 1.8f;
				Cost 	= 70;
				Score 	= Settings.Instance().SCORE_PK_FB*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
			switch(Level)
			{
			case 1:
				HP 		= 150f;
				MaxHP 	= 150f;
				PhyDef 	= 0.7f;
				MagDef 	= 0.00f;
				Resistant = true;
				Cost 	= 30;
				Score 	= Settings.Instance().SCORE_PK_SPT*Level;
				break;
				
			case 2:
				HP 		= 250f;
				MaxHP 	= 250f;
				PhyDef 	= 0.7f;
				MagDef 	= 0.00f;
				Resistant = true;
				Cost 	= 60;
				Score 	= Settings.Instance().SCORE_PK_SPT*Level;
				break;
				
			case 3:
				HP 		= 500f;
				MaxHP 	= 500f;
				PhyDef 	= 0.7f;
				MagDef 	= 0.00f;
				Resistant = true;
				Cost 	= 100;
				Score 	= Settings.Instance().SCORE_PK_SPT*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
			switch(Level)
			{
			case 1:
				HP 		= 100f;
				MaxHP 	= 100f;
				PhyDef	= 0.00f;
				MagDef 	= 0.6f;
				Resistant = true;
				Cost 	= 20;
				Score 	= Settings.Instance().SCORE_PK_SPB*Level;
				break;
				
			case 2:
				HP 		= 250f;
				MaxHP 	= 250f;
				PhyDef	= 0.00f;
				MagDef 	= 0.6f;
				Resistant = true;
				Cost 	= 40;
				Score 	= Settings.Instance().SCORE_PK_SPB*Level;
				break;
				
			case 3:
				HP 		= 500f;
				MaxHP 	= 500f;
				PhyDef	= 0.00f;
				MagDef 	= 0.6f;
				Resistant = true;
				Cost 	= 90;
				Score 	= Settings.Instance().SCORE_PK_SPB*Level;
				break;
			}
			break;
			
		case Common.ENTITY_TYPE_ENEMY_BOSS:
			switch(Level)
			{
			case 1:
				HP 		= 1200f;
				MaxHP 	= 1200f;
				PhyDef 	= 0.6f;
				MagDef 	= 0.8f;
				Speed 	= 0.6f;
				Cost 	= 1000;
				Score 	= Settings.Instance().SCORE_PK_BOSS*Level;
				break;
				
			case 2:
				HP 		= 600f;
				MaxHP 	= 600f;
				PhyDef 	= 0.7f;
				MagDef 	= 0.7f;
				Speed 	= 0.6f;
				Cost 	= 600;
				Score 	= Settings.Instance().SCORE_PK_BOSS*Level;
				break;
				
			case 3:
				HP 		= 1600f;
				MaxHP 	= 1600f;
				PhyDef 	= 0.5f;
				MagDef 	= 0.5f;
				Speed 	= 0.6f;
				Cost 	= 1200;
				Score 	= Settings.Instance().SCORE_PK_BOSS*Level;
				break;
			}
			break;
		};
		
		//Load walking path
		Path = map.getEnemyWalkingPath();
		PathSize = map.getEnemyWalkingPathCount();
		
		//***** Creating enemy *************
		//Getting starting position
		_startPos = map.getEnemyStartPos();
		_endPos = map.getEnemyEndPos();
		
		//Create sprite
		switch(EntityType)
		{
		case Common.ENTITY_TYPE_ENEMY_TEAR:	
			CreateSprite(Graphics.ENEMY_TEAR, 0, x, y, Graphics.ENEMY_TEAR.get(0).getWidth()/2, Graphics.ENEMY_TEAR.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_BURN:	
			CreateSprite(Graphics.ENEMY_BURN, 0, x, y, Graphics.ENEMY_BURN.get(0).getWidth()/2, Graphics.ENEMY_BURN.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
			CreateSprite(Graphics.ENEMY_STRONG_TEAR, 0, x, y, Graphics.ENEMY_STRONG_TEAR.get(0).getWidth()/2, Graphics.ENEMY_STRONG_TEAR.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
			CreateSprite(Graphics.ENEMY_STRONG_BURN, 0, x, y, Graphics.ENEMY_STRONG_BURN.get(0).getWidth()/2, Graphics.ENEMY_STRONG_BURN.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
			CreateSprite(Graphics.ENEMY_FAST_TEAR, 0, x, y, Graphics.ENEMY_FAST_TEAR.get(0).getWidth()/2, Graphics.ENEMY_FAST_TEAR.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
			CreateSprite(Graphics.ENEMY_FAST_BURN, 0, x, y, Graphics.ENEMY_FAST_BURN.get(0).getWidth()/2, Graphics.ENEMY_FAST_BURN.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
			CreateSprite(Graphics.ENEMY_SPECIAL_TEAR, 0, x, y, Graphics.ENEMY_SPECIAL_TEAR.get(0).getWidth()/2, Graphics.ENEMY_SPECIAL_TEAR.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
			CreateSprite(Graphics.ENEMY_SPECIAL_BURN, 0, x, y, Graphics.ENEMY_SPECIAL_BURN.get(0).getWidth()/2, Graphics.ENEMY_SPECIAL_BURN.get(0).getHeight()/2, 1f);
			break;
			
		case Common.ENTITY_TYPE_ENEMY_BOSS:
			CreateSprite(Graphics.ENEMY_BOSS, 0, x, y, Graphics.ENEMY_BOSS.get(0).getWidth()/2, Graphics.ENEMY_BOSS.get(0).getHeight()/2, 1f);
			break;
		};
		
		
		//Increment released zombie count
		Settings.Instance().releasedZombies.setValue(Settings.Instance().releasedZombies.getValue()+1);
		Settings.Instance().aliveZombies.setValue(Settings.Instance().aliveZombies.getValue()+1);
		
		//Set up AI
		//**********************************
		AIcurrentState = new State_WalkToAGoal();
		AIcurrentState.Enter(this);
	}
	
	public boolean isAlive() {return Alive;}
	
	public void UpdateAI()
	{
		AIcurrentState.Execute(this);
	}
	
	public void ChangeState(BaseState<Enemy> newState)
	{
		AIcurrentState.Exit(this);
		
		AIcurrentState = newState;
		
		AIcurrentState.Enter(this);
	}
	
	
	
	
	//========================================================================================
	/////////////////////////////////////// AI ///////////////////////////////////////////////
	//========================================================================================
	
	//========================================================================================
	///////////////////////////////// HandleMessage //////////////////////////////////////////
	//========================================================================================
	public void HandleMessage(Telegram msg)
	{
		//Call onMessage function
		if(AIcurrentState == null)
			return;
		
		if(!AIcurrentState.onMessage(this, msg)){}	
	}
	
	//========================================================================================
	///////////////////////////////// State_WalkToGoal ///////////////////////////////////////
	//========================================================================================
    class State_WalkToAGoal extends BaseState<Enemy>
	{
    	
    	private float Freeze = 0;
    	//private float Bio = 1;
		//Goal
		private Vector goal = new Vector();
		
		//Dummy
		Vector vector = new Vector();
		
		private int goalCount = 0;
		private int index = 1;
		
		private Timer regenarateHP = new Timer();
		
		//=================== onMessage() ==================
		//==============================================
		public boolean onMessage(Enemy enemy, Telegram msg)
		{
			switch(msg.Msg)
			{
			case Common.ENTITY_MESSAGE_HIT:
				{
					//Calculate damage taken
					double magD = MagDef;
					double phyD = PhyDef;
					
					double pAbsorb = (1.0f - phyD);
					double mAbsorb = (1.0f - magD);
					
					//0.8 - 1.2
					double range = (Settings.Instance().RANDOM.nextDouble()+1)*0.65;
					if(range < 0.8d) range+=0.15d;
					else if(range > 1.2d) range-=0.1d;
			
					
					double pDmg = msg.Extra.PhyATK * range;
					double mDmg = msg.Extra.MagATK * range;
					
					double p = pDmg * pAbsorb;
					double m = mDmg * mAbsorb;
					
					//Can't be negative
					if(p < 0) p = 0;
					if(m < 0) m = 0;
					
					//Reduce HP
					HP -= (p+m);
					
					int rand = (int)(Settings.Instance().RANDOM.nextInt(10000)/100);
					//Calculate probability
					if(rand < 10 && EntityType != Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR && EntityType != Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN)
					{
						//Freeze enemy
						if(msg.Extra.Category == Common.ENTITY_TYPE_FREEZE && FreezePeriod < 5)
							FreezePeriod+= 90;

						//Make enemy frozen
						else if(msg.Extra.Category == Common.ENTITY_TYPE_FREEZE && FrozenPeriod < 5)
						{
							//Calculate another probability
							rand = (int)(Settings.Instance().RANDOM.nextInt(100000)/1000);
							if(rand < 10) FrozenPeriod+=45;
						}
						
						//Poison it
						if(msg.Extra.Category == Common.ENTITY_TYPE_POISON && PoisonPeriod < 5)
							PoisonPeriod+= 90;
					}
					
					//_enemy_sprite.onAnimationTime(1, 1000);
					//You are dead
					if(HP <= 0)
					{
						//Tell that this monster is dead
						Alive = false;
						//Increment statistics
						Settings.Instance().KILLS.setValue(Settings.Instance().KILLS.getValue()+1);
						//Add gold to the players account
						Settings.Instance().GOLD.setValue(Settings.Instance().GOLD.getValue() + Cost);
						//Increase score
						Settings.Instance().killScore.setValue(Settings.Instance().killScore.getValue() + Score);
						
						//Send message to inform the gun
						ExtraInfo extr = new ExtraInfo();
						extr.enemyPar = enemy;
						
						//Send back to the gun who killed it
						MessageDispatcher.Instance().SendMessage(0, msg.Sender, getID(), Common.ENTITY_MESSAGE_IAM_DEAD, extr);
						
						ChangeState(new State_Dead());
					}
					
					break;
				}
			}
			return false;
		}
		
		//=================== Enter() ==================
		//==============================================
		public void Enter(Enemy enemy)
		{
			//Load information
			//Our first goal is walk to second coordination of an array
			goal = Path.get(index);
			
			//Get the count of how many goals there are
			goalCount = PathSize;
			
			regenarateHP.updateFpsStamp();
		}
		
		//============== Execute() ====================
		//=============================================
		public void Execute(Enemy enemy)
		{
			//Reset specials
			Freeze 	= 1;
			
			//Animate
			if(PoisonPeriod <= 0 && FreezePeriod <= 0 && FrozenPeriod <= 0)
			{
				if(!onAnimation) ChangeFrame(0);
				Animate();
			}
			
			
			if(PoisonPeriod > 0)
			{
				if(PoisonDelay.ifFpsPassed(30))
					if((HP - 3) > 0 ) HP -= 3;
					else HP = 1;
				
				AnimatePoison();
			}
			
			//Slowing down foe
			if(FreezePeriod > 0)
			{
				Freeze = 0.4f;
				AnimateFrost();
			}
			
			if(FrozenPeriod > 0)
			{
				Freeze = 0;
				AnimateFrozen();
			}
			
				
			
			
			//Get position of enemy
			vector = getPosition();
			//Set goal
			vector.setDirection(goal.x, goal.y, true);
			
			//vector.velocityX = vector.velocityX * Settings.Instance().screenDensity;
			//vector.velocityY = vector.velocityY * Settings.Instance().screenDensity;
			//Go to the goal
			MoveXY( (vector.x) + (vector.velocityX*Freeze*Speed)*Settings.Instance().screenDensity, (vector.y) + (vector.velocityY*Freeze*Speed)*Settings.Instance().screenDensity);
			
			//Change to a new goal if current goal was reached
			if(isGoalReached(vector, goal))
			{	
				//Check to see if it's the last goal
				if(isGoalReached(vector, _endPos))
				{	
					//The goal has been reached, change state
					//Remove one human
					Settings.Instance().aliveHumans.setValue(Settings.Instance().aliveHumans.getValue()-1);
					enemy.ChangeState(new State_Dead());
				}
				//set new goal
				index++;
				
				//Avoid getting stuck on the last position.
				if(index == goalCount-1)
				{
					changeGoal(Path.get(index));
				}
				else if(index < goalCount-1)
				{
					//Jitter
					int value = Math.round(5*Settings.Instance().screenDensity);
					Vector jitter = new Vector(Settings.Instance().RANDOM.nextInt(value*2)-value, Settings.Instance().RANDOM.nextInt(value*2)-value);
					changeGoal(Path.get(index).add(jitter));
				}
			}
			
			//Reduce time of effect
			if(FreezePeriod > 0) FreezePeriod--;
			if(FrozenPeriod > 0) FrozenPeriod--;
			if(PoisonPeriod > 0) PoisonPeriod--;
			
			RegenerateHP();
			
			//Recheck location in grid

			int newCell = Settings.Instance().Grid.getCell(getPosition().x, getPosition().y);
			if(cell != newCell)
			{
				Settings.Instance().Grid.remove(cell, enemy);
				Settings.Instance().Grid.add(newCell, enemy);
				cell = newCell;
			}
		}
		
		//====================== Exit() ====================
		//==================================================
		public void Exit(Enemy enemy)
		{
		}
		
		
		private void RegenerateHP()
		{
			if(!(HP >= MaxHP))
			{
				if(regenarateHP.ifFpsPassed(Math.round(1*Common.FPS_EQUALTOSECOND))) 
					HP++;
			}
		}
		
		private void Animate()
		{
			//Random animation
			if(onAnimation) return;
			
			//Eye movement
			if(Settings.Instance().RANDOM.nextInt(100) < 5)
			{
				switch(EntityType)
				{
				case Common.ENTITY_TYPE_ENEMY_TEAR:
					onAnimationReversed(0, 4);
					ChangeAnimationSpeed(2);
					break;
					
				case Common.ENTITY_TYPE_ENEMY_BURN:
					onAnimationReversed(0, 6);
					ChangeAnimationSpeed(8);
					break;
					
				case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
					onAnimationReversed(0, 4);
					ChangeAnimationSpeed(2);
					break;
					
					
				case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
					onAnimationReversed(0, 4);
					ChangeAnimationSpeed(1);
					break;
				
					
				case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
					onAnimationReversed(0, 7);
					ChangeAnimationSpeed(1);
					break;
					
				case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
					onAnimationReversed(0, 4);
					ChangeAnimationSpeed(1);
					break;

				case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
					onAnimationReversed(0, 3);
					ChangeAnimationSpeed(4);
					break;
					

				case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
					onAnimationReversed(0, 12);
					ChangeAnimationSpeed(1);
					break;
					
					

				case Common.ENTITY_TYPE_ENEMY_BOSS:
					onAnimationReversed(0, 4);
					ChangeAnimationSpeed(1);
					break;
				}
			}
		}
		
	
		
		private void AnimateFrost()
		{
			switch(EntityType)
			{
			case Common.ENTITY_TYPE_ENEMY_TEAR:
				ChangeFrame(5);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_BURN:
				ChangeFrame(7);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
				ChangeFrame(5);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
				ChangeFrame(5);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
				ChangeFrame(8);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
				ChangeFrame(5);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
				ChangeFrame(4);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
				ChangeFrame(13);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_BOSS:
				ChangeFrame(5);
				break;
			}
		}
		
		private void AnimateFrozen()
		{
			switch(EntityType)
			{
			case Common.ENTITY_TYPE_ENEMY_TEAR:
				ChangeFrame(6);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_BURN:
				ChangeFrame(8);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
				ChangeFrame(6);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
				ChangeFrame(6);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
				ChangeFrame(9);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
				ChangeFrame(6);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
				ChangeFrame(5);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
				ChangeFrame(14);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_BOSS:
				ChangeFrame(6);
				break;
			}
		}
		
		private void AnimatePoison()
		{
			switch(EntityType)
			{
			case Common.ENTITY_TYPE_ENEMY_TEAR:
				ChangeFrame(7);
				break;
	
			case Common.ENTITY_TYPE_ENEMY_BURN:
				ChangeFrame(9);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
				ChangeFrame(7);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
				ChangeFrame(7);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
				ChangeFrame(10);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
				ChangeFrame(6);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
				ChangeFrame(15);
				break;
				
			case Common.ENTITY_TYPE_ENEMY_BOSS:
				ChangeFrame(7);
				break;
			}
		}
		
		private void changeGoal(Vector goal)
		{
			this.goal = goal;
		}
		
		private boolean isGoalReached(Vector pos, Vector goal)
		{
			float a = goal.x - pos.x;
			float b = goal.y - pos.y;
			
			//Count the distance from object to a goal
			double magnitude = Math.sqrt(a*a + b*b);
			
			//If the distance is right
			if(magnitude <= Settings.Instance().GOAL_REACH_DISTANCE)
			{
				//Reached the goal
				return true;
			}
			return false;
		}
	};
	
	
	//========================================================================================
	///////////////////////////////// State_Dead /////////////////////////////////////////////
	//========================================================================================
	class State_Dead extends BaseState<Enemy>
	{
		int alpha;
		
		public boolean onMessage(Enemy enemy, Telegram msg)
		{
			return true;
		}
		
		public void Enter(Enemy enemy)
		{
			alpha = 255;
		}
		
		public void Execute(Enemy enemy)
		{
			alpha-=30;
			
			//Add this entity to remove list
			if(alpha <= 0)
			{
				//Change statistics
				Settings.Instance().aliveZombies.setValue(Settings.Instance().aliveZombies.getValue()-1);
				Settings.Instance().Grid.remove(cell, enemy);
				EntityDelayedRemove.Instance().Remove(enemy);
				
				AIcurrentState = null;
				Path = null;
			
				_startPos = null;
				_endPos = null;
				
				PoisonDelay.releaseMemory();
				PoisonDelay = null;
			}
			else ChangeOpacity(alpha);
		}
		
		public void Exit(Enemy enemy){}
	};
};