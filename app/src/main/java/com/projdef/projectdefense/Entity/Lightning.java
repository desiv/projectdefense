/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;
import java.util.Collections;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Graphics.Line;
import com.projdef.projectdefense.Manager.EntityDelayedRemove;
import com.projdef.projectdefense.Utilities.Vector;

public class Lightning
{
	Bitmap lightBitmap;
	Canvas canvas;
	Vector Source;
	Vector sizeOfBitmap;
	Paint paint;
	int opacity;
	float Rotation;
	Matrix matrix;
	int offset;
	
	public Lightning(Vector source, Vector dest, int thickness)
	{	
		matrix = new Matrix();
		offset = (int)(16*Settings.Instance().screenDensity);
		opacity = 255;
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setAlpha(opacity);
		//Save up the original location
		Source = source.clone();
		
		//Starting position of a new bitmap
		Vector base = new Vector(offset, offset);
		//Magnitude of a lightning line
		Vector tangent = dest.subtract(Source);
		
		sizeOfBitmap = tangent.clone();
		if(sizeOfBitmap.x < 0) sizeOfBitmap.x*= -1;
		if(sizeOfBitmap.y < 0) sizeOfBitmap.y*= -1;
		
		//Adjust the position in a bitmap. It will stop lightning going beyond the boundaries
		Rotation = (float) Math.atan2(tangent.x, tangent.y);
		Rotation = (float) Math.toDegrees(Rotation);
		
		//Change negative degree to positive.
		if(Rotation < 0) Rotation+=360;
		
		//Adjust the start of the lightning
		if(Rotation > 90 && Rotation <= 180) base = base.add(new Vector(0, sizeOfBitmap.y));
		else if(Rotation > 180 && Rotation <= 270) base = base.add(new Vector(sizeOfBitmap.x, sizeOfBitmap.y));
		else if(Rotation > 270 && Rotation <= 360) base = base.add(new Vector(sizeOfBitmap.x, 0));
		
		//Create bitmap of defined size
		lightBitmap = Bitmap.createBitmap((int)sizeOfBitmap.x+offset*2, (int)sizeOfBitmap.y+offset*2, Bitmap.Config.ARGB_8888);
		//Apply to the canvas
		canvas = new Canvas(lightBitmap);
		
		tangent.Normalize();
		Vector normal = tangent.getNormalized();
		//Flip normals
		normal = new Vector(normal.y, -normal.x);
		float length = tangent.length();
		
		//Choose random positions in the lightning
		//0 - Start point, 1 - End point
		ArrayList<Float> positions = new ArrayList<Float>();
		positions.add(0f);
		
		for(int i = 0; i < length /4; i++)
			positions.add(Settings.Instance().RANDOM.nextFloat());
		
		Collections.sort(positions);
		
		float Sway = 80*Settings.Instance().screenDensity;
	    float Jaggedness = 1 / Sway;
		
	    //Previous point save
	    Vector prevPoint = base;
	    float prevDisplacement = 0;
	    Line tmpline;
	    for(int i = 1; i < positions.size(); i++)
	    {
	    	float pos = positions.get(i);
	    	
	    	// used to prevent sharp angles by ensuring very close positions also have small perpendicular variation.
	        float scale = (length * Jaggedness) * (pos - positions.get(i - 1));
	        
	        // defines an envelope. Points near the middle of the bolt can be further from the central line.
	        float envelope = pos > 0.95f ? 20 * (1 - pos) : 1;
	        
	        float displacement = Settings.Instance().RANDOM.nextFloat() * 2 * Sway - Sway;
	        displacement -= (displacement - prevDisplacement) * (1 - scale);
	        displacement *= envelope;
	        
	        //Calculated point
	        Vector point = base.add(tangent.multiply(pos)).add(normal.multiply(displacement));
	        
	        //Create new line with calculated point
	        tmpline = new Line(prevPoint, point, thickness);
	        //Draw line into bitmap
	        tmpline.Draw(canvas);
	        
	        prevPoint = point;
	        prevDisplacement = displacement;
	    }
	    
	    //Create last line to reach end point
	    tmpline = new Line(prevPoint, tangent.add(base), thickness);
	    //Draw line into bitmap
	    tmpline.Draw(canvas);
	}
	
	public void Draw(Canvas c)
	{
		if(matrix != null)
			matrix.reset();
		else
			return;
		
		if(Rotation > 90 && Rotation <= 180)
			matrix.setTranslate(Source.x - offset, Source.y - sizeOfBitmap.y - offset);
		
		else if(Rotation > 180 && Rotation <= 270)
			matrix.setTranslate(Source.x - sizeOfBitmap.x - offset, Source.y - sizeOfBitmap.y - offset);
		
		else if(Rotation > 270 && Rotation <= 360) 
			matrix.setTranslate(Source.x - sizeOfBitmap.x - offset, Source.y - offset);
		
		else matrix.setTranslate(Source.x - offset, Source.y - offset);
		
		c.drawBitmap(lightBitmap, matrix, paint);
	}
	
	public void UpdateAI()
	{			
		opacity-=30;
		if(opacity < 0) opacity = 0;
		paint.setAlpha(opacity);
		
		if(opacity <= 0)
		{
			EntityDelayedRemove.Instance().Remove(this);
			
			matrix = null;
			paint = null;
		}
	}
}