/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;

import com.projdef.projectdefense.MessageDispatcher;
import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;
import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Base.BaseState;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Manager.EntityDelayedRemove;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.ExtraInfo;
import com.projdef.projectdefense.Utilities.Telegram;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Vector;

public class Fire extends BaseEntity
{
	//AI
	private BaseState<Fire> AIcurrentState;
	private Timer startTime = new Timer();
	
	private int FlameType = 0;
	private int speed;
	
	int[] cells;
	
	//List of monsters which are near this entity
	private ArrayList<Enemy> NearbyEnemies = new ArrayList<Enemy>();
	private ArrayList<Enemy> enemiesInCell = new ArrayList<Enemy>();
	
	public Fire() {}
	
	//Initialize entity
	public void Initialize(float x, float y, float VelocityX, float VelocityY, float rotation, int level, Object extra)
	{	
		//EntityManager.Instance().Register(this);
		//Generate fire ID
		setFireID();
		//Copy gun's settings
		FlameType 	= (Integer)extra;
		Level 		= level;
		Range 		= 10;
		speed 		= Math.round(5*Settings.Instance().screenDensity);
		
		if(Settings.Instance().screenDensity > 1.0f)
			Range 		= Math.round(10*Settings.Instance().screenDensity);
		
		
		switch(FlameType)
		{
		case Common.ENTITY_TYPE_FIRE:
			
			switch(Level)
			{
			case 1:
				PhyAtk 		= 0.05f;
				MagAtk 		= 1.5f;
				break;
				
			case 2:
				PhyAtk 		= 0.05f;
				MagAtk 		= 2.2f;
				break;
				
			case 3:
				PhyAtk 		= 0.05f;
				MagAtk 		= 4.0f;
				break;
			}
			CreateSprite(Graphics.FIRE_1,	//Array of sprites for animation
						 0,					//Start index of first frame to show.
						 x, y,				//Position
						 Graphics.FIRE_1.get(0).getWidth()/2,
						 Graphics.FIRE_1.get(0).getHeight()/2,			//Center of sprite (usually used for rotation)
						 1f);				//Scaling
			break;
			
		case Common.ENTITY_TYPE_POISON:
			switch(Level)
			{
			case 1:
				PhyAtk 		= 0.05f;
				MagAtk 		= 1.0f;
				break;

			case 2:
				PhyAtk 		= 0.05f;
				MagAtk 		= 2.0f;
				break;
			
			case 3:
				PhyAtk 		= 0.05f;
				MagAtk 		= 3.0f;
				break;
			}
			CreateSprite(Graphics.POISON_1,	//Array of sprites for animation
						 0,					//Start index of first frame to show.
						 x, y,				//Position
						 Graphics.POISON_1.get(0).getWidth()/2,
						 Graphics.POISON_1.get(0).getHeight()/2,    //Center of sprite (usually used for rotation)
						 1f);				//Scaling
			break;
		
		case Common.ENTITY_TYPE_FREEZE:
			switch(Level)
			{
			case 1:	
				PhyAtk 		= 0.05f;
				MagAtk 		= 0.5f;
				break;
			
			case 2:
				PhyAtk 		= 0.05f;
				MagAtk 		= 0.5f;
				break;
				
			case 3:
				PhyAtk 		= 0.05f;
				MagAtk 		= 1.0f;
				break;
			}
			CreateSprite(Graphics.FREEZE_1,	//Array of sprites for animation
						 0,					//Start index of first frame to show.
						 x, y,				//Position
						 Graphics.FREEZE_1.get(0).getWidth()/2,
						 Graphics.FREEZE_1.get(0).getHeight()/2,			//Center of sprite (usually used for rotation)
						 1f);				//Scaling
			break;
		}
		
		//PhyAtk *= Settings.Instance().screenDensity;
		//MagAtk *= Settings.Instance().screenDensity;
		
		//Initialize AI
		AIcurrentState = new State_Travel();
		AIcurrentState.Enter(this);
		
		ChangeVelocity(VelocityX, VelocityY);
		Rotate(rotation);
		
		startTime.updateTimeStamp();
		
		cells = Settings.Instance().Grid.getCells(x, y, Range);
	}
	
	public void UpdateAI()
	{
		AIcurrentState.Execute(this);
	}
	
	
	public void ChangeState(BaseState<Fire> newState)
	{
		AIcurrentState.Exit(this);
		
		AIcurrentState = newState;
		
		AIcurrentState.Enter(this);
	}
	
	
	
	
	
	

	//========================================================================================
	/////////////////////////////////////// AI ///////////////////////////////////////////////
	//========================================================================================
	
	//========================================================================================
	///////////////////////////////// HandleMessage //////////////////////////////////////////
	//========================================================================================
	public void HandleMessage(Telegram msg)
	{
		//Call onMessage function
		if(!AIcurrentState.onMessage(this, msg)){//error has been thrown. write a global message
		}	
	}
	
	
	
	//========================================================================================
	///////////////////////////////// State_Idle /////////////////////////////////////////////
	//========================================================================================
	private class State_Travel extends BaseState<Fire>
	{
		private Timer scalingTimer = new Timer();
		private float Scale;
		private float ScaleIndex;
		private int opacity;
		
		//Starting position
		private Vector P = new Vector();
		
		public void Enter(Fire fire)
		{
			//Set animation
			ChangeAnimationSpeed(10);
			//onAnimationLoop(0, 4, true);
			
			P.x = getPosition().x;
			P.y = getPosition().y;
			
			Scale = 0.56f;
			
			//Reduce size of level 1
			if(Level == 1) Scale *= 0.50f;
			
			ScaleIndex = 0.1f;
//			
//			if(Settings.Instance().screenDensity == 1.5f)
//				ScaleIndex = 0.1f * 0.5f;
//			if(Settings.Instance().screenDensity == 2.0f)
//				ScaleIndex = 0.1f * 0.25f;
			
			opacity = 255;
			
			scalingTimer.updateTimeStamp();
		}
		
		
		public void Execute(Fire fire)
		{
			//Sprite movement
			MoveXY(getPosition().x + getPosition().velocityX*speed,
					          getPosition().y + getPosition().velocityY*speed);
			
			//Dying
			//Calculate the distance
			if(Vector.getDistance(P, getPosition()) > Settings.Instance().ENTITY_FIRE_DIE_DISTANCE)
			{
				//If the distance is far enough - die.
				ChangeState(new State_Die());
				return;
			}
			
			//Get accuping cells
			//cells = Settings.Instance().Grid.getCells(getPosition().x, getPosition().y, Range);
			
			//Animation. Making sprite bigger
			if(Vector.getDistance(P, getPosition()) > Settings.Instance().ENTITY_FIRE_SCALEANIMATION_DISTANCE)
			{
				Scale += ScaleIndex;
				ScaleIndex*= 0.90f;
				
				//Special case for level 1 
				if(Level == 1 && Scale > 0.85f) opacity = Math.round(opacity*0.8f);
				//
				else if(Scale > 1.1f) opacity = Math.round(opacity*0.8f);
			}
			
			//Scaling. Changing scale
			ChangeScale(Scale);
			ChangeOpacity(opacity);
			
			
			//Re-check if there are enemies in the field of sight
			if(isEnemyInRange())
			{
				//Damage enemy
				for(Enemy e: NearbyEnemies)
				{	
					ExtraInfo info = new ExtraInfo();
					
					info.PhyATK = PhyAtk;
					info.MagATK = MagAtk;
					info.Category = FlameType;

					//Send hit message to enemy's entity
					MessageDispatcher.Instance().SendMessage(0, e.ID, ID, Common.ENTITY_MESSAGE_HIT, info);
				}
			}
		}
		
		
		public void Exit(Fire fire){}
		public boolean onMessage(Fire fire, Telegram msg){return true;}
		
		
		private boolean isEnemyInRange()
		{
			NearbyEnemies.clear();
			//enemiesInCell.clear();
			
			//Register all the enemies that are near this entity
			for(Enemy E : ((World)Settings.Instance().Game.World).EnemyList)
			{
			   //Calculate distance
				if(Vector.getDistance(getPosition(), E.getPosition()) <= Range)
					NearbyEnemies.add(E);
			}
			
			
			
//			for(int i = 0; i < cells.length; i++)
//			{
//				if(Settings.Instance().Grid.grid[cells[i]].list.isEmpty())
//					continue;
//				else
//				{
//					//Add objects that are in the cell
//					for(Enemy E: Settings.Instance().Grid.grid[cells[i]].list)
//						enemiesInCell.add(E);
//				}
//			}
//			
//			if(enemiesInCell.isEmpty()) return false;
//			
//			
//			//Register all the enemies that are near this entity
//			for(Enemy E : enemiesInCell)
//			{
//			   //Calculate distance
//				if(Vector.getDistance(getPosition(), E.getPosition()) <= Range)
//					NearbyEnemies.add(E);
//			}
			
			
			//If there is any of the enemies near.
			if(NearbyEnemies.size() > 0)return true;

			return false;
		}
	};
	
		
		
	
	private class State_Die extends BaseState<Fire>
	{	
		public boolean onMessage(Fire fire, Telegram msg)
		{
			return true;
		}
		
		public void Enter(Fire fire)
		{
			EntityDelayedRemove.Instance().Remove(fire);
			
			//AI
			AIcurrentState = null;
			startTime = null;
			
			//List of monsters which are near this entity
			NearbyEnemies.clear();
			NearbyEnemies = null;
			
			enemiesInCell.clear();
			enemiesInCell = null;
		}
		
		public void Execute(Fire fire)
		{
		}
		
		public void Exit(Fire fire){}
	};
	
};