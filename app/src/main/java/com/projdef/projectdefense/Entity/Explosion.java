/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Entity;

import java.util.ArrayList;

import android.graphics.Bitmap;

import com.projdef.projectdefense.Graphics.Sprite;

public class Explosion extends Sprite
{
	boolean alive = true;
	
	public Explosion(ArrayList<Bitmap> bitmaps, float x, float y)
	{
		CreateSprite(bitmaps, 0, x, y, (float)(bitmaps.get(0).getWidth()/2), (float)(bitmaps.get(0).getHeight()/2), 1f);
		
		onAnimationFrame(0, bitmaps.size()-1);
		ChangeAnimationSpeed(1);
		
		alive = true;
	}
	
	public void UpdateAI()
	{
		if(!onAnimation)
		{
			alive = false;
			releaseMemory();
		}
	}
	
	public boolean getState()
	{
		return alive;
	}
}