/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import com.projdef.projectdefense.Entity.Gun;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Utilities.Common;

import android.graphics.Bitmap;
import android.graphics.Paint;

/**
 * 
 * @author Marius Savickas
 *
 */
class Options 
{
	private World World;
	
	private Paint paint_background;
	private Paint paint_border;
	private Paint paint_words;
	private Paint paint_max;
	private Paint paint_words_green;
	private Paint paint_words_red;
	//Animation variables
	private boolean isVisible = false;
	private boolean animate = true;
	private int animationStage = Common.ANIMATION_OPTION_BAR_RISE;
	//Used for animating the bar
	private int animationIndex;	//Pixels
	private int downBarSize;
	//Selected spot and entity of it
	private boolean spotValid = false;
	private int spotIndex = -1;
	private int entityID = -1;
	private Gun Entity;
	
	//0 - main state(building options)
	private int state = 0;
	
	Options(World world)
	{
		World = world;
		
		//Background
		paint_background = new Paint();
		paint_background.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_background.setAntiAlias(Settings.Instance().AntiAlias);
		paint_background.setColor(0xA0101010);
		
		//Border
		paint_border = new Paint();
		paint_border.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_border.setAntiAlias(Settings.Instance().AntiAlias);
		paint_border.setStrokeWidth(3);
		paint_border.setColor(0xF5404040);
		
		//Letters: white
		paint_words = new Paint();
		paint_words.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_words.setAntiAlias(true);
		paint_words.setColor(0xFF808080);
		paint_words.setTextSize(10*Settings.Instance().screenDensity);
		
		//Letters: max word
		paint_max = new Paint();
		paint_max.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_max.setAntiAlias(true);
		paint_max.setColor(0xF5808080);
		paint_max.setTextSize(15*Settings.Instance().screenDensity);
		
		//Letters: green
		paint_words_green = new Paint();
		paint_words_green.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_words_green.setAntiAlias(true);
		paint_words_green.setColor(0xF5008000);
		paint_words_green.setTextSize(15*Settings.Instance().screenDensity);
		
		//Letters: red
		paint_words_red = new Paint();
		paint_words_red.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_words_red.setAntiAlias(true);
		paint_words_red.setColor(0xF5800000);
		paint_words_red.setTextSize(15*Settings.Instance().screenDensity);
	}
	
	public void Initialize()
	{
		//How tall the bar is
		downBarSize = (int)(80*Settings.Instance().screenDensity);
		animationIndex = (int)(80*Settings.Instance().screenDensity);
	}
	
	public void Uninitialize()
	{
		World = null;
		Entity = null;
	}
	
	public void Draw()
	{
		//If options are not visible, then what's the point in drawing?
		if(!isVisible) return;
		
		//Check spot validity
		if(!spotValid) return;
		
		//Checking to see if there is a entity in the spot and if there is - get its state(building, upgrading), type.
		if(entityID == -1 || Entity == null) 
			state = Common.OPTION_BUILDING_TIER;
		else 
			state = Common.OPTION_UPGRADING_TIER;
		
		
		//Draw black rectangle
		//That's where the options will be
		Settings.Instance().Canvas.drawRect(0,
											Settings.Instance().screenHeight - downBarSize + animationIndex,
											Settings.Instance().screenWidth,
											Settings.Instance().screenHeight+animationIndex,
											paint_background);
		
		//Horizontal borders
		Settings.Instance().Canvas.drawLine(0,
											Settings.Instance().screenHeight - downBarSize  +animationIndex,
											Settings.Instance().screenWidth,
											Settings.Instance().screenHeight - downBarSize + animationIndex,
											paint_border);
		
		//Vertical borders
		for(int i = 1; i <= 3; i++)
			Settings.Instance().Canvas.drawLine(downBarSize*i,
												Settings.Instance().screenHeight - downBarSize + animationIndex,
												downBarSize*i,
												Settings.Instance().screenHeight+animationIndex,
												paint_border);
		
		
		float Density = Settings.Instance().screenDensity;
		float width = 80*Density;
		//float DrawX = 1;
		float DrawY = Settings.Instance().screenHeight+animationIndex;
		
		Settings S = Settings.Instance();
		
		
		//State switch
		switch(state)
		{
		//======================					  ===========================
		//====================== First tier (Building) ===========================
		//======================                      ===========================
		case Common.OPTION_BUILDING_TIER:			//Build
		{
			//================== Box 1 / Gun turret ====================
			
			S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(2), hcenter(width, Graphics.THUMBNAILS.get(2)), DrawY-downBarSize, null);
			S.Canvas.drawText("1 Lvl", hcenter(width, "1 Lvl", paint_words), DrawY-37*Density, paint_words);
			S.Canvas.drawText("Turret", hcenter(width, "Turret", paint_words), DrawY-25*Density, paint_words);
			
			if(S.GOLD.getValue() - S.COST_BULLET_LEVEL_1 >= 0)
				S.Canvas.drawText(S.COST_BULLET_LEVEL_1 + " G", hcenter(width, S.COST_BULLET_LEVEL_1 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
			else 
				S.Canvas.drawText(S.COST_BULLET_LEVEL_1 + " G", hcenter(width, S.COST_BULLET_LEVEL_1 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
			
			//================= Box 2 / Torch ==========================
			S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(3), hcenter(width, Graphics.THUMBNAILS.get(3))+downBarSize, DrawY-downBarSize, null);
			S.Canvas.drawText("1 Lvl", hcenter(width, "1 Lvl", paint_words)+downBarSize, DrawY-37*Density, paint_words);
			S.Canvas.drawText("Torch", hcenter(width, "Torch", paint_words)+downBarSize, DrawY-25*Density, paint_words);
			
			if(S.GOLD.getValue() - S.COST_FIRE_LEVEL_1 >= 0)
				S.Canvas.drawText(S.COST_FIRE_LEVEL_1 + " G", hcenter(width, S.COST_FIRE_LEVEL_1 + " G", paint_words_green)+downBarSize, DrawY-7*Density, paint_words_green);
			else 
				S.Canvas.drawText(S.COST_FIRE_LEVEL_1 + " G", hcenter(width, S.COST_FIRE_LEVEL_1 + " G", paint_words_red)+downBarSize, DrawY-7*Density, paint_words_red);
			
			//================= Box 3 /  ==========================
			break;
		}
		//======================					     		    =========================
		//====================== Second tier (Upgrading) Gun Turret =========================
		//======================					     			=========================
		case Common.OPTION_UPGRADING_TIER:		//Upgrade
		{
			//Check for lock
			if(Entity == null) break;
			
			if(Entity.getLvl() >= Player.Instance().tierLock && Entity.getLvl() != Common.LVL_MAX)
			{
				S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(8), hcenter(width, Graphics.THUMBNAILS.get(8)), 				DrawY-vcenter(width, Graphics.THUMBNAILS.get(8))*3, null);
				S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(8), hcenter(width, Graphics.THUMBNAILS.get(8))+downBarSize,		DrawY-vcenter(width, Graphics.THUMBNAILS.get(8))*3, null);
				S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(8), hcenter(width, Graphics.THUMBNAILS.get(8))+downBarSize*2, 	DrawY-vcenter(width, Graphics.THUMBNAILS.get(8))*3, null);
				break;
			}
			
			switch(Entity.getType())
			{
			case Common.ENTITY_TYPE_GUN_TURRET:
				//Display icons accordingly to the level
				switch(Entity.getLvl())
				{
				//Tier 1/3
				case 1:
					/*
					 * Box 1
					 */
					S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(7), hcenter(width, Graphics.THUMBNAILS.get(7)), DrawY-downBarSize+15, null);
					S.Canvas.drawText("2 Lvl", hcenter(width, "2 Lvl", paint_words), DrawY-37*Density, paint_words);
					S.Canvas.drawText("Bullet", hcenter(width, "Bullet", paint_words), DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_BULLET_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_BULLET_LEVEL_2 + " G", hcenter(width, S.COST_BULLET_LEVEL_2 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_BULLET_LEVEL_2 + " G", hcenter(width, S.COST_BULLET_LEVEL_2 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
					
					/*
					 * Box 2
					 */
					S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(0), hcenter(width, Graphics.THUMBNAILS.get(0))+downBarSize, DrawY-downBarSize+15, null);
					S.Canvas.drawText("1 Lvl", hcenter(width, "1 Lvl", paint_words)+downBarSize, DrawY-37*Density, paint_words);
					S.Canvas.drawText("Electricity", hcenter(width, "Electricity", paint_words)+downBarSize, DrawY-25*Density, paint_words);

					if(S.GOLD.getValue() - S.COST_ELECTR_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_ELECTR_LEVEL_2 + " G", hcenter(width, S.COST_ELECTR_LEVEL_2 + " G", paint_words_green)+downBarSize, DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_ELECTR_LEVEL_2 + " G", hcenter(width, S.COST_ELECTR_LEVEL_2 + " G", paint_words_red)+downBarSize, DrawY-7*Density, paint_words_red);
					
					/*
					 * Box 3
					 */
					S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(1), hcenter(width, Graphics.THUMBNAILS.get(1))+downBarSize*2, DrawY-downBarSize+15, null);
					S.Canvas.drawText("1 Lvl", hcenter(width, "1 Lvl", paint_words)+downBarSize*2, DrawY-37*Density, paint_words);
					S.Canvas.drawText("Laser", hcenter(width, "Laser", paint_words)+downBarSize*2, DrawY-25*Density, paint_words);

					if(S.GOLD.getValue() - S.COST_LASER_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_LASER_LEVEL_2 + " G", hcenter(width, S.COST_LASER_LEVEL_2 + " G", paint_words_green)+downBarSize*2, DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_LASER_LEVEL_2 + " G", hcenter(width, S.COST_LASER_LEVEL_2 + " G", paint_words_red)+downBarSize*2, DrawY-7*Density, paint_words_red);
					
					break;
				//Tier 2/3
				case 2:
					/*
					 * Box 1
					 */
					switch(Entity.getTurretType())
					{
					case Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET:
							S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(7), hcenter(width, Graphics.THUMBNAILS.get(7)), DrawY-downBarSize+15, null);
							S.Canvas.drawText("3 Lvl", hcenter(width, "3 Lvl", paint_words), DrawY-37*Density, paint_words);
							S.Canvas.drawText("Bullet", hcenter(width, "Bullet", paint_words), DrawY-25*Density, paint_words);
							
							if(S.GOLD.getValue() - S.COST_BULLET_LEVEL_3 >= 0)
								S.Canvas.drawText(S.COST_BULLET_LEVEL_3 + " G", hcenter(width, S.COST_BULLET_LEVEL_3 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
							else
								S.Canvas.drawText(S.COST_BULLET_LEVEL_3 + " G", hcenter(width, S.COST_BULLET_LEVEL_3 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
							break;
							
					case Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR:
						S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(0), hcenter(width, Graphics.THUMBNAILS.get(0)), DrawY-downBarSize+15, null);
						S.Canvas.drawText("2 Lvl", hcenter(width, "2 Lvl", paint_words), DrawY-37*Density, paint_words);
						S.Canvas.drawText("Electricity", hcenter(width, "Electricity", paint_words), DrawY-25*Density, paint_words);
						
						if(S.GOLD.getValue() - S.COST_ELECTR_LEVEL_3 >= 0)
							S.Canvas.drawText(S.COST_ELECTR_LEVEL_3 + " G", hcenter(width, S.COST_ELECTR_LEVEL_3 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
						else
							S.Canvas.drawText(S.COST_ELECTR_LEVEL_3 + " G", hcenter(width, S.COST_ELECTR_LEVEL_3 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
						break;
						
					case Common.ENTITY_TYPE_GUN_TURRET_TYPE_LASER:
						S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(1), hcenter(width, Graphics.THUMBNAILS.get(1)), DrawY-downBarSize+15, null);
						S.Canvas.drawText("2 Lvl", hcenter(width, "2 Lvl", paint_words), DrawY-37*Density, paint_words);
						S.Canvas.drawText("Laser", hcenter(width, "Poison", paint_words), DrawY-25*Density, paint_words);
						
						if(S.GOLD.getValue() - S.COST_LASER_LEVEL_3 >= 0)
							S.Canvas.drawText(S.COST_LASER_LEVEL_3 + " G", hcenter(width, S.COST_LASER_LEVEL_3 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
						else
							S.Canvas.drawText(S.COST_LASER_LEVEL_3 + " G", hcenter(width, S.COST_LASER_LEVEL_3 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
						break;
					}
					break;
				//Tier 3/3
				case 3:
					/*
					 * Box 1
					 */
					S.Canvas.drawText("MAX", hcenter(width, "MAX", paint_max), DrawY-hcenter(width, "MAX", paint_max)*2, paint_max);
					break;
				};
				break;
			
			//======================					     		=========================
			//====================== Second tier (Upgrading) Torch  =========================
			//======================					     	    =========================
			case Common.ENTITY_TYPE_GUN_TORCH:
				
				//Display icons accordingly to the level
				switch(Entity.getLvl())
				{
				//Tier 1/3
				case 1:
					/*
					 * Box 1
					 */
					S.Canvas.drawBitmap(Graphics.FIRE_1.get(0), hcenter(width, Graphics.FIRE_1.get(0)), DrawY-downBarSize+15, null);
					S.Canvas.drawText("Fire", hcenter(width, "Fire", paint_words), DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_FIRE_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_FIRE_LEVEL_2 + " G", hcenter(width, S.COST_FIRE_LEVEL_2 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_FIRE_LEVEL_2 + " G", hcenter(width, S.COST_FIRE_LEVEL_2 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
					
					/*
					 * Box 2
					 */
					S.Canvas.drawBitmap(Graphics.FREEZE_1.get(0), hcenter(width, Graphics.FREEZE_1.get(0))+downBarSize,  DrawY-downBarSize+15, null);
					S.Canvas.drawText("Freeze", hcenter(width, "Freeze", paint_words)+downBarSize, DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_FREEZE_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_FREEZE_LEVEL_2 + " G", hcenter(width, S.COST_FREEZE_LEVEL_2 + " G", paint_words_green)+downBarSize, DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_FREEZE_LEVEL_2 + " G", hcenter(width, S.COST_FREEZE_LEVEL_2 + " G", paint_words_red)+downBarSize, DrawY-7*Density, paint_words_red);
					/*
					 * Box 3
					 */
					S.Canvas.drawBitmap(Graphics.POISON_1.get(0), hcenter(width, Graphics.POISON_1.get(0))+downBarSize*2,  DrawY-downBarSize+15, null);
					S.Canvas.drawText("Poison", hcenter(width, "Poison", paint_words)+downBarSize*2, DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_POISON_LEVEL_2 >= 0)
						S.Canvas.drawText(S.COST_POISON_LEVEL_2 + " G", hcenter(width, S.COST_POISON_LEVEL_2 + " G", paint_words_green)+downBarSize*2, DrawY-7*Density, paint_words_green);
					else 
						S.Canvas.drawText(S.COST_POISON_LEVEL_2 + " G", hcenter(width, S.COST_POISON_LEVEL_2 + " G", paint_words_red)+downBarSize*2, DrawY-7*Density, paint_words_red);
					break;
				//Tier 2/3
				case 2:
					/*
					 * Box 1
					 */
					S.Canvas.drawBitmap(Graphics.FIRE_1.get(0), hcenter(width, Graphics.FIRE_1.get(0)),  DrawY-downBarSize+15, null);
					S.Canvas.drawText("Fire", hcenter(width, "Fire", paint_words), DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_FIRE_LEVEL_3 >= 0)
						S.Canvas.drawText(S.COST_FIRE_LEVEL_3 + " G", hcenter(width, S.COST_FIRE_LEVEL_3 + " G", paint_words_green), DrawY-7*Density, paint_words_green);
					else
						S.Canvas.drawText(S.COST_FIRE_LEVEL_3 + " G", hcenter(width, S.COST_FIRE_LEVEL_3 + " G", paint_words_red), DrawY-7*Density, paint_words_red);
					
					/*
					 * Box 2
					 */
					S.Canvas.drawBitmap(Graphics.FREEZE_1.get(0), hcenter(width, Graphics.FREEZE_1.get(0))+downBarSize,  DrawY-downBarSize+15, null);
					S.Canvas.drawText("Freeze", hcenter(width, "Freeze", paint_words)+downBarSize, DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_FREEZE_LEVEL_3 >= 0)
						S.Canvas.drawText(S.COST_FREEZE_LEVEL_3 + " G", hcenter(width, S.COST_FREEZE_LEVEL_3 + " G", paint_words_green)+downBarSize, DrawY-7*Density, paint_words_green);
					else
						S.Canvas.drawText(S.COST_FREEZE_LEVEL_3 + " G", hcenter(width, S.COST_FREEZE_LEVEL_3 + " G", paint_words_red)+downBarSize, DrawY-7*Density, paint_words_red);
					
					/*
					 * Box 3
					 */
					S.Canvas.drawBitmap(Graphics.POISON_1.get(0), hcenter(width, Graphics.POISON_1.get(0))+downBarSize*2,  DrawY-downBarSize+15, null);
					S.Canvas.drawText("Poison", hcenter(width, "Poison", paint_words)+downBarSize*2, DrawY-25*Density, paint_words);
					
					if(S.GOLD.getValue() - S.COST_POISON_LEVEL_3 >= 0)
						S.Canvas.drawText(S.COST_POISON_LEVEL_3 + " G", hcenter(width, S.COST_POISON_LEVEL_3 + " G", paint_words_green)+downBarSize*2, DrawY-7*Density, paint_words_green);
					else
						S.Canvas.drawText(S.COST_POISON_LEVEL_3 + " G", hcenter(width, S.COST_POISON_LEVEL_3 + " G", paint_words_red)+downBarSize*2, DrawY-7*Density, paint_words_red);
					break;
				//Tier 3/3
				case 3:
					S.Canvas.drawText("MAX", hcenter(width, "MAX", paint_max), DrawY-hcenter(width, "MAX", paint_max)*2, paint_max);
				}
				break;
				
			};
		};
		
	};
		
		//================= Box 4 / Cancel ================
		S.Canvas.drawBitmap(Graphics.THUMBNAILS.get(9),
							hcenter(width, Graphics.THUMBNAILS.get(9))+downBarSize*3,
							DrawY-vcenter(width, Graphics.THUMBNAILS.get(9))*2-5,
							null);
	}
	
	//Execute option
	public void Process(int option)
	{	
		int tier = 0;
		if(entityID != -1) tier = Entity.getLvl();
		
		//Check for lock
		if(tier >= Player.Instance().tierLock && option != 4)
		{
			//setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
			return;
		}
		
		switch(tier)
		{
		/*
		 * Tier 0/3
		 * Building Guns.
		 */
		case 0:
			switch(option)
			{
			//Option 1(Box)
			case 1:
				if(World.CreateGun(spotIndex, Common.ENTITY_TYPE_GUN_TURRET, Common.BUILD_TIME_TURRET))	//Type: 0 - Gun turret
				{
					//if(getSelectedSpot() > -1)
					//	World.Map.SpriteBuilding[getSelectedSpot()].ChangeFrame(1);
					
					setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				}
				break;
			
			//Option 1(Box)
			case 2:
				if(World.CreateGun(spotIndex, Common.ENTITY_TYPE_GUN_TORCH, Common.BUILD_TIME_TORCH))	//Type: 1 - Torch
				{
					//if(getSelectedSpot() > -1)
					//	World.Map.SpriteBuilding[getSelectedSpot()].ChangeFrame(1);
					
					setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				}
				break;
				
			//Option 3(Box)
			case 3:
				break;
			
			//Option 4(Box)
			case 4:
				setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				break;
			};
			
			break;
		
		/*
		 * Tier 1/3
		 * Upgrading.
		 */
		case 1:
			switch(option)
			{
			//Option 1(Box)
			case 1:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET)
					{
						Entity.UpgradeGun(Common.ENTITY_TYPE_GUN_TURRET_TYPE_BULLET, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
					//Torch upgrade to Fire
					else
					{	
						Entity.UpgradeGun(Common.ENTITY_TYPE_FIRE, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
			
			//Option 1(Box)
			case 2:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET) 
					{
						Entity.UpgradeGun(Common.ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
					//Torch upgrade to Freezing
					else 
					{
						Entity.UpgradeGun(Common.ENTITY_TYPE_FREEZE, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
				
			//Option 3(Box)
			case 3:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET) 
					{
						Entity.UpgradeGun(Common.ENTITY_TYPE_GUN_TURRET_TYPE_LASER, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
					//Torch upgrade to Poinson
					else 
					{
						Entity.UpgradeGun(Common.ENTITY_TYPE_POISON, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
		    
				//Option 4(Box)
			case 4:
				setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				break;
			}
			break;
		
		/*
		 * Tier 2/3
		 * Upgrading.
		 */
		case 2:
			switch(option)
			{
			//Option 1(Box)
			case 1:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET)
					{
						Entity.UpgradeGun(null, null);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
					//Torch upgrade to Fire
					else 
					{
						Entity.UpgradeGun(null, Common.ENTITY_TYPE_FIRE);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
			
			//Option 1(Box)
			case 2:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET) {}
					//Torch upgrade to Freeze
					else 
					{
						Entity.UpgradeGun(null, Common.ENTITY_TYPE_FREEZE);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
				
			//Option 3(Box)
			case 3:
				if((Entity.getLvl() < Common.LVL_MAX))
				{
					//Gun turret
					if(Entity.getType() == Common.ENTITY_TYPE_GUN_TURRET) {}
					//Torch upgrade to Poison
					else 
					{
						Entity.UpgradeGun(null, Common.ENTITY_TYPE_POISON);
						setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
					}
				}
				break;
			
			//Option 4(Box)
			case 4:
				setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				break;
			}
			break;
			
			
		/*
		 * Tier 3/3
		 * Max.
		 */
		case 3:
			switch(option)
			{
			//Option 1(Box)
			case 1:
				break;
			
			//Option 1(Box)
			case 2:
				break;
				
			//Option 3(Box)
			case 3:
				break;
			
			//Option 4(Box)
			case 4:
				setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
				break;
			}
			break;
		
			
		};
	}
	
	
	public void Update()
	{
		//If options are not visible, then what's the point in updating?
		if(!isVisible) return;
		
		//Animation
		if(animate)
		{
			if(animationStage == Common.ANIMATION_OPTION_BAR_RISE)
			{
				if(!(animationIndex <= 0)) animationIndex-=10;
				if(animationIndex <= 0) animate = false;
			}
			
			if(animationStage == Common.ANIMATION_OPTION_BAR_DROP)
			{
				if(!(animationIndex >= downBarSize)) animationIndex+=10;
				if(animationIndex >= downBarSize) 
				{
					animate = false;
					setVisibility(false);
				}
			}
		}
	}
	
	public boolean HandleTouch(float x, float y)
	{
		//Handle option window
		//If the options window is turned on
		if(getVisibility() && !getAnimationState())
		{	
			//Option 1 was hit
			if(Common.isInBounds2(0, Settings.Instance().screenHeight - downBarSize, downBarSize, downBarSize, x, y))
			{
				Process(1);
				return true;
			}
			
			//Option 2
			else if(Common.isInBounds2(downBarSize, Settings.Instance().screenHeight - downBarSize, downBarSize, downBarSize, x, y))
			{
				Process(2);
				return true;
			}
			
			//Option 3
			else if(Common.isInBounds2(downBarSize*2, Settings.Instance().screenHeight - downBarSize, downBarSize, downBarSize, x, y))
			{
				Process(3);
				return true;
			}
			
			//Options 4
			else if(Common.isInBounds2(downBarSize*3, Settings.Instance().screenHeight - downBarSize, downBarSize, downBarSize, x, y))
			{
				Process(4);
				return true;
			}
			else
			{
				setAnimation(Common.ANIMATION_OPTION_BAR_DROP);
			}
		}
		
		return false;
	}
	
	/**
	 * Selects spot in the map and raises option bar
	 * @param spotIndex index of a spot
	 * @param entityID	
	 */
	public void SelectSpot(int spotIndex, int entityID)
	{
		//Reset animation
		if(spotValid) World.Map.SpriteBuilding[this.spotIndex].ChangeFrame(0);
		
		spotValid = true;
		
		this.spotIndex = spotIndex;
		this.entityID = entityID;
		this.Entity = null;
		
		for(Gun g: World.GunList)
		{
			if(entityID != -1 && g.getID() == entityID)
				Entity = g;
		}
		
		if(Entity == null && entityID != -1) spotValid = false;
		
		//Animate press on gun
		World.Map.SpriteBuilding[this.spotIndex].ChangeFrame(1);
		//Draw option bar
		setAnimation(Common.ANIMATION_OPTION_BAR_RISE);
	}
	
	/**
	 * Animation switch
	 * @param a - command ( ANIMATION_OPTION_BAR_RISE or ANIMATION_OPTION_BAR_DROP )
	 */
	public void setAnimation(int a)
	{
		if(Common.ANIMATION_OPTION_BAR_RISE == a)
		{
			animationStage = Common.ANIMATION_OPTION_BAR_RISE;
			setVisibility(true);
		}
		else if(Common.ANIMATION_OPTION_BAR_DROP == a)
		{
			//Deselect
			if(spotValid) World.Map.SpriteBuilding[this.spotIndex].ChangeFrame(0);
			animationStage = Common.ANIMATION_OPTION_BAR_DROP;
		}
		
		animate = true;
	}
	
	public void clear()
	{
		spotIndex = -1;
		entityID = -1;
		Entity = null;
	}
	
	//Returns coordinates for center
	private float hcenter(float width, Bitmap bmp)
	{
		return (width-bmp.getWidth())/2;
	}
	
	//Returns coordinates for center
	private float vcenter(float width, Bitmap bmp)
	{
		return (width-bmp.getWidth())/2;
	}
	
	//Returns coordinates for center
	private float hcenter(float width, String txt, Paint p)
	{
		return (width-p.measureText(txt))/2;
	}
	
	public boolean getVisibility(){return isVisible;}
	
	public boolean getAnimationState() {return animate;}
	
	public int getEntityID(){return entityID;}
	
	public int getSelectedSpot(){return spotIndex;}
	
	public void setVisibility(boolean v){isVisible = v;}
};