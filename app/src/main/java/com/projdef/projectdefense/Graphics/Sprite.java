/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */

package com.projdef.projectdefense.Graphics;

import java.util.ArrayList;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.Utilities.Vector;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

public class Sprite
{	
	private Vector position, center;
	private float degree, scale;
	protected boolean onAnimation;
	private boolean loop, endAnimation, memoryReleased;
	
	private Matrix matrix = new Matrix();
	private Paint paint = new Paint();
	
	private ArrayList<Bitmap> Sprite;

	private int animationSpeed;
	private int animationType;
	private int startFrame;
	//State for reversed animation 0 - beginning, 1 - ending; 
	private int reversedState;
	private int endFrame;
	private long animationTime;
	
	private int frame;
	private int zeroFrame;
	private int frameSize;
	private Timer animationSpeedtime = new Timer();
	private Timer animationFrameTime = new Timer();
	
	//================= Constructor =====================
	public Sprite()
	{
		paint.setAlpha(255);
		paint.setAntiAlias(Settings.Instance().AntiAlias);
		paint.setDither(false);
		paint.setFilterBitmap(false);
		
		frame = 0;
		position = new Vector();
		center = new Vector();
		degree = 0;
		animationSpeed = Common.ANIMATION_SPEED;
		onAnimation = false;
		loop = false;
		endAnimation = false;
		memoryReleased = false;
	}
	
	/**
	 * Creates a sprite.
	 * @param Sprite an array of bitmaps
	 * @param animIndex starting frame of animation
	 * @param x position at screen
	 * @param y position at screen
	 * @param cX center of bitmap
	 * @param cY center of bitmap
	 * @param scale scaling
	 */
	public void CreateSprite(ArrayList<Bitmap> Sprite, int animIndex, float x, float y, float cX, float cY, float scale)
	{
		paint.setAlpha(255);
		
		this.scale 	= scale;
		this.Sprite = Sprite;
		frame 		= animIndex;
		zeroFrame 	= animIndex;
		frameSize	= Sprite.size();
		
		//Matrix initialization
		matrix.reset();
		matrix.setTranslate(x, y);
		
		position.x = x;
		position.y = y;
		
		//Center coordinations of the Sprite
		center.x = cX;
		center.y = cY;
		
		//Set direction with normalized vectors
		position.setDirection(400, y, true);
	}
	//===============================
	//================================
	//================================

	/**
	 * Draw a sprite into canvas
	 */
	public void DrawSprite(Canvas c)
	{
		if(memoryReleased)
			return;
		
		if(frame >= frameSize) {frame = 0; return;}
		
		//if our Sprite is created then draw it
		if(!Sprite.isEmpty())
		{	
			//Check whether rotation and scaling is needed
			if(degree != 0 || scale != 1.0f)
			{
				//Skip matrix scaling if there is no scaling, but only rotation
				if(scale == 1.0f)
					matrix.setTranslate((position.x-center.x), (position.y-center.y));
				else
				{
					matrix.setTranslate((position.x - center.x*scale), (position.y - center.y*scale));
					matrix.preScale(scale, scale);
				}
				
				matrix.preRotate(degree*-1, center.x, center.y);
				c.drawBitmap(Sprite.get(frame), matrix, paint);
			}
			//Skip scaling and rotating.
			else c.drawBitmap(Sprite.get(frame), position.x-center.x, position.y-center.y, paint);
		}
		else return;
		

		//Animation
		if(onAnimation)
		{
			switch(animationType)
			{
			case 0:	//From frame to frame
				if(animationSpeedtime.ifFpsPassed((int)(animationSpeed)))
				{
					frame++;
					//End of animation
					if(frame >= endFrame)
					{
						//Reset to original frame
						frame = zeroFrame;
						
						//End the animation
						if(!loop)onAnimation = false;
						if(endAnimation){onAnimation = false; loop = false; endAnimation = false;}
					}
					animationSpeedtime.updateFpsStamp();
				}
				break;

			
			case 1://Timed frame
				if(animationFrameTime.ifFpsPassed(animationTime))
				{
					frame = zeroFrame;
					onAnimation = false;
				}
				break;
				
				
			case 2: //Reversed frame to frame
				if(animationSpeedtime.ifFpsPassed(animationSpeed))
				{
					if(reversedState == 0) frame++;
					else frame--;
					
					if(reversedState == 0 && frame >= endFrame)
					{
						//Reverse
						reversedState = 1;
					}
					else if(reversedState == 1 && frame <= startFrame)
					{
						 //stop it
						frame = zeroFrame;
						onAnimation = false;
					}
					
					animationSpeedtime.updateFpsStamp();
				}
				break;
			};
		}
	}
	
		

	
	public void ChangeFrame(int frame) 
	{
		this.frame = frame;
		//Stop any animation
		onAnimation = false;
	}
	
	/**
	 * Changes the animation speed. Measured by game frames
	 * @param speed - interval to advance to another frame
	 */
	public void ChangeAnimationSpeed(int speed) {animationSpeed = speed;}
	
	/**
	 * Animation will be played from particular starting frame to end frame
	 * startframe must be smaller than endframe
	 * @param startFrame
	 * @param endFrame
	 */
	protected void onAnimationFrame(int startFrame, int endFrame)
	{
		if(startFrame < endFrame)
		{
			animationType	= 0;
			frame 			= startFrame;
			this.endFrame		= endFrame;
			
			onAnimation 	= true;
			loop 			= false;
			animationSpeedtime.updateFpsStamp();
		}
		else return;
	}
	
	/**
	 * Particular frame will be played within a period of time
	 * @param frame
	 * @param time
	 */
	protected void onAnimationTime(int frame, long time)
	{
		animationType 	= 1;
		this.frame 			= frame;
		animationTime	 = time;
		
		onAnimation 	= true;
		loop 			= false;
		animationFrameTime.updateFpsStamp();
	}
	
	protected void onAnimationLoop(int startFrame, int endFrame, boolean state)
	{
		if(state && startFrame < endFrame) 
		{
			//Normal animation
			animationType = 0;
			frame = startFrame;
			this.endFrame = endFrame;
			
			onAnimation = true;
			loop = true;
			animationSpeedtime.updateFpsStamp();
			
		}
	}
	
	protected void onEndAnimationLoop()
	{
		endAnimation = true;
	}
	
	/**
	 * Animation that goes from starting frame to end frame and then in the reverse order
	 * @param starFrame
	 * @param endFrame
	 */
	protected void onAnimationReversed(int startFrame, int endFrame)
	{
		if(startFrame < endFrame)
		{
			//Reversed animation
			animationType = 2;
			//Set up frame range
			this.startFrame = startFrame;
			frame = startFrame;
			this.endFrame = endFrame;
			reversedState = 0;
			//Enable animation
			onAnimation = true;
			loop = false;
			animationSpeedtime.updateFpsStamp();
		}
	}
	
	
	public void Rotate(double dgr)
	{
		degree += dgr;
		if(degree > 360) degree = degree - 360;
		if(degree < 0) degree = degree + 360;
		
		//Set new facing direction
		position.velocityX = (float)Math.cos(degree*(Math.PI/180));
		position.velocityY = (float)Math.sin(degree*(Math.PI/180))*-1;
	}
	
	//===================== MoveXY() ============
	/* Moves the Sprite */
	protected void MoveXY(float x, float y)
	{
		position.x = x;
		position.y = y;
	}
	
	protected void ChangeVelocity(float Vx, float Vy)
	{
		position.velocityX = Vx;
		position.velocityY = Vy;
	}
	
	protected void ChangeScale(float s)
	{
		scale = s;
	}
	
	//Position of Sprite
	public Vector getPosition(){return position;}
	
	//What scale
	protected float getScale(){return scale;}
	
	protected float getDegree()
	{
		Float d = degree;
		return d;
	}
	
	/**
	 * Set opacity of a spirte
	 * @param o - opacity (0 - 255)
	 */
	protected void ChangeOpacity(int o)
	{
		paint.setAlpha(o);
	}
	
	
	protected int getSpriteHeight(){return Sprite.get(0).getHeight();}
	protected int getSpriteWidth(){return Sprite.get(0).getWidth();}
	
	
	public void releaseMemory()
	{
		position = null;
		center = null;
		
		matrix = null;
		paint = null;
		
		Sprite = null;

		animationSpeedtime.releaseMemory();
		animationSpeedtime = null;
		
		animationFrameTime.releaseMemory();
		animationFrameTime = null;
		
		memoryReleased = true;
	}
};