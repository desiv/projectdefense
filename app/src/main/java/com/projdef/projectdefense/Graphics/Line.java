/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Graphics;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Utilities.Vector;


public class Line
{
	Vector A;
	Vector B;
	float Thickness = 1;
	float ImageThickness = 16;
	Vector capCenter;
	Vector segmentCenter;
	Matrix manipulate;
	Matrix matrix;
	Paint paint;
	ArrayList<Bitmap> BitmapList;
	
	/**
	 * Draws a line with specific bitmap
	 * @param a - start point
	 * @param b - start point
	 * @param thickness - counted by pixels. Initial is 8 pixels.
	 * 
	 */
	public Line(Vector a, Vector b, float thickness)
	{
		BitmapList = Graphics.LIGHTNING;
		
		ImageThickness = 16*Settings.Instance().screenScaleSprites;
		
		A = a;
		B = b;
		Thickness = thickness;
		
		//Center of cap
		capCenter = new Vector(BitmapList.get(0).getWidth(),
							   BitmapList.get(0).getHeight() / 2f);
		//Center of segment
		segmentCenter = new Vector(0, BitmapList.get(1).getHeight() / 2f);
		
		manipulate = new Matrix();
		matrix = new Matrix();
		
		paint = new Paint();
		paint.setDither(true);
		paint.setAntiAlias(true);
	}
	
	
	/**
	 * Draws a line with specific bitmap
	 * @param a - start point
	 * @param b - start point
	 * @param thickness - counted by pixels. Initial is 8 pixels.
	 * 
	 */
	public Line(ArrayList<Bitmap> bitmap, Vector a, Vector b, float thickness)
	{
		BitmapList = bitmap;
		
		A = a;
		B = b;
		Thickness = thickness;
		
		//Center of cap
		capCenter = new Vector(BitmapList.get(0).getWidth(),
							   BitmapList.get(0).getHeight() / 2f);
		//Center of segment
		segmentCenter = new Vector(0, BitmapList.get(1).getHeight() / 2f);
		
		manipulate = new Matrix();
		matrix = new Matrix();
		
		paint = new Paint();
		paint.setDither(true);
		paint.setAntiAlias(true);
	}
	
	
	public void Draw(Canvas c)
	{
		Vector tangent = B.subtract(A);
		//Gets the rotation in radians
		float rotation = (float) Math.atan2(tangent.x, tangent.y) * -1;
		rotation = (float) Math.toDegrees(rotation) + 90;
		
		//Determine if scaling is needed.
		float thicknessScale = Thickness / ImageThickness;
	    
		/*
		 * Segment
		 */
	    manipulate.reset();
		manipulate.setScale(tangent.length(), thicknessScale, segmentCenter.x, segmentCenter.y);
		manipulate.postRotate(rotation, segmentCenter.x, segmentCenter.y);
		
		matrix.set(manipulate);
	    matrix.postTranslate(A.x, A.y-segmentCenter.y);
	    
	    c.drawBitmap(BitmapList.get(1), matrix, paint);
	    
	    /*
	     * Half-circle
	     */
	    manipulate.reset();
	    manipulate.setScale(1, thicknessScale, capCenter.x, capCenter.y);
		manipulate.postRotate(rotation, capCenter.x, capCenter.y);
	    
	    matrix.set(manipulate);
	    matrix.postTranslate(A.x - capCenter.x, A.y - capCenter.y); //Reposition the bitmap accordingly to its width
	    
	    c.drawBitmap(BitmapList.get(0), matrix, paint);
    
	    /*
	     * Other half-circle
	     */
	    manipulate.reset();
	    manipulate.setScale(1, thicknessScale, capCenter.x, capCenter.y);
	    manipulate.postRotate((float)(rotation+180), capCenter.x, capCenter.y);
	    
	    matrix.set(manipulate);
	    matrix.postTranslate(B.x - capCenter.x, B.y - capCenter.y); //Reposition the bitmap accordingly to its width
	    c.drawBitmap(BitmapList.get(0), matrix, paint);
	}
}