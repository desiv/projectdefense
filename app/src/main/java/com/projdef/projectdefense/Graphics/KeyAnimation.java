/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Graphics;

import java.util.ArrayList;

import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.Utilities.Vector;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

/**
 * Class to handle simple key frame animation
 * @author Marius Savickas
 *
 */
public class KeyAnimation
{
	private ArrayList<Bitmap> animation;
	private int frame;
	private float x, y;
	private Paint paint = new Paint();
	
	private boolean scalable = false;
	private boolean center = false;
	private Matrix m;
	private float scale;
	
	public KeyAnimation()
	{
		frame = 0;
		animation = new ArrayList<Bitmap>();
		center = false;
	}
	
	public KeyAnimation(ArrayList<Bitmap> bitmaplist)
	{
		frame = 0;
		animation = bitmaplist;
		center = false;
	}
	
	/**
	 * Used mostly for optimization. Also, when scaling is chosen, sprite is automatically positioned
	 *  to draw around given position(center) regardless of the second option. If the options scalable is false
	 *  and center is true - centers the image, otherwise acts like a normal. 
	 * @param scalable	- flag for scalling
	 * @param center	- flag for center
	 */
	public KeyAnimation(boolean scalable, boolean center)
	{		
		frame = 0;
		animation = new ArrayList<Bitmap>();
		this.center = center;
		
		if(!scalable) return;
		
		this.scalable = scalable; 
		scale = 1;
		m = new Matrix();
	}
	
	/**
	 * Changes frame
	 * @param frame
	 */
	public void ChangeFrame(int frame)
	{
		if(animation.size() > frame)
			this.frame = frame;
	}
	
	/**
	 * Adds a frame to an array
	 * @param bitmap
	 */
	public void AddBitmap(Bitmap bitmap)
	{
		animation.add(bitmap);
	}
	
	
	/**
	 * Draws current frame
	 */
	public void Draw()
	{
		//Draw with matrix
		if(scalable)
		{
			m.setTranslate(x - (animation.get(frame).getWidth()*scale)/2, y - (animation.get(frame).getHeight()*scale)/2);
			m.preScale(scale, scale);
			Settings.Instance().Canvas.drawBitmap(animation.get(frame), m, paint);
		}
		//Draw centered, but not scaled.
		else if(center) Settings.Instance().Canvas.drawBitmap(animation.get(frame), x-animation.get(frame).getWidth()/2, y-animation.get(frame).getHeight()/2, paint);
		//Draw normally
		else Settings.Instance().Canvas.drawBitmap(animation.get(frame), x, y, paint);
	}
	
	/**
	 * Draws current frame into an arbitrary canvas
	 */
	public void Draw(Canvas c)
	{
		//Draw with matrix
		if(scalable)
		{
			m.setTranslate(x - (animation.get(frame).getWidth()*scale)/2, y - (animation.get(frame).getHeight()*scale)/2);
			m.preScale(scale, scale);
			c.drawBitmap(animation.get(frame), m, paint);
		}
		//Draw centered, but not scaled.
		else if(center) c.drawBitmap(animation.get(frame), x-animation.get(frame).getWidth()/2, y-animation.get(frame).getHeight()/2, paint);
		//Draw normally
		else c.drawBitmap(animation.get(frame), x, y, paint);
	}
	
	/**
	 * Changes scale
	 * @param s - float value of scale
	 */
	public void SetScale(float s)
	{
		scale = s;
	}
	
	/**
	 * Changes location of the drawable
	 * @param x
	 * @param y
	 */
	public void SetLocation(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Changes opacity. Value range [0,255]. There is a safety for out of range values.
	 * @param o
	 */
	public void SetOpacity(int o)
	{
		if(o >= 0 && o < 256) paint.setAlpha(o);
	}
	
	public int getOpacity()
	{
		return paint.getAlpha();
	}
	
	/**
	 * Gets location of this animation
	 * @return Vector with x and y
	 */
	public Vector getLocation()
	{
		return new Vector(x, y);
	}
	
	/**
	 * Get's a height of an image that this animation holds
	 * @param frame
	 * @return integer height of an image
	 */
	public int getImgHeight(int frame)
	{
		return animation.get(frame).getHeight();
	}
	
	/**
	 * Get's a width of an image that this animation holds
	 * @param frame
	 * @return integer width of an image
	 */
	public int getImgWidth(int frame)
	{
		return animation.get(frame).getWidth();
	}
	
	/**
	 * Returns current frame
	 * @return int
	 */
	public int getFrame()
	{
		return frame;
	}
}