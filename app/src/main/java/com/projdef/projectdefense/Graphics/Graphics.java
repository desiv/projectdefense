/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Graphics;

import java.util.ArrayList;

import com.projdef.projectdefense.Settings;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class Graphics
{
	//////////////////////////////////////////
	///////////////// Static ////////////////
	/////////////////////////////////////////
	
	public static Config BitmapFormat = Config.RGB_565;
	
	public static ArrayList<Bitmap> MAP_1 = new ArrayList<Bitmap>();
	
	//High physical dmg
	public static ArrayList<Bitmap> GUN_TURRET_P_1;
	//High magical dmg
	public static ArrayList<Bitmap> GUN_TORCH_1;
	
	public static ArrayList<Bitmap> FIRE_1;
	public static ArrayList<Bitmap> POISON_1;
	public static ArrayList<Bitmap> FREEZE_1;
	public static ArrayList<Bitmap> BULLET_1;
	
	//Lightning array containing 0 - half-circle, 1 - line, 2 - half-circle
	public static ArrayList<Bitmap> LIGHTNING;
	
	public static ArrayList<Bitmap> LASER;
	public static ArrayList<Bitmap> EXPLOSION;
	
	public static ArrayList<Bitmap> THUMBNAILS;
	public static ArrayList<Bitmap> CACHED_MAP_DATA;
	
	public static ArrayList<Bitmap> ENEMY_TEAR;
	public static ArrayList<Bitmap> ENEMY_BURN;
	public static ArrayList<Bitmap> ENEMY_STRONG_TEAR;
	public static ArrayList<Bitmap> ENEMY_STRONG_BURN;
	public static ArrayList<Bitmap> ENEMY_FAST_TEAR;
	public static ArrayList<Bitmap> ENEMY_FAST_BURN;
	public static ArrayList<Bitmap> ENEMY_SPECIAL_TEAR;
	public static ArrayList<Bitmap> ENEMY_SPECIAL_BURN;
	public static ArrayList<Bitmap> ENEMY_BOSS;
	
	//Menu
	public static ArrayList<Bitmap> MENU;
	public static ArrayList<Bitmap> NOTICE;
	public static ArrayList<Bitmap> TITLES;
	public static ArrayList<Bitmap> MAP_ALPHABET;
	public static ArrayList<Bitmap> COMMON;
	
	public static int[] GaussianBlur(int[] pixels, int width, int height, int pixRadius, double sigma)
	{
		if(pixRadius <= 0) return pixels;
		//Allocate temporary buffer to store transformed pixels
		int[] tmpBuffer = new int[width*height];
		int[] tmpBuffer2 = new int[width*height];
		
		//Allocate Gaussian matrix
		int sizeOfGMatrix = (pixRadius*2+1);
	    double[] gMatrix = new double[sizeOfGMatrix];
	    double sum = 0;
		
	    //Calculate Gaussian matrix    
    	for(int i = 0, x = -1*pixRadius; x <= pixRadius; x++, i++)
    	{
    		//Value
    		gMatrix[i] = (1/Math.sqrt(2*Math.PI*sigma*sigma))*Math.exp(-1*((x*x)/(2*sigma*sigma)));
    		//Summation
    		sum+= gMatrix[i];
    	}
    
	    
	    //If matrix summation isn't equal to 1 - adjust values
	    if(sum != (double)1)
	    {
	    	for(int i = 0; i < sizeOfGMatrix; i++)
	    	{
	    		gMatrix[i] = gMatrix[i] * (1/sum);
	    	}
	    }
		
		
	    ARGB weighedPixel = new ARGB();
		//Transform pixels horizontally
		for(int  y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				weighedPixel.alpha = 255;
				weighedPixel.red = 0;
				weighedPixel.green = 0;
				weighedPixel.blue = 0;
				
				//Get neighbourhood pixels
				for(int z = 0, j = (y*width+x)-pixRadius; j <= (y*width+x)+pixRadius; j++, z++)
				{
					if(j >= 0 && j < width*height-pixRadius)
					{
						weighedPixel.red 	+= Math.round(((pixels[j] >> 16) & 0xFF) * gMatrix[z]);
						weighedPixel.green	+= Math.round(((pixels[j] >> 8) & 0xFF) * gMatrix[z]);
						weighedPixel.blue 	+= Math.round((pixels[j] & 0xFF) * gMatrix[z]);
					}
					else
					{
						int pix = 0;
						if(j%width < width/2 && j < width*height-pixRadius) pix = (y*width+x)+pixRadius;
						else pix = (y*width+x)-pixRadius;
					
							
						weighedPixel.red	+= Math.round(((pixels[pix] >> 16) & 0xFF) * gMatrix[z]);
						weighedPixel.green 	+= Math.round(((pixels[pix] >> 8) & 0xFF) * gMatrix[z]);
						weighedPixel.blue 	+= Math.round((pixels[pix] & 0xFF) * gMatrix[z]);
					
						
					}
				}
				tmpBuffer[y*width+x] = Color.rgb(weighedPixel.red, weighedPixel.green, weighedPixel.blue);
			}	
			
		}
		
		//Transform pixels vertically
		for(int  x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				weighedPixel.alpha = 255;
				weighedPixel.red = 0;
				weighedPixel.green = 0;
				weighedPixel.blue = 0;
				
				//Get neighbourhood pixels
				for(int z = 0, j = (y*width+x)-pixRadius*width; j <= (y*width+x)+pixRadius*width; j+=width, z++)
				{
					if(j >= 0 && j < width*height-pixRadius)
					{
						weighedPixel.red 	+= Math.round(((tmpBuffer[j] >> 16) & 0xFF) * gMatrix[z]);
						weighedPixel.green	+= Math.round(((tmpBuffer[j] >> 8) & 0xFF) * gMatrix[z]);
						weighedPixel.blue 	+= Math.round((tmpBuffer[j] & 0xFF) * gMatrix[z]);
					}
					else
					{
						int pix = 0;
						if(j%width < width/2 && j < width*height-pixRadius) pix = (y*width+x)+pixRadius;
						else pix = (y*width+x)-pixRadius;
					
							
						weighedPixel.red	+= Math.round(((tmpBuffer[pix] >> 16) & 0xFF) * gMatrix[z]);
						weighedPixel.green 	+= Math.round(((tmpBuffer[pix] >> 8) & 0xFF) * gMatrix[z]);
						weighedPixel.blue 	+= Math.round((tmpBuffer[pix] & 0xFF) * gMatrix[z]);
					
						
					}
				}
				tmpBuffer2[y*width+x] = Color.rgb(weighedPixel.red, weighedPixel.green, weighedPixel.blue);
			}	
			
		}
		
		
		return tmpBuffer2;
	}
	
	//Reduces opacity of pixel array
	public static void Opacity(int[] pixels, int width, int height, int opacity)
	{
		for(int i = 0; i < width*height; i++)
				pixels[i] = Color.argb(opacity, (pixels[i] >> 16) & 0xFF, //Red
												(pixels[i] >> 8) & 0xFF, //Green
												 pixels[i] & 0xFF);		//Blue
	}
	
	/**
	 * Crops an image from a sprite sheet
	 * @param sprite 	- sprite sheet
	 * @param first_pixel_X
	 * @param first_pixel_Y
	 * @param img_width
	 * @param img_height
	 * @param scale		- cropped image scaling
	 * @return cropped and scaled Bitmap
	 */
	public static Bitmap cropImg(Bitmap sprite, int first_pixel_X, int first_pixel_Y, int img_width, int img_height, float scale)
	{
		Bitmap bmp = Bitmap.createBitmap(sprite, first_pixel_X, first_pixel_Y, img_width, img_height);
		
		if(img_width > 1 && img_height > 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()*scale), Math.round(bmp.getHeight()*scale), true);
		//Only scale width
		else if (img_width == 1 && img_height > 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()), Math.round(bmp.getHeight()*scale), true);
		//Only scale height
		else if (img_width > 1 && img_height == 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()*scale), Math.round(bmp.getHeight()), true);
		//Don't scale it
		
		else return bmp;
	}
	
	
	/**
	 * Loads a bitmap from resources and scales it if defined
	 * @param ID
	 * @param scale - scale the bitmap
	 * @return
	 */
	public static Bitmap loadImg(int ID, float scale)
	{	
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inDensity = Settings.Instance().screenDensityDpi;
		options.inScreenDensity = Settings.Instance().screenDensityDpi;
		options.inTargetDensity = Settings.Instance().screenDensityDpi;
		options.inScaled = false;
		options.inDither = true; 
		options.inPurgeable = false;
		//options.inPreferQualityOverSpeed = true;
		options.inPreferredConfig = Graphics.BitmapFormat;
		
		Bitmap tmpbmp = BitmapFactory.decodeResource(Settings.Instance().Context.getResources(), ID, options);
		
		Bitmap.Config cfg = tmpbmp.getConfig();
		
		if(cfg == null)
			cfg = Config.ARGB_8888;
		
		Bitmap bmp = tmpbmp.copy(cfg, true);
		//tmpbmp.recycle();
		
		if(bmp.getWidth() > 1 && bmp.getHeight() > 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()*scale), Math.round(bmp.getHeight()*scale), true);
		//Only scale width
		else if (bmp.getWidth() == 1 && bmp.getWidth() > 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()), Math.round(bmp.getHeight()*scale), true);
		//Only scale height
		else if (bmp.getWidth() > 1 && bmp.getHeight() == 1)
			return Bitmap.createScaledBitmap(bmp, Math.round(bmp.getWidth()*scale), Math.round(bmp.getHeight()), true);
		//Don't scale it
		else return bmp;
	}
	
	
	public static Bitmap scaleImg(Bitmap bitmap, float scale)
	{
		return Bitmap.createScaledBitmap(bitmap,  Math.round(bitmap.getWidth()*scale), Math.round(bitmap.getHeight()*scale), true);
	}
	

	static public void drawMultiLineText(String str, float y, Paint paint, Canvas canvas) 
	{
	   String[] lines = str.split("\n");
	   float txtSize = -paint.ascent() + paint.descent();       

	   if (paint.getStyle() == Style.FILL_AND_STROKE || paint.getStyle() == Style.STROKE){
	      txtSize += paint.getStrokeWidth(); //add stroke width to the text size
	   }
	   float lineSpace = txtSize * 0.2f;  //default line spacing

	   for (int i = 0; i < lines.length; ++i) {
	      canvas.drawText(lines[i], (Settings.Instance().screenWidth - paint.measureText(lines[i]))/2, y + (txtSize + lineSpace) * i, paint);
	   }
	}
};





class ARGB
{
	int alpha, red, green, blue;
};