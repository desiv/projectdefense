/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.util.HashSet;
import java.util.Set;

import com.projdef.projectdefense.Base.BaseEntity;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.ExtraInfo;
import com.projdef.projectdefense.Utilities.Telegram;
import com.projdef.projectdefense.Utilities.Timer;

/**
 * 
 * @author Marius Savickas
 *
 */
public class MessageDispatcher
{
	//Queued message container
	private Set<Telegram> _priorityList = new HashSet<Telegram>();
	
	Timer time = new Timer();
	
	//Private constructor prevents instantiation from other classes
	private MessageDispatcher(){}
	
	
	//Singleton initialization
	private static class SingletonHolder
	{
		public static final MessageDispatcher instance = new MessageDispatcher();
	}
	
	public static MessageDispatcher Instance()
	{
		return SingletonHolder.instance;
	}
	
	
	
	//Insert new message into queue
	public void InsertQueueMessage(Telegram msg)
	{
		//Check time. time must be larger then 0.5s
		if(msg.dispatchTime >= time.timeNow()+Common.MESSAGE_INSERT_MIN_DELAY_TIME)
		{
			//Insert the message
			_priorityList.add(msg);
		}
	}
	
	//This method calls the base game entity's messagehandle function
	public void Discharge(BaseEntity receiver, Telegram msg)
	{
		receiver.HandleMessage(msg);
	}
	
	
	
	
	//This method is used to create the message and call the discharge function
	public void SendMessage(long delay, int receiver, int sender, int msg, ExtraInfo extr)
	{
		//Create message
		Telegram message = new Telegram(receiver, sender, msg, extr);
		
		if(delay <= 0)
		{
			//Get the pointer to the receiver
			BaseEntity eReceiver = (BaseEntity)Settings.Instance().EntityManager.getEntityByID(receiver);
			
			//Check if the entity is valid, otherwise - discard the message
			if(eReceiver != null)
				//Send immediately
				Discharge(eReceiver, message);
		}
		else
		{
			Timer t = new Timer();
			
			//delayed message
			message.dispatchTime = delay + t.timeNow();
			
			InsertQueueMessage(message);
		}
	}
	
	
	

	//Dispatches delayed messages
	public void DispatchDelayedMessages()
	{
		for(Telegram t: _priorityList)
		{
			if(t.dispatchTime <= time.timeNow())
			{
				BaseEntity eReceiver = (BaseEntity)Settings.Instance().EntityManager.getEntityByID(t.Receiver);
				
				//Discharge queued message
				Discharge(eReceiver, t);
				
				//Remove message from the list
				_priorityList.remove(t);
			}
		}
	}
};