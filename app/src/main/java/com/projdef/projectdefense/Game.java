/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.io.IOException;
import java.util.ArrayList;
import com.projdef.projectdefense.AchvmContainer.Achievement;
import com.projdef.projectdefense.Base.BaseWorld;
import com.projdef.projectdefense.Entity.Gun;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Manager.EntityManager;
import com.projdef.projectdefense.Manager.OnScreenMsgManager;
import com.projdef.projectdefense.Manager.SpawnManager;
import com.projdef.projectdefense.Manager.TransactionManager;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.FpsCounter;
import com.projdef.projectdefense.Utilities.Grid;
import com.projdef.projectdefense.Utilities.Sound;
import com.projdef.projectdefense.Utilities.Timer;
import com.projdef.projectdefense.R;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import org.apache.commons.lang3.mutable.MutableLong;

/**
 * Main game class
 * @author Marius Savickas
 *
 */
public class Game
{	
	private Canvas c;
	private SurfaceHolder Sh;
	public int fps = 0;
	public int[] fpsTable = new int[Common.FPS_TABLE_MAX_SIZE];
	private int fpsTableIndex = 0;
	private double timeNow = 0;
	private double timeLastFrame = 0;
	public volatile boolean initialized = false;
	
	//Timers
	private Timer fpsTimer = new Timer();
	
	//private Messages _messages;
	public BaseWorld Menu;
	public BaseWorld MapSelect;
	public BaseWorld World;
	public BaseWorld About;
	public BaseWorld Score;
	public BaseWorld Lost;
	public BaseWorld Win;
	public BaseWorld WinScore;
	public BaseWorld WinUnlocks;
	public BaseWorld Transaction;
	public BaseWorld Notice;
	private BaseWorld curWorldState;
	
	//======================== Constructor ===================================
	public Game(SurfaceHolder surfaceholder)
	{
		//Sets context to a global value
		Settings.Instance().Game = this;
		Sh = surfaceholder;
		Sh.setFormat(PixelFormat.RGB_565);
	}
	
	
		
	
	//=========== onStart() ==========================
	//This is being called before the game loop begins
	public void Initialize()
	{	
		
		
		//Get screen parameters
		DisplayMetrics metrics = new DisplayMetrics();
		Settings.Instance().Activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		Settings.Instance().screenDensity = metrics.density;
		Settings.Instance().screenDensityDpi = metrics.densityDpi;
		Settings.Instance().screenHeight = metrics.heightPixels;
		Settings.Instance().screenWidth = metrics.widthPixels;
		
		//Adjust for different types of screen
		//if(Settings.Instance().isDisplayResolution(720, 1280))
		Settings.Instance().screenDensity = (float)metrics.widthPixels/320.0f;
		
		Settings S = Settings.Instance();
		/*
		 * Set scaling for sprites.
		 * 
		 * SpriteSheet sprites are 2x bigger than normal
		 */
		//0.5f is for the normal screens
		S.screenScaleSprites = 0.5f * S.screenDensity;
		
		/*
		 * Set scaling for Map.
		 * 
		 * Map is 1.5x bigger than normal
		 */
		S.screenScaleMap = (2.0f/3.0f) * S.screenDensity;
		
		/*
		 * Set scaling for Menu.
		 * 
		 * Menu is adjusted for the biggest screen possible.
		 * Clipping will be needed to display on the normal
		 * Menu background img is 480x854
		 */		
		S.screenScaleMenu = (2.0f/3.0f) * S.screenDensity;
		
		
		/*
		 * Count the number of maps.
		 */
		AssetManager assm = Settings.Instance().Context.getAssets();
		String[] filenames = null;
		try {
			filenames = assm.list("maps");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Settings.Instance().Activity.finish();
		}
		
		Settings.Instance().numberOfMaps = filenames.length;
		
		
		/*
		 * Load player data
		 */
		if(Player.Instance().LoadFile("player.dat")){}
		else Player.Instance().CreateFile("player.dat");
		
		Player.Instance().stats.get(0).lock = false;
		Player.Instance().stats.get(1).lock = false;
		Player.Instance().stats.get(2).lock = false;
		Player.Instance().stats.get(3).lock = false;
		Player.Instance().stats.get(4).lock = false;
		Player.Instance().stats.get(5).lock = false;
		Player.Instance().stats.get(6).lock = false;
		Player.Instance().stats.get(7).lock = false;
		Player.Instance().stats.get(8).lock = false;
		Player.Instance().stats.get(9).lock = false;
		
		Player.Instance().tierLock = 3;
		
		Settings.Instance().SpawnManager = new SpawnManager();
		Settings.Instance().EntityManager = new EntityManager();
		
		Settings.Instance().Sound = new Sound(Settings.Instance().Context);
		Settings.Instance().Sound.load(Settings.Instance().Context);
		
		Settings.Instance().Grid = new Grid(metrics.widthPixels, metrics.heightPixels, 1, 1);
		TransactionManager.Instance().reset();
		/*
		 * Initialize global stats
		 */
		Settings.Instance().ENTITY_ENEMY_DIAMETER = (int)(10);//*Settings.Instance().screenDensity);
		Settings.Instance().ENTITY_GUN_INPUT_BUILD_DISTANCE = (int)(30*Settings.Instance().screenDensity);
		Settings.Instance().ENTITY_TORCH_SHOT_DELAY = (int)(3);//*Settings.Instance().screenDensity);
		Settings.Instance().GOAL_REACH_DISTANCE	= (int)(5*Settings.Instance().screenDensity);
		Settings.Instance().ENTITY_FIRE_DIE_DISTANCE = (int)(55*Settings.Instance().screenDensity);
		Settings.Instance().ENTITY_FIRE_SCALEANIMATION_DISTANCE = (int)(10*Settings.Instance().screenDensity);
		
		Settings.Instance().ENTITY_GUN_ROTATION_PREDICT_LOC = 10;
		Settings.Instance().ENTITY_GUN_TURRET_ROTATION_PREDICT_LOC = 7;
		
		if(Settings.Instance().screenDensity > 1.0f)
		{
			Settings.Instance().ENTITY_GUN_ROTATION_PREDICT_LOC = Math.round(10*Settings.Instance().screenDensity);
			Settings.Instance().ENTITY_GUN_TURRET_ROTATION_PREDICT_LOC = Math.round(7*Settings.Instance().screenDensity);
		}
		
		Settings.Instance().SPAWN_DELAY_FIRST = 15*Common.FPS_EQUALTOSECOND;  //10 sec
		Settings.Instance().SPAWN_DELAY_WAVE = 9*Common.FPS_EQUALTOSECOND; //Math.round(500*Settings.Instance().screenDensity);
		Settings.Instance().SPAWN_DELAY_FOE = 2*Common.FPS_EQUALTOSECOND;
		
		CreateAchievements();
		
		/*
		 * Load graphics
		 */
		LoadGraphics();
		
		Settings.Instance().BGyOffset = (Settings.Instance().screenHeight - Graphics.MAP_1.get(0).getHeight())/2;
		
		//Create buffer
		Settings.Instance().BBuffer = Bitmap.createBitmap(Settings.Instance().screenWidth, Settings.Instance().screenHeight, Graphics.BitmapFormat);
		//Create canvas
		Settings.Instance().Canvas = new Canvas(Settings.Instance().BBuffer);
		Settings.Instance().Canvas.setDensity(Settings.Instance().screenDensityDpi);
		
		//Initialize
		World 		= new World();
		Menu 		= new Menu();
		Lost 		= new Lost();
		Win 		= new Win();
		MapSelect 	= new MapSelect();
		About 		= new About();
		Transaction = new Transaction();
		Notice 		= new Notice();
		Score 		= new Score();
		WinScore 	= new WinScore();
		WinUnlocks 	= new WinUnlocks();
		
		//Changes state
		ChangeState(Menu);
		
		Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		
		initialized = true;
	}
	
	public void Uninitialize()
	{
		Sh = null;
		
		fpsTimer.releaseMemory();
		fpsTimer = null;
		
		Graphics.MAP_1.clear();
		Graphics.GUN_TURRET_P_1.clear();
		Graphics.GUN_TORCH_1.clear();
		Graphics.FIRE_1.clear();
		Graphics.POISON_1.clear();
		Graphics.FREEZE_1.clear();
		Graphics.BULLET_1.clear();
		Graphics.LIGHTNING.clear();
		Graphics.LASER.clear();
		Graphics.EXPLOSION.clear();
		Graphics.THUMBNAILS.clear();
		Graphics.CACHED_MAP_DATA.clear();
		Graphics.ENEMY_TEAR.clear();
		Graphics.ENEMY_BURN.clear();
		Graphics.ENEMY_STRONG_TEAR.clear();
		Graphics.ENEMY_STRONG_BURN.clear();
		Graphics.ENEMY_FAST_TEAR.clear();
		Graphics.ENEMY_FAST_BURN.clear();
		Graphics.ENEMY_SPECIAL_TEAR.clear();
		Graphics.ENEMY_SPECIAL_BURN.clear();
		Graphics.ENEMY_BOSS.clear();
		Graphics.MENU.clear();
		Graphics.NOTICE.clear();
		Graphics.TITLES.clear();
		Graphics.MAP_ALPHABET.clear();
		Graphics.COMMON.clear();
		
		Settings.Instance().BBuffer = null;
		Settings.Instance().Canvas = null;
		Settings.Instance().Context = null;
		
		if(((World)World).Options != null)
			((World)World).Options.Uninitialize();
		
		if(((World)World).Map != null)
			((World)World).Map.Uninitialize();
		
		if(((World)World).GunList != null)
			for(Gun G: ((World)World).GunList)
				if(G != null)
					G.releasememory();
		
		if(((World)World).GunList != null)
			((World)World).GunList.clear();
		
		Settings.Instance().Game = null;
		Settings.Instance().Activity = null;
		Settings.Instance().Grid = null;
		Settings.Instance().SpawnManager.Unitilaize();
		Settings.Instance().SpawnManager = null;
		Settings.Instance().EntityManager = null;
		Settings.Instance().Sound = null;
		
		World 		= null;
		Menu 		= null;
		Lost 		= null;
		Win 		= null;
		MapSelect 	= null;
		About 		= null;
		Transaction = null;
		Notice 		= null;
		Score 		= null;
		WinScore 	= null;
		WinUnlocks 	= null;
		
		initialized = false;
	}
	
	public void onResume()
	{
		if(curWorldState == Menu && Menu != null)
			Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		else if(curWorldState == About && About != null)
			Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		else if(curWorldState == MapSelect && MapSelect != null)
			Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		else if(curWorldState == Score && Score != null)
			Settings.Instance().Sound.playMenu(Settings.Instance().Context);
		else if (curWorldState == World && World != null)
			((World)World).reloadSound();
		
		
		if(curWorldState != null)
			curWorldState.Draw = true;
	}
	
	
	public void onPause()
	{
		if(World != null)
			((World)World).unloadSound();
		
		if(Settings.Instance().Sound != null)
			Settings.Instance().Sound.release();
	}
	
	/**
	 * Draws and simulates physics
	 */
	public void Update()
	{
		//Increment total frames
		if(!Settings.Instance().stopFPSCount)
			FpsCounter.Instance().CountFps();
		
		synchronized(Common.LOCK){
			onDraw();
			onPhysics();
		}
		
		//Process sound
		Settings.Instance().Sound.Process();
		
		//Calculate fps
		timeLastFrame = timeNow;
		timeNow = fpsTimer.timeNow();
		double delay = timeNow - timeLastFrame;
		int FPS = (int) (1000/delay);
		
		if(fpsTableIndex < Common.FPS_TABLE_MAX_SIZE)
		{
			fpsTable[fpsTableIndex] = FPS;
			fpsTableIndex++;
		}
		else fpsTableIndex = 0;
		
		int sum = 0;
		
		for(int i = 0; i < Common.FPS_TABLE_MAX_SIZE; i++)
		{
			sum+= fpsTable[i];
			fps = sum/Common.FPS_TABLE_MAX_SIZE;
		}
	}
	
	/**
	 * Changes game state
	 * @param state - state to change
	 */
	public void ChangeState(BaseWorld state)
	{
		if(curWorldState != null) curWorldState.Exit();
		curWorldState = state;
		curWorldState.Initialize();
	}
	
	/**
	 * Gets current state
	 * @return
	 */
	public BaseWorld getCurrentState()
	{
		return curWorldState;
	}
	
	public void setDrawState(boolean state)
	{
		if(curWorldState != null)
			curWorldState.Draw = true;
	}
	
	/**
	 * Main draw function that draws everything on screen.
	 */
	private void onDraw()
	{
		//Draw state if the flag is set
		if(curWorldState.Draw)
		{
			c = null;
			
			try
			{
				/* We need to lock canvas before drawing */
				c = Sh.lockCanvas(null);
				
				if(c != null)
				{
					c.setDensity(Settings.Instance().screenDensityDpi);
					//Everything that needs to be drawn goes here		
					curWorldState.Draw(c);
				}
					
			} finally
			  {
					if(c != null) Sh.unlockCanvasAndPost(c);
			  }
		}
	}
	
	
	/**
	 * Simulates current state's physics
	 */
	private void onPhysics()
	{	
			curWorldState.Physics();
			
			TransactionManager.Instance().Process();
	}
	
	public void onTouch(float x, float y, int event)
	{
			if(curWorldState == null) return;
		
			switch(event)
			{
			case MotionEvent.ACTION_UP:
				curWorldState.HandleTouchRelease(x, y);
				break;
			
			case MotionEvent.ACTION_DOWN:
				curWorldState.HandleTouchPress(x, y);
				break;
				
			case MotionEvent.ACTION_MOVE:
				curWorldState.HandleMovement(x, y);
				break;
			
			};
	}
	
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		if(event == null || curWorldState == null) return true;
		
		return curWorldState.HandlePhysicalKey(keyCode, event);
	}
	
	
	private void CreateAchievements()
	{
		//For easier reading
		AchvmContainer A = AchvmContainer.Instance();
		Settings S = Settings.Instance();
		
		A.deleteAll();
		
		/*
		 * Properties
		 */
		A.addProperty("gold", S.GOLD);
		A.addProperty("kills", S.KILLS);
		A.addProperty("aliveHumans", S.aliveHumans);
		A.addProperty("releasedZombies", S.releasedZombies);
		A.addProperty("aliveZombies", S.aliveZombies);
		A.addProperty("totalZombies", S.totalZombies);
		A.addProperty("totalHumans", S.totalHumans);
		
		/*
		 * Achievements
		 */
		
		//Winning
		//If all of the zombies went out and there are some humans alive
		Achievement achievement =
		A.new Achievement("win",
						  A.getProperty("releasedZombies"), ">=", A.getProperty("totalZombies"), //>=
						  A.new ChangeToWin(),
						  false);
			
			achievement.addAdditional(A.getProperty("aliveHumans"), ">", new MutableLong(0));
			achievement.addAdditional(A.getProperty("aliveZombies"), "==", new MutableLong(0));
		A.addAchievement(achievement);
		
		//Losing
		achievement =
		A.new Achievement("lose",
						  A.getProperty("aliveHumans"), "<=", new MutableLong(0), //<=
						  A.new ChangeToLose(),
						  false);	
		
		A.addAchievement(achievement);
	}
	
	//======================= LoadGraphics =================
	private void LoadGraphics()
	{
		Graphics.MENU = new ArrayList<Bitmap>();
		//Background
		Graphics.MENU.add(Graphics.loadImg(R.drawable.bg_menu, Settings.Instance().screenScaleMenu));
		Graphics.MENU.add(Graphics.loadImg(R.drawable.bg_mapselect_map, Settings.Instance().screenScaleMenu));
		Graphics.MENU.add(Graphics.loadImg(R.drawable.menu_common_bg, Settings.Instance().screenScaleMenu));
		Graphics.MENU.add(Graphics.loadImg(R.drawable.bg_mapselect_map_pressed, Settings.Instance().screenScaleMenu));
		
		Graphics.COMMON = new ArrayList<Bitmap>();
		Graphics.COMMON.add(Graphics.loadImg(R.drawable.lock, Settings.Instance().screenScaleSprites));
		Graphics.COMMON.add(Graphics.loadImg(R.drawable.x, Settings.Instance().screenScaleSprites));
		Graphics.COMMON.add(Graphics.loadImg(R.drawable.star, Settings.Instance().screenScaleSprites));
		Graphics.COMMON.add(Graphics.loadImg(R.drawable.star_grey, Settings.Instance().screenScaleSprites));
		
		Graphics.TITLES = new ArrayList<Bitmap>();
		//Background
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_projectdefense, Settings.Instance().screenScaleMenu));
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_mapselect, Settings.Instance().screenScaleMenu));
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_win, Settings.Instance().screenScaleMenu));
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_lose, Settings.Instance().screenScaleMenu));
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_about, Settings.Instance().screenScaleMenu));
		//5
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_score, Settings.Instance().screenScaleMenu));
		Graphics.TITLES.add(Graphics.loadImg(R.drawable.title_stars, Settings.Instance().screenScaleMenu));
		
		Graphics.MAP_1 = new ArrayList<Bitmap>();
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.bg0, Settings.Instance().screenScaleMap));	//0
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.ba1, Settings.Instance().screenScaleSprites));			//1
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.ba1_h, Settings.Instance().screenScaleSprites));			//2
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.screen_effect_blood_top, Settings.Instance().screenScaleMap)); //3
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.screen_effect_blood_bottom, Settings.Instance().screenScaleMap)); //4
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.screen_effect_blood_left, Settings.Instance().screenScaleMap));	//5
		Graphics.MAP_1.add(Graphics.loadImg(R.drawable.screen_effect_blood_right, Settings.Instance().screenScaleMap));	//6
		
		
		Graphics.MAP_ALPHABET = new ArrayList<Bitmap>();
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map0, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map1, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map2, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map3, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map4, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map5, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map6, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map7, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map8, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.number_map9, Settings.Instance().screenScaleSprites));
		
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.colon, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.exclamationmark, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.questionmark, Settings.Instance().screenScaleSprites));
		
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_a, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_b, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_c, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_d, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_e, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_f, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_g, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_h, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_i, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_j, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_k, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_l, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_m, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_n, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_o, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_p, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_q, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_r, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_s, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_t, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_u, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_v, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_w, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_x, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_y, Settings.Instance().screenScaleSprites));
		Graphics.MAP_ALPHABET.add(Graphics.loadImg(R.drawable.alphabet_z, Settings.Instance().screenScaleSprites));
		
		
		Graphics.NOTICE = new ArrayList<Bitmap>();
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_top, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_left, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_right, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button_pressed, Settings.Instance().screenScaleMap));
		//5
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button_yes, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button_yes_pressed, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button_no, Settings.Instance().screenScaleMap));
		Graphics.NOTICE.add(Graphics.loadImg(R.drawable.bg_notice_button_no_pressed, Settings.Instance().screenScaleMap));
		
		Graphics.EXPLOSION = new ArrayList<Bitmap>();
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion1, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion2, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion3, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion4, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion5, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion6, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion7, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion8, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion9, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion10, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion11, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion12, Settings.Instance().screenScaleMap/2));
		Graphics.EXPLOSION.add(Graphics.loadImg(R.drawable.explosion13, Settings.Instance().screenScaleMap/2));
		
		/*
		 * Gun turret animation
		 */
		//Bullet 1 lvl
		Graphics.GUN_TURRET_P_1 = new ArrayList<Bitmap>();
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_bullet1, Settings.Instance().screenScaleSprites));
		//Tesla 1 lvl // 1
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_tesla_coil1, Settings.Instance().screenScaleSprites));
		//Laser 1 lvl //2
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_laser1, Settings.Instance().screenScaleSprites));
		//Bullet 2 lvl	//3
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_bullet2, Settings.Instance().screenScaleSprites));
		//Bullet 3 lvl //4
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_bullet3, Settings.Instance().screenScaleSprites));
		//Tesla 2 lvl // 5
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_tesla_coil2, Settings.Instance().screenScaleSprites));
		//Laser 2 lvl //6
		Graphics.GUN_TURRET_P_1.add(Graphics.loadImg(R.drawable.turret_laser2, Settings.Instance().screenScaleSprites));
		
		
		/*
		 * Torch animation
		 */
		Graphics.GUN_TORCH_1 = new ArrayList<Bitmap>();
		Graphics.GUN_TORCH_1.add(Graphics.loadImg(R.drawable.torch_1, Settings.Instance().screenScaleSprites));
		Graphics.GUN_TORCH_1.add(Graphics.loadImg(R.drawable.torch_2, Settings.Instance().screenScaleSprites));
		Graphics.GUN_TORCH_1.add(Graphics.loadImg(R.drawable.torch_3, Settings.Instance().screenScaleSprites));
		
		/*
		 * Fire animation
		 */
		Graphics.FIRE_1 = new ArrayList<Bitmap>();
		Graphics.FIRE_1.add(Graphics.loadImg(R.drawable.fire1, Settings.Instance().screenScaleMap));
		Graphics.FIRE_1.add(Graphics.loadImg(R.drawable.fire2, Settings.Instance().screenScaleMap));
		Graphics.FIRE_1.add(Graphics.loadImg(R.drawable.fire3, Settings.Instance().screenScaleMap));
		Graphics.FIRE_1.add(Graphics.loadImg(R.drawable.fire4, Settings.Instance().screenScaleMap));
		/*
		 * Freeze animation
		 */
		Graphics.FREEZE_1 = new ArrayList<Bitmap>();
		Graphics.FREEZE_1.add(Graphics.loadImg(R.drawable.frost, Settings.Instance().screenScaleMap));

		
		/*
		 * Poison animation
		 */
		Graphics.POISON_1 = new ArrayList<Bitmap>();
		Graphics.POISON_1.add(Graphics.loadImg(R.drawable.poison, Settings.Instance().screenScaleMap));
		
		/*
		 * Bullet
		 */
		Graphics.BULLET_1 = new ArrayList<Bitmap>();
		Graphics.BULLET_1.add(Graphics.loadImg(R.drawable.bullet1, Settings.Instance().screenScaleMap));
		
		
		/*
		 * Lightning
		 */
		Graphics.LIGHTNING = new ArrayList<Bitmap>();
		Graphics.LIGHTNING.add(Graphics.loadImg(R.drawable.half_circle1_light, Settings.Instance().screenScaleMap));
		Graphics.LIGHTNING.add(Graphics.loadImg(R.drawable.segment_light, Settings.Instance().screenScaleMap));

		/*
		 * Laser
		 */
		Graphics.LASER = new ArrayList<Bitmap>();
		Graphics.LASER.add(Graphics.loadImg(R.drawable.half_circle1_laser, Settings.Instance().screenScaleMap));
		Graphics.LASER.add(Graphics.loadImg(R.drawable.segment_laser, Settings.Instance().screenScaleMap));
		
		/*
		 * Enemy animation
		 */
		Graphics.ENEMY_TEAR = new ArrayList<Bitmap>();
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear, Settings.Instance().screenScaleSprites));
		//Eye Close 1 - 4
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_eyeclose_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_eyeclose_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_eyeclose_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_eyeclose_4, Settings.Instance().screenScaleSprites));	
		//Cold 5 - 6
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_frozen, Settings.Instance().screenScaleSprites));
		//Poison 7
		Graphics.ENEMY_TEAR.add(Graphics.loadImg(R.drawable.tear_poison, Settings.Instance().screenScaleSprites));
		
		
		
		Graphics.ENEMY_BURN = new ArrayList<Bitmap>();
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn, Settings.Instance().screenScaleSprites));
		//Color change 1-6
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_4, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_5, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_6, Settings.Instance().screenScaleSprites));
		//6
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_color_7, Settings.Instance().screenScaleSprites));
		//Cold 7-8
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_frozen, Settings.Instance().screenScaleSprites));
		//Poison 9
		Graphics.ENEMY_BURN.add(Graphics.loadImg(R.drawable.burn_poison, Settings.Instance().screenScaleSprites));
		
		
		
		Graphics.ENEMY_STRONG_TEAR = new ArrayList<Bitmap>();
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st, Settings.Instance().screenScaleSprites));
		//Eyes color change 1 - 4
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_eye_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_eye_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_eye_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_eye_4, Settings.Instance().screenScaleSprites));
		//Cold 5 - 6
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_frozen, Settings.Instance().screenScaleSprites));
		//Poison 7
		Graphics.ENEMY_STRONG_TEAR.add(Graphics.loadImg(R.drawable.st_poison, Settings.Instance().screenScaleSprites));
		
		
		Graphics.ENEMY_STRONG_BURN = new ArrayList<Bitmap>();
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb, Settings.Instance().screenScaleSprites));
		//Mouth movement 1-4
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_mouth_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_mouth_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_mouth_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_mouth_4, Settings.Instance().screenScaleSprites));
		//Cold 5 - 6
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_frozen, Settings.Instance().screenScaleSprites));
		//Poison 7
		Graphics.ENEMY_STRONG_BURN.add(Graphics.loadImg(R.drawable.sb_poison, Settings.Instance().screenScaleSprites));
		
		
		Graphics.ENEMY_FAST_TEAR = new ArrayList<Bitmap>();
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft, Settings.Instance().screenScaleSprites));
		//Mouth 1 - 4
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_mouth_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_mouth_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_mouth_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_mouth_4, Settings.Instance().screenScaleSprites));
		//Cold 5 - 6
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_frozen, Settings.Instance().screenScaleSprites));
		//Poison
		Graphics.ENEMY_FAST_TEAR.add(Graphics.loadImg(R.drawable.ft_poison, Settings.Instance().screenScaleSprites));
		
		
		
		Graphics.ENEMY_FAST_BURN = new ArrayList<Bitmap>();
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb, Settings.Instance().screenScaleSprites));
		//Forehead animation 1 - 7
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_4, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_5, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_6, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_forehead_7, Settings.Instance().screenScaleSprites));
		//Cold 8 - 9
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_frozen, Settings.Instance().screenScaleSprites));
		//Poison 10
		Graphics.ENEMY_FAST_BURN.add(Graphics.loadImg(R.drawable.fb_poison, Settings.Instance().screenScaleSprites));
	
	
		Graphics.ENEMY_SPECIAL_TEAR = new ArrayList<Bitmap>();
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt, Settings.Instance().screenScaleSprites));
		//Mouth animation 1 - 4
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_artery_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_artery_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_artery_3, Settings.Instance().screenScaleSprites));
		//Cold 4 - 5
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_frozen, Settings.Instance().screenScaleSprites));
		// 6
		Graphics.ENEMY_SPECIAL_TEAR.add(Graphics.loadImg(R.drawable.spt_poison, Settings.Instance().screenScaleSprites));
		
		
		Graphics.ENEMY_SPECIAL_BURN = new ArrayList<Bitmap>();
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb, Settings.Instance().screenScaleSprites));
		//Eye color change 1 - 12
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_4, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_5, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_6, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_7, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_8, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_9, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_10, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_11, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_eye_12, Settings.Instance().screenScaleSprites));
		//Cold 13 - 14
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_frozen, Settings.Instance().screenScaleSprites));
		//Poison 15
		Graphics.ENEMY_SPECIAL_BURN.add(Graphics.loadImg(R.drawable.spb_poison, Settings.Instance().screenScaleSprites));
	
		
		Graphics.ENEMY_BOSS = new ArrayList<Bitmap>();
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss, Settings.Instance().screenScaleSprites));
		//Artery animation 1 - 3
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_mouth_1, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_mouth_2, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_mouth_3, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_mouth_4, Settings.Instance().screenScaleSprites));
		//Cold 5 - 6
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_frost, Settings.Instance().screenScaleSprites));
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_frozen, Settings.Instance().screenScaleSprites));
		//Cold 7
		Graphics.ENEMY_BOSS.add(Graphics.loadImg(R.drawable.boss_poison, Settings.Instance().screenScaleSprites));
		
		
		Bitmap bmp;
		float scaleTo;
		//To which size to downsize on a normal density
		int size = 50; //Px
		
		/*
		 * Thumbnails
		 */
		Graphics.THUMBNAILS = new ArrayList<Bitmap>();
		
		//Lightning	//0
		bmp = Graphics.loadImg(R.drawable.lightning_thumb, 1);
		scaleTo = (size*Settings.Instance().screenDensity)/bmp.getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(bmp, scaleTo));
		
		//Laser	//1
		bmp = Graphics.loadImg(R.drawable.laser_thumb, 1);
		scaleTo = (size*Settings.Instance().screenDensity)/bmp.getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(bmp, scaleTo));
		
		//Gun turret	//2
		scaleTo = (size*Settings.Instance().screenDensity)/Graphics.GUN_TURRET_P_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.GUN_TURRET_P_1.get(0), scaleTo));
		
		//Torch	//3
		scaleTo = (size*Settings.Instance().screenDensity)/Graphics.GUN_TORCH_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.GUN_TORCH_1.get(0), scaleTo));
		
		//Fire	//4
		scaleTo = (size*Settings.Instance().screenDensity)/Graphics.FIRE_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.FIRE_1.get(0), scaleTo));
		
		//Freeze	//5
		scaleTo = (size*Settings.Instance().screenDensity)/Graphics.FREEZE_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.FREEZE_1.get(0), scaleTo));
		
		//Poison	//6	
		scaleTo = (size*Settings.Instance().screenDensity)/Graphics.POISON_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.POISON_1.get(0), scaleTo));
		
		//Bullet	//7
		scaleTo = ((size-20)*Settings.Instance().screenDensity)/Graphics.BULLET_1.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.BULLET_1.get(0), scaleTo));
		
		//Lock	//8
		scaleTo = ((size-10)*Settings.Instance().screenDensity)/Graphics.COMMON.get(0).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.COMMON.get(0), scaleTo));
		
		//X	//9
		scaleTo = ((size-20)*Settings.Instance().screenDensity)/Graphics.COMMON.get(1).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.COMMON.get(1), scaleTo));
		
		//Star	//10
		scaleTo = ((size-30)*Settings.Instance().screenDensity)/Graphics.COMMON.get(2).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.COMMON.get(2), scaleTo));
		
		//Star grey	//11
		scaleTo = ((size-30)*Settings.Instance().screenDensity)/Graphics.COMMON.get(3).getWidth();
		Graphics.THUMBNAILS.add(Graphics.scaleImg(Graphics.COMMON.get(3), scaleTo));
		

		
		Graphics.CACHED_MAP_DATA = new ArrayList<Bitmap>();
		//Cached "GOLD" bitmap
		Graphics.CACHED_MAP_DATA.add(OnScreenMsgManager.Instance().MakeBitmap("GOLD: ", Common.MAP_STATS_WORD_SIZE*Settings.Instance().screenDensity));
		//Cached gold bitmap
		Graphics.CACHED_MAP_DATA.add(OnScreenMsgManager.Instance().MakeBitmap("0", Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity));
		//Cached "HUMANS" bitmap
		Graphics.CACHED_MAP_DATA.add(OnScreenMsgManager.Instance().MakeBitmap("HUMANS: ", Common.MAP_STATS_WORD_SIZE*Settings.Instance().screenDensity+2*Settings.Instance().screenDensity));
		//Cached "HUMANS" bitmap
		Graphics.CACHED_MAP_DATA.add(OnScreenMsgManager.Instance().MakeBitmap("0", Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity));
	}
	
	
};