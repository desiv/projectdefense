/* 
 * Copyright  2013 by Marius Savickas, all rights reserved
 * 
 */


package com.projdef.projectdefense;

import com.projdef.projectdefense.Manager.TransactionManager;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.Timer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;


/**
 * Main class
 * @author Marius Savickas 
 *
 */
public class ProjectDefenseActivity extends Activity {
	GameHolder GameHolder = null;

	/** Called when the activity is first created. */
	
    
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
   
        /* Removes title */
    	requestWindowFeature(Window.FEATURE_NO_TITLE); 	
    	/* Removes notification bar */
    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	/* Disable rotation to landscape */
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    	//Keek screen always on.
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	setVolumeControlStream(AudioManager.STREAM_MUSIC);
    	
    	Settings.Instance().Activity = this;
    	Settings.Instance().Context = this;
    	
    	//Create game thread
    	GameHolder = new GameHolder(this);
    	setContentView(GameHolder);
    	
    	//Log.e("", "Create");
    }
	
	 @Override
	protected void onStart()
    {
		super.onStart();
    	
    	GameHolder.StartThread();
    	//Log.e("", "Start");
    }
	
	@Override
	protected void onRestart()
	{
		super.onRestart();
		//Log.e("", "Restart");
	}
	 
	
	@Override
    protected void onResume()
    {
    	super.onResume();
    	
    	//Check if everything was loaded
    	if(Settings.Instance().Sound != null)
    		Settings.Instance().Sound.load(this);
    	
    	if(GameHolder.Game != null)
    		GameHolder.Game.onResume();
    	
    	GameHolder.GThread.setPause(false);
    	GameHolder.GThread.interrupt(); //Interrupt sleep
    	//Log.e("", "Resume");
    }
	
	
    //When user leaves activity'
    @Override
    protected void onPause()
    {
    	super.onPause();
    	
    	if(Settings.Instance().Sound != null)
    		Settings.Instance().Sound.unload();
    	
    	GameHolder.Game.onPause();
    	GameHolder.GThread.setPause(true);
    	
    	Settings S = Settings.Instance();
		Notice notice = (Notice)Settings.Instance().Game.Notice;
		
		if(S.Game.getCurrentState() == S.Game.World && S.Game.World != null)
		{
			notice.setupStates(S.Game.getCurrentState(), S.Game.Menu, true, true);
			notice.Text("Do you want to exit?");
			notice.Text(" ");
			notice.Text("All the progress will be lost.");
	    	TransactionManager.Instance().doSimple(GameHolder.Game.Notice);
		}
		
		//Log.e("", "Pause");
    }
    
  
    
    @Override
    protected void onStop()
    {
    	super.onStop();
    	
    	if(Settings.Instance().Sound != null)
    		Settings.Instance().Sound.release();
    	
    	GameHolder.StopThread();
//    	Log.e("", "Stop");
    }
    
    
    @Override
    protected void onDestroy()
    {
    	super.onDestroy();	
    	GameHolder.Game.Uninitialize();
    	GameHolder.Uninitialize();
    	GameHolder = null;
    	
    	Settings.Instance().Activity = null;
    	Settings.Instance().Context = null;
    	
//    	Log.e("", "Destroy");
    }
   

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(event == null || GameHolder == null || GameHolder.Game == null) return true;
		
		super.onTouchEvent(event);
		synchronized(Common.LOCK){
			GameHolder.Game.onTouch(event.getX(), event.getY(), event.getAction());
		}
		return true;
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		synchronized(Common.LOCK){
			if(GameHolder.Game.onKeyUp(keyCode, event))
				return true;
		}
				
		return super.onKeyUp(keyCode, event);
	}
}

class GameHolder extends SurfaceView implements SurfaceHolder.Callback
{
	GameLoopThread GThread = null;
	Game Game = null;
	Context context = null;
	
	/* Game constructor */
	public GameHolder(Context context)
	{
		/* Needed for the SurfaceView extend */
		super(context);
		getHolder().addCallback(this); //Gets holder
		this.context = context;
		
		//Initialize object
		Game = new Game(getHolder());
		
		GameLoadingThread GLT = new GameLoadingThread(Game);
		GLT.start();
	}
	
	public void StartThread()
	{
		GThread = new GameLoopThread(Game);
		GThread.setRun(true);
		GThread.setPause(false);
		GThread.start();
	}
	
	
	public void StopThread()
	{
		GThread.setRun(false);
		GThread.setPause(false);
		//Interrupt any sleeping
		GThread.interrupt();
		
		//Wait for thread to close
		try {
			GThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		GThread = null;
	}
	
	public void Uninitialize()
	{
		GThread = null;
		Game = null;
		context = null;
	}
	
	/* On SurfaceView creation we start our game loop thread */
	
	public void surfaceCreated(SurfaceHolder holder)
	{
		Paint paint = new Paint();
		
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(true);
		paint.setColor(0xff42b8fb);
		paint.setTextSize(20);
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		int width = display.getWidth();
		int height = display.getHeight();
		
		//Draw "Loading"
		Canvas c = null;
		String str = "Loading..";
		try
		{
			c = getHolder().lockCanvas(null);
			
			if(c != null)
				c.drawText(str, Common.hcenter(width, str, paint), Common.vcenter(height, paint), paint);
				
		} finally
		  {
				if(c != null) getHolder().unlockCanvasAndPost(c);
		  }
		
		Game.setDrawState(true);
	}
	
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}
	public void surfaceDestroyed(SurfaceHolder holder){}

	
	
	
	
	
	
	
	
	
	
};



//=============== Game loop thread ===================================
//
//===================================================================
class GameLoopThread extends Thread
{
	private volatile boolean pause = false;
	private volatile boolean run = false;
	private Game Game = null;
	Timer fpsSlower = new Timer();
	
	/* Constructor */
	public GameLoopThread(Game game){Game = game;}

	@Override
	public void run()
	{	
		//Wait for initializing
		while(!Game.initialized) 
			Sleep(100);
		
		
		/* Main game loop */
		while(run)
		{
			while(!pause && run)
			{	
				if(fpsSlower.ifTimePassed(Common.GAME_SPEED))
					Game.Update();
			}

			Sleep(5000);
		}
		
		Game = null;
	}
	
	public void setPause(boolean p)
	{
		pause = p;
	}
	
	public void setRun(boolean r)
	{
		run = r;
	}
	
	public boolean isRunning()
	{
		return run;
	}
	
	protected void Sleep(long milisecs)
	{
		try
		{
			sleep(milisecs);
		}catch(InterruptedException e){}
	}
};

class GameLoadingThread extends Thread
{
	private Game Game = null;
	
	GameLoadingThread(Game game)
	{
		Game = game;
	}
	
	@Override
	public void run()
	{
		if(Game != null)
		{
			Game.Initialize();
		}
		
		Game = null;
	}
};
