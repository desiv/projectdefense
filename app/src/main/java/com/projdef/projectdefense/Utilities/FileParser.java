/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

public class FileParser
{
	private byte[] buffer;
	private int offset;
	
	//Copies the buffer
	public FileParser(byte[] b)
	{
		buffer = b;
	}
	
	public short ReadByte()
	{
		offset++;
		return (short)(buffer[offset-1] & 0x00FF);
	}
	
	
	public byte ReadByteSigned()
	{
		offset++;
		return (byte)buffer[offset-1];
	}
	
	public int ReadWord()
	{
		offset+=2;
		
		byte byte1 = buffer[offset-1];
		byte byte2 = buffer[offset-2];

		return ((byte1 << 8) | (byte2 & 0x00FF)) & 0x0000FFFF;
	}
	
	public short ReadWordSigned()
	{
		offset+=2;
		
		byte byte1 = buffer[offset-1];
		byte byte2 = buffer[offset-2];
		
		return (short)((byte1 << 8) | (byte2 & 0x00FF));
	}
	
	public long ReadDWord()
	{
		offset+=4;
		byte b1 = buffer[offset-1];
		byte b2 = buffer[offset-2];
		byte b3 = buffer[offset-3];
		byte b4 = buffer[offset-4];

		
		return (((b1 << 56) >>> 32) |
			   ((b2 << 56) >>> 40) |
			   ((b3 << 56) >>> 48) |
			   ((b4 << 56) >>> 56)) & 0xFFFFFFFFL; 
	}
	
	public int ReadDWordSigned()
	{
		offset+=4;
		byte b1 = buffer[offset-1];
		byte b2 = buffer[offset-2];
		byte b3 = buffer[offset-3];
		byte b4 = buffer[offset-4];
		
		return (b1 << 24) |
			   ((b2 << 24) >>> 8) |
			   ((b3 << 24) >>> 16) |
			   ((b4 << 24) >>> 24); 
	}
	
	public void Skip(int bytes)
	{
		offset+=bytes;
	}
	
};