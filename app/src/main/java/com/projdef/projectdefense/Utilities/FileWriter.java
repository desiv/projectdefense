/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

public class FileWriter
{
	private byte[] buffer;
	private int offset;
	
	public FileWriter()
	{
		this.buffer = new byte[1024];
		offset = 0;
	}
	
	//Arbitrary size
	public FileWriter(int size)
	{
		this.buffer = new byte[size];
		offset = 0;
	}
	
	public void WriteByte(byte b)
	{
		offset++;
		buffer[offset-1] = b;
	}
	
	public void WriteWord(short word)
	{
		offset+=2;
		buffer[offset-1] = (byte)((word & 0xFF00) >> 8);
		buffer[offset-2] = (byte)(word & 0x00FF);	
	}
	
	public void WriteDWord(int dword)
	{
		offset+=4;
		buffer[offset-1] = (byte)((dword & 0xFF000000) >> 24);
		buffer[offset-2] = (byte)((dword & 0x00FF0000) >> 16);
		buffer[offset-3] = (byte)((dword & 0x0000FF00) >> 8);
		buffer[offset-4] = (byte)(dword & 0x000000FF);
	}
	
	public void Skip(int bytes)
	{
		offset+=bytes;
	}
	
	public byte[] getBuffer()
	{
		return buffer;
	}
}