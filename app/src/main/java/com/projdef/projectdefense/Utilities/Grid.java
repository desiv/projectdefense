/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

import java.util.ArrayList;

import com.projdef.projectdefense.Entity.Enemy;

public class Grid
{
	public ObjWrap[] grid;
	int width, height, columns, rows, cellcount, cellwidth, cellheight;
	
	
	public Grid(int width, int height, int columns, int rows)
	{
		this.width = width;
		this.height = height;
		this.columns = columns;
		this.rows = rows;
		cellcount = columns * rows;
		cellwidth = width/columns;
		cellheight = height/rows;
		
		grid = new ObjWrap[columns*rows];
		
		for(int i = 0; i < columns*rows; i++)
			grid[i] = new ObjWrap();
	}
	
	public boolean add(int cell, Enemy obj)
	{	
		if(cell < 0 || cell >= cellcount) 
		{
			//Log.e("Grid::Add", "Wrong cell!: " + cell);
			//Settings.Instance().Activity.finish();
			return false;
		}
		
		return grid[cell].list.add(obj);
	}
	
	public void remove(int cell, Enemy obj)
	{
		if(cell < 0 || cell >= cellcount)
		{
			//Log.e("Grid::Remove", "Wrong cell!: " + cell);
			//Settings.Instance().Activity.finish();
			return;
		}
		
		grid[cell].list.remove(obj);
	}
	
	/**
	 * Returns number of cells occupied by a circle
	 * @param x
	 * @param y
	 * @param radius
	 * @return
	 */
	public int[] getCells(float x, float y, float radius)
	{
		//Cells are from 0 to n-1
		int cell = (int)Math.floor(y/cellheight)*columns + (int)Math.floor(x/cellwidth);
		
		//Rows and collums begin from 1 to n
		int cellrow = (int)Math.ceil((cell+1)/(float)columns);
		int cellcolumn = (cell+1)%columns;
		
		if(cellcolumn == 0) cellcolumn = columns;
		
		int offset = Math.round(radius/57.5f);
		if(offset == 0) offset = 1;
		
		int size = 0;
		int[] tmpLocs = new int[25];
		for(int iY = cellrow-offset; iY <= cellrow+offset; iY++)
		{
			//Skip
			if(iY <= 0 || iY > rows)
				continue;
			
			for(int iX = cellcolumn-offset; iX <= cellcolumn+offset; iX++)
			{
				if(iX <= 0 || iX > columns)
					continue;
				
				if(Common.checkCollision(new Vector((iX-1)*cellwidth, (iY-1)*cellheight), cellwidth, cellheight, new Vector(x, y), radius))
				{
					tmpLocs[size] = (iY-1)*columns + (iX-1);
					size++;
				}
			}
		}
		
		
		int[] retVal = new int[size];
		for(int i = 0; i < size; i++)
			retVal[i] = tmpLocs[i];
		
		return retVal;
	}
	
	public int getCell(float x, float y)
	{
		return (int)Math.floor(y/cellheight)*columns + (int)Math.floor(x/cellwidth);
	}
	
	
	public void clear()
	{
		for(int i = 0; i < cellcount; i++)
			grid[i].list.clear();
	}
	
	/**
	 * Wrapper which contains arraylist of objects
	 * @author Marius Savickas
	 *
	 */
	public class ObjWrap
	{
		public ArrayList<Enemy> list;
		public ObjWrap()
		{
			list = new ArrayList<Enemy>();
			list.ensureCapacity(25);
		}
	}
}