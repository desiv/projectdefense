/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

import com.projdef.projectdefense.Settings;

import android.graphics.Bitmap;
import android.graphics.Paint;

public class Common
{	
	//Lock
	public static final Integer LOCK = 1;
	
	public static final int TRUE = 1;
	public static final int FALSE = 0;
	
	public static final int FPS_TABLE_MAX_SIZE = 30; //Size of array for fps avarage to be counted.
	public static final int FPS_EQUALTOSECOND = 30;	 //How many fps equal to one second
	public static final int GAME_SPEED = 32; // Milliseconds. Bigger number reduces speed, lower - increases.
	public static final int ANIMATION_SPEED = 15; //FPS
	public static final float SPEED_SCROLLING = 1f;
	
	public static final int CLOCKWISE = -1;
	public static final int COUNTERCLOCKWISE = 1;
	
	public static final int INVALID_BUILD_SPOT = -1;
	public static final int MAP_STATS_WORD_SIZE = 12;
	public static final int MAP_STATS_NUMBER_SIZE = 14;
	
	
	public static final int LVL_MAX = 3;
	
	public static final int ANIMATION_OPTION_BAR_RISE = 0xA0;
	public static final int ANIMATION_OPTION_BAR_DROP = 0xA1;
			
	public static final int OPTION_BUILDING_TIER 	= 0xB0;
	public static final int OPTION_UPGRADING_TIER 	= 0xB1;
	
	//Building time in seconds
	public static final float BUILD_TIME_TURRET = 1;	//In seconds
	public static final float BUILD_TIME_TORCH	= 3;	//In seconds
	
	public static final float BUILD_TIME_BULLET_LEVEL_2		= 5;
	public static final float BUILD_TIME_BULLET_LEVEL_3		= 15;
	
	public static final float BUILD_TIME_ELECTR_LEVEL_2		= 15;
	public static final float BUILD_TIME_ELECTR_LEVEL_3		= 30;
	
	public static final float BUILD_TIME_LASER_LEVEL_2		= 10;
	public static final float BUILD_TIME_LASER_LEVEL_3		= 15;
	
	public static final float BUILD_TIME_FIRE_LEVEL_2		= 8;
	public static final float BUILD_TIME_FIRE_LEVEL_3		= 16;
	
	public static final float BUILD_TIME_POISON_LEVEL_2		= 10;
	public static final float BUILD_TIME_POISON_LEVEL_3		= 20;
	
	public static final float BUILD_TIME_FREEZE_LEVEL_2		= 15;
	public static final float BUILD_TIME_FREEZE_LEVEL_3		= 30;
	
	//Used in message queue
	public static final long MESSAGE_INSERT_MIN_DELAY_TIME = 40;
	
	public static final long ENTITY_DISAPPEAR_TIME = 1000;		//1s == 1000
	
	public static final long ENTITY_GUN_SHOT_DELAY 			= 30; //FPS count
	public static final double ENTITY_GUN_ROTATION_SPEED 	= 6;
	public static final int ENTITY_GUN_ROTATION_ANGLE_OK 	= 5;
	
	public static final int ENTITY_FIRE_ID  		= 10001;
	
	public static final int ENTITY_BULLET_ID  		= 10002;
	
	public static final int ENTITY_MESSAGE_HIT 			= 0xD0;
	public static final int ENTITY_MESSAGE_DIE 			= 0xD1;
	public static final int ENTITY_MESSAGE_IAM_DEAD		= 0xD2;
	
	public static final int ENTITY_TYPE_GUN_TURRET 					= 0xF1;
	public static final int ENTITY_TYPE_GUN_TURRET_TYPE_ELECTR 		= 0xF12;
	public static final int ENTITY_TYPE_GUN_TURRET_TYPE_LASER 		= 0xF13;
	public static final int ENTITY_TYPE_GUN_TURRET_TYPE_BULLET 		= 0xF14;
	public static final int ENTITY_TYPE_GUN_TORCH 					= 0xF2;
	
	public static final int ENTITY_TYPE_ENEMY 				= 0x100;
	public static final int ENTITY_TYPE_ENEMY_STRONG_TEAR 	= 0x101;
	public static final int ENTITY_TYPE_ENEMY_STRONG_BURN	= 0x102;
	public static final int ENTITY_TYPE_ENEMY_TEAR 			= 0x103;
	public static final int ENTITY_TYPE_ENEMY_BURN			= 0x104;
	public static final int ENTITY_TYPE_ENEMY_FAST_TEAR 	= 0x105;
	public static final int ENTITY_TYPE_ENEMY_FAST_BURN		= 0x106;
	public static final int ENTITY_TYPE_ENEMY_SPECIAL_TEAR 	= 0x107;
	public static final int ENTITY_TYPE_ENEMY_SPECIAL_BURN	= 0x108;
	public static final int ENTITY_TYPE_ENEMY_BOSS			= 0x109;
	
	public static final int ENTITY_TYPE_FIRE				= 0x2;
	public static final int ENTITY_TYPE_POISON				= 0x21;
	public static final int ENTITY_TYPE_FREEZE				= 0x22;
	
	
	public static final int COST_INCREASE	= 10;
	
	public static final byte FLAG_NOTICE1 	= 0x01;
	public static final byte FLAG_NOTICE2 	= 0x02;
	public static final byte FLAG_NOTICE3 	= 0x04;
	public static final byte FLAG_NOTICE4 	= 0x08;
	public static final byte FLAG_NOTICE5 	= 0x10;
	public static final byte FLAG_NOTICE6 	= 0x20;
	public static final byte FLAG_NOTICE7 	= 0x40;
	
	public static final byte FLAG_TUT_ON 	= 0x01;
	public static final byte FLAG_TUT1 	= 0x02;
	public static final byte FLAG_TUT2 	= 0x04;
	public static final byte FLAG_TUT3 	= 0x08;
	public static final byte FLAG_TUT4 	= 0x10;
	public static final byte FLAG_TUT5 	= 0x20;
	public static final byte FLAG_TUT6 	= 0x40;
	
	public static final byte FLAG_SOUND = 0x01;
	/**
	 * Checks whether the x and y is between 4 positions
	 * @param top
	 * @param bottom
	 * @param left
	 * @param right
	 * @param X
	 * @param Y
	 * @return true if X and Y is inside the rectangle, otherwise false
	 */
	public static boolean isInBounds(float top, float bottom, float left, float right, float X, float Y)
	{
		if((int)Y >= (int)top && (int)Y <= (int)bottom &&
		    (int)X >= (int)left && (int)X <= (int)right)
			return true;
		
		return false;
	}
	
	/**
	 * Checks whether the given x an y is between rectangle area
	 * @param start_x	- start of rectangle
	 * @param start_y	- start of rectangle
	 * @param scale_x	- size of rectangle
	 * @param scale_y	-size of rectangle
	 * @param X	
	 * @param Y
	 * @return true if X and Y is inside the rectangle, otherwise false
	 */
	public static boolean isInBounds2(float start_x, float start_y, float scale_x, float scale_y, float X, float Y)
	{
		if((int)Y >= (int)start_y && (int)Y <= (int)start_y+scale_y && 
			(int)X >= (int)start_x && (int)X <= (int)start_x+scale_x)
			return true;
		
		return false;
	}
	
	
	//Only used for the grid
	public static boolean checkCollision(Vector sq, int sqw, int sqh, Vector circ, float radius)
	{
		boolean collision = false;
		
		//Check collision along x axis
		float minX1 = sq.x;
		float maxX1 = sq.x+sqw;
		
		float minX2 = circ.x - radius;
		float maxX2 = circ.x + radius;
		
		if(!((maxX2 < minX1 ) || (minX2 > maxX1)))
				collision = true;
		
		
		//Check collision along y axis
		float minY1 = sq.y;
		float maxY1 = sq.y+sqh;
		
		float minY2 = circ.y - radius;
		float maxY2 = circ.y + radius;
		
		if(!((maxY2 < minY1 ) || (minY2 > maxY1)))
				if(collision == true)
					return true;
			
		
		return false;
	}
	
	//Returns coordinates for center
	public static float hcenter(float width, String txt, Paint p)
	{
		return (width-p.measureText(txt))/2;
	}
	
	//Returns coordinates for center
	public static float hcenter(float width, Bitmap bmp)
	{
		return (width-bmp.getWidth())/2;
	}
	
	public static float vcenter(float height, Paint p)
	{
		return ((height - p.getTextSize())/ 2) + p.getTextSize();
	}
	
	
	public static String generateWinPhrase()
	{
		String text = new String();
		int random = Settings.Instance().getRandom(10);
		switch(random)
		{
		case 0:
			text = "..killing spree!";
			break;
		
		case 1: 
			text = "..you are on fire!\n..Nah, srsly, torch failed..";
			break;
			
		case 2:
			text = "..gave them a bloody hell!";
			break;
			
		case 3:
			text = "..they had it comin'";
			break;
			
		case 4:
			text = "..who's the man?!";
			break;
		
		case 5:
			text = "Error[404]: This level\nof awesomeness not found";
			break;
			
		case 6:
			text = "..we fight until we die..";
			break;
		
		case 7:
			text = "..victory is ours!";
			break;
			
		case 8:
			text = " ";
			break;
			
		case 9:
			text = "..they should consider this\nas a warning.";
			break;
			
		};
		
		return text;
	}
	
	
	public static String generateLostPhrase()
	{
		String text = new String();
		int random = Settings.Instance().getRandom(10);
		switch(random)
		{
		case 0:
			text = "..disappointed.";
			break;
		
		case 1:
			text = "..you are a disgrace\nto asian stereotypes..";
			break;
			
		case 2:
			text = " ";
			break;
			
		case 3:
			text = "..nope.\nWe ain't lyin' - you lost.";
			break;
			
		case 4:
			text = "..don't worry.\nYou'll get them the next time.";
			break;
			
		case 5:
			text = "..procrastinating, aren't we?";
			break;
			
		case 6:
			text = "..thank you A LOT.\nMonsters ate my food.";
			break;
			
		case 7:
			text = "..another day, another loss.";
			break;
			
		case 8:
			text = "..what's the mater?\nYou are high or what?";
			break;
			
		case 9:
			text = "[insert an insult here]";
			break;
		}
		
		return text;
	}
};