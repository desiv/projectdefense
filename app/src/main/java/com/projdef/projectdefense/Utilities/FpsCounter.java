/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

public class FpsCounter
{
	private static long FPS = 0;
	
	static class SingletonHolder
	{
		static FpsCounter instance;
	}
	
	public static FpsCounter Instance()
	{
		return SingletonHolder.instance = new FpsCounter();
	}
	
	public void CountFps()
	{
		FPS++;
	}
	
	public long getFpsCount()
	{
		return FPS;
	}
}