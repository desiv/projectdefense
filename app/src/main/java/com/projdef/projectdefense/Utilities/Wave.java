/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;
public class Wave 
{
	public FOES Foes[];
	public int numberOfFoes = 0;
	
	//Must initialize, otherwise it will give a null exception
	public void Initialize(int numOfFoes, FOES foes[])
	{
		numberOfFoes = numOfFoes;
		Foes = foes;
	}
}