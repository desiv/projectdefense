/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

import java.util.ArrayList;
import java.util.Iterator;

import com.projdef.projectdefense.Player;
import com.projdef.projectdefense.R;
import com.projdef.projectdefense.Settings;
import com.projdef.projectdefense.World;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class Sound
{
	//Context context;
	AudioManager AM;
	SoundPool spool;
	float volume;
	final Integer spoolLock = 0x0001;
	boolean loaded = false;
	
	ArrayList<SoundCont> list;
	ArrayList<SoundCont> removeList;
	
	
	static public int STOP = 0x0001;
	static public int PLAY = 0x0002;
	static public int PAUSE = 0x0003;
	static public int RESUME = 0x0004;
	
	MediaPlayer bg_menu;
	MediaPlayer bg_world;
	
	public SoundResources sr_torch;
	public SoundResources sr_world;
	
	public int soundID_explosion;
	public int soundID_torch_seg;
	public int soundID_thunder;
	
	public int soundID_win, soundID_lose, soundID_star;
	
	public Sound(Context context)
	{
		//this.context = context;
		
		list = new ArrayList<SoundCont>();
		list.ensureCapacity(10);
		
		removeList = new ArrayList<SoundCont>();
		removeList.ensureCapacity(10);
		
		if(AM == null)
			AM = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		
		volume = 1; //(float) AM.getStreamVolume(AudioManager.STREAM_MUSIC);
	}
	
	public void load(Context context)
	{
		if(loaded == true)
			return;
		
		if(spool == null)
			spool = new SoundPool(30, AudioManager.STREAM_MUSIC, 1);
		
		soundID_explosion = spool.load(context, R.raw.explosion, 0);
		soundID_torch_seg = spool.load(context, R.raw.flame_segment, 1);
		soundID_thunder = spool.load(context, R.raw.thunder, 0);
		
		soundID_win = spool.load(context, R.raw.win, 0);
		soundID_lose = spool.load(context, R.raw.lose, 0);
		soundID_star = spool.load(context, R.raw.star1, 0);
		
		loaded = true;
	}
	
	
	public void unload()
	{
		if(loaded == false)
			return; 
		
		spool.release();
		spool = null;
		
		loaded = false;
	}
	
	public void Process()
	{
		synchronized(list)
		{
			for(SoundCont SC: list)
			{
				if(SC.state == PLAY)
				{
					//Increase volume
					if(SC.srcVolume < SC.dstVolume)
					{
						
						float reduce = SC.srcVolume * 1.1f;
						if(reduce == 0)
							reduce = 0.02f;
						
						SC.srcVolume+=reduce;
						
						if(SC.media != null)
							SC.media.setVolume(SC.srcVolume, SC.srcVolume);
						else 
						{
							synchronized(spoolLock)
							{
								if(spool != null)
									spool.setVolume(SC.ID, SC.srcVolume, SC.srcVolume);
							}
						}
					}
					//Everything is done, remove the object
					else
						removeList.add(SC);
				}
				
				else if(SC.state == STOP)
				{
					//Decrease volume
					if(SC.srcVolume > SC.dstVolume)
					{
						float reduce = SC.srcVolume * 0.2f;
						SC.srcVolume-=reduce;
						
						if(SC.media != null)
							SC.media.setVolume(SC.srcVolume, SC.srcVolume);
						else 
						{
							synchronized(spoolLock)
							{
								if(spool != null)
									spool.setVolume(SC.ID, SC.srcVolume, SC.srcVolume);
							}
						}
						
						if(SC.srcVolume < 0.1f)
						{
							SC.srcVolume = 0;
							
							if(SC.media != null)
								SC.media.setVolume(SC.srcVolume, SC.srcVolume);
							else 
							{
								synchronized(spoolLock){
									if(spool != null)
										spool.setVolume(SC.ID, SC.srcVolume, SC.srcVolume);
								}
							}
						}
					}
					//Remove object
					else
					{	
						if(SC.media != null)
						{
							SC.media.stop();
							SC.media.release();
						}
						else
						{
							synchronized(spoolLock)
							{	
								if(spool != null)
									spool.stop(SC.ID);
							}
						}
						
						removeList.add(SC);
					}
				}
			
				else if(SC.state == PAUSE)
				{
					//Decrease volume
					if(SC.srcVolume > SC.dstVolume)
					{
						float reduce = SC.srcVolume * 0.2f;
						SC.srcVolume-=reduce;
	
						if(SC.media != null)
							SC.media.setVolume(SC.srcVolume, SC.srcVolume);
						else 
						{
							synchronized(spoolLock)
							{
								if(spool != null)
									spool.setVolume(SC.ID, SC.srcVolume, SC.srcVolume);
							}
						}
						
						
						if(SC.srcVolume < 0.1f)
						{
							SC.srcVolume = 0;
	
							if(SC.media != null)
								SC.media.setVolume(SC.srcVolume, SC.srcVolume);
							else
							{
								synchronized(spoolLock)
								{
									if(spool != null)
										spool.setVolume(SC.ID, SC.srcVolume, SC.srcVolume);
								}
							}
						}
					}
					//Remove object
					else
					{	
						if(SC.media != null)
							SC.media.pause();
						else
						{
							synchronized(spoolLock)
							{
								if(spool != null)
									spool.pause(SC.ID);
							}
						}
						
						removeList.add(SC);
					}
				}
			}
		}
		
		synchronized(removeList)
		{
			for(SoundCont SC: removeList)
			{
				list.remove(SC);
			}
		}
	}
	
	public int play(int id)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return 0;
		
		if(!loaded)
			load(Settings.Instance().Context);
		
		int retID = 0;
		
		synchronized(spoolLock)
		{
			retID = spool.play(id, volume, volume, 0, 0, 1);
		}
		
		//Re-do it if playing has failed.
		if(retID == 0)
		{
			unload();
			load(Settings.Instance().Context);
			//play(id);	
		}
		
		return retID;
	}
	
	public int play(int id, int loop, boolean gradient)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return 0;
		
		if(!loaded)
			load(Settings.Instance().Context);
		
		int retid = 0;
		
		synchronized(spoolLock)
		{
			if(spool == null)
				return 0;
			
			if(!gradient)
				retid = spool.play(id, volume, volume, 0, loop, 1);
			else
			{
				retid = spool.play(id, 0, 0, 0, loop, 1);
				list.add(new SoundCont(id, 0, 1, PLAY));
			}
		}
		
		if(retid == 0)
		{
			unload();
			load(Settings.Instance().Context);
			//play(id, loop, gradient);
		}
		
		return retid;
	}
	
	public void resume(int id, boolean gradient)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		if(!loaded)
			load(Settings.Instance().Context);
		
		synchronized(spoolLock)
		{
			if(spool == null)
				return;
			
			if(gradient)
			{
				spool.resume(id);
				list.add(new SoundCont(id, 0, 1, PLAY));
			}
			else
			{
				spool.setVolume(id, 1, 1);
				spool.resume(id);
			}
		}
	}
	
	
	
	public void stop(int id, boolean gradient)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		synchronized(spoolLock)
		{
			if(spool == null)
				return;
			
			if(gradient)
			{
				list.add(new SoundCont(id, 1, 0, STOP));
			}
			else
				spool.stop(id);
		}
	}

	public void pause(int id, boolean gradient)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
			
		synchronized(spoolLock)
		{
			if(spool == null)
				return;
			
			if(gradient)
				list.add(new SoundCont(id, 1, 0, PAUSE));
			else
				spool.pause(id);
		}
	}
	
	
	
	
	
	//Sound functions for Menu
	public void playMenu(Context context)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		if(bg_menu == null)
		{
			bg_menu = MediaPlayer.create(context, R.raw.catacombs_moosader);
			bg_menu.seekTo(0);
			bg_menu.start();	
			bg_menu.setVolume(0, 0);
			bg_menu.setLooping(true);
			
			synchronized(list){
				list.add(new SoundCont(bg_menu, 0, 15, PLAY));
			}
		}
		
		
		else if(!bg_menu.isPlaying())
		{
			bg_menu.seekTo(0);
			bg_menu.start();	
			bg_menu.setVolume(0, 0);
			bg_menu.setLooping(true);
			
			synchronized(list){
				list.add(new SoundCont(bg_menu, 0, 15, PLAY));
			}
		}
	}
	
	public void stopMenu()
	{	
		if(bg_menu.isPlaying())
		{
			synchronized(list){
				list.add(new SoundCont(bg_menu, 1, 0, STOP));
			}
		}
		
		bg_menu = null;
	}
	
	public void pauseMenu()
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		if(bg_menu != null)
			if(bg_menu.isPlaying())
			{
				synchronized(list){
					list.add(new SoundCont(bg_menu, 1, 0, PAUSE));
				}
			}
	}
	
	
	
	
	
	
	//Sound functions for World
	public void playWorld(Context context)
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		if(bg_world == null)
		{
			int index = ((World)Settings.Instance().Game.World).mapNumber % 4;
			float volume = 0.1f;
			
			if(index == 1)
				volume = 1;
			
			switch(index)
			{
			case 0:
				bg_world = MediaPlayer.create(context, R.raw.cavern_moosader);
				break;
			case 1:
				bg_world = MediaPlayer.create(context, R.raw.midnightfield_moosader);
				break;
			case 2:
				bg_world = MediaPlayer.create(context, R.raw.slowhuitar_moosader);
				break;
			case 3:
				bg_world = MediaPlayer.create(context, R.raw.tunafish_moosader);
				break;
			};
			
			
			
			bg_world.seekTo(0);
			bg_world.start();	
			bg_world.setVolume(0, 0);
			bg_world.setLooping(true);
			removeGradient(bg_world, STOP);
			removeGradient(bg_world, PAUSE);
			
			synchronized(list){
				list.add(new SoundCont(bg_world, 0, volume, PLAY));
			}
		}
		
		else if(!bg_world.isPlaying())
		{
			bg_world.seekTo(0);
			bg_world.start();	
			bg_world.setVolume(0, 0);
			bg_world.setLooping(true);
			removeGradient(bg_world, STOP);
			removeGradient(bg_world, PAUSE);
			
			synchronized(list){
				list.add(new SoundCont(bg_world, 0, 0.1f, PLAY));
			}
		}
		
	}
	
	public void stopWorld()
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		float volume = 0.1f;
		
		if(((World)Settings.Instance().Game.World).mapNumber % 4 == 1)
			volume = 1;
		
		if(bg_world != null)
		{
			if(bg_world.isPlaying())
			{
				//Removes gradient playing while it is going for a stop
				removeGradient(bg_world, PLAY);
				removeGradient(bg_world, PAUSE);
				synchronized(list){
					list.add(new SoundCont(bg_world, volume, 0, STOP));
				}
			}
		}		
		bg_world = null;
	}
	
	
	public void pauseWorld()
	{
		if((Player.Instance().soundFlag & Common.FLAG_SOUND) == 0)
			return;
		
		if(bg_world.isPlaying())
		{
			removeGradient(bg_world, PLAY);
			synchronized(list){
				list.add(new SoundCont(bg_world, 0.1f, 0, PAUSE));
			}
		}
	}
	
	public void release()
	{	
		if(bg_menu != null)
		{
			if(bg_menu.isPlaying())
				bg_menu.stop();
			
			bg_menu.release();
		}
			
		if(bg_world != null)
		{
			if(bg_world.isPlaying())
				bg_world.stop();
			
			bg_world.release();
		}
		
		if(spool != null)
			spool.release();
		
		synchronized(list){
			list.clear();
		}
			
		synchronized(removeList){
			removeList.clear();
		}
		
		
		if(spool != null)
		{
			synchronized(spool){
				spool = null;
			}
		}
			
		
		bg_menu = null;
		bg_world = null;
		
		loaded = false;
	}
	
	
	private void removeGradient(MediaPlayer mp, int state)
	{
		synchronized(list){
			
			Iterator<SoundCont> iter = list.iterator();
			SoundCont SC = null;
			
			while(iter.hasNext())
			{
				SC = iter.next();
				
				if(SC == null)
					continue;
					
				if(SC.media == mp && SC.state == state)
				{
					iter.remove();
				}
			}
		}
	}
};



class SoundResources
{
	int soundID;
	int streamID;
	boolean playing = false;
	
	SoundResources(int id)
	{
		this.soundID = id;
		this.playing = false;
		this.streamID = 0;
	}
}

//Container for sound processes
class SoundCont
{
	public int ID;
	public float srcVolume;
	public float dstVolume;
	public int state;
	public MediaPlayer media;
	
	SoundCont(int id, float maxVol, float minVol, int state)
	{
		ID = id;
		srcVolume = maxVol;
		dstVolume = minVol;
		this.state = state;
		media = null;
	}
	
	
	SoundCont(MediaPlayer mp, float maxVol, float minVol, int state)
	{
		ID = 0;
		srcVolume = maxVol;
		dstVolume = minVol;
		this.state = state;
		media = mp;
	}
}