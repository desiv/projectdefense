/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

public class Vector
{
	public float x, y;
	public float velocityX, velocityY;
	//private boolean normalized = false;
	
	public Vector()
	{
		x = 0;
		y = 0;
		
		velocityX = 1;
		velocityY = 1;
	}
	
	public Vector(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Vector(Vector v)
	{
		x = v.x;
		y = v.y;
		
		velocityX = v.velocityX;
		velocityY = v.velocityY;
	}
	
	/**
	 * Copies Vector
	 */
	public Vector clone(){return new Vector(this);}
	
	/**
	 * Vector addition
	 * @param vector - vector to add
	 * @return return new Vector
	 */
	public Vector add(Vector vector)
	{
		Vector ret = new Vector();
		
		ret.x = this.x + vector.x;
		ret.y = this.y + vector.y;
		
		return ret;
	}
	
	/**
	 * Vector subtraction
	 * @param vector - vector to subtract
	 * @return return new Vector
	 */
	public Vector subtract(Vector vector)
	{
		Vector ret = new Vector();
		
		ret.x = this.x - vector.x;
		ret.y = this.y - vector.y;
		
		return ret;
	}
	
	/**
	 * Vector multiplication
	 * @param vector - vector to multiply
	 * @return return new Vector
	 */
	public Vector multiply(Vector vector)
	{
		Vector ret = new Vector();
		
		ret.x = this.x * vector.x;
		ret.y = this.y * vector.y;
		
		return ret;
	}
	
	
	/**
	 * Vector multiplication
	 * @param a - float value to multiply vector
	 * @return return new Vector
	 */
	public Vector multiply(float value)
	{
		Vector ret = new Vector();
		
		ret.x = this.x * value;
		ret.y = this.y * value;
		
		return ret;
	}
	
	public int length()
	{
		return (int) getDistance(new Vector(0, 0), this);
	}
	
	/* New pointing direction */
	public void setDirection(float X, float Y)
	{
		velocityX = X - x;
		velocityY = Y - y;
	}
	
	public void setDirection(float X, float Y, boolean normalize)
	{
		velocityX = X - x;
		velocityY = Y - y;
		
		if(normalize)
		{
			Normalize();
		}
	}
	
	public void set(Vector v)
	{
		x = v.x;
		y = v.y;
		
		velocityX = v.velocityX;
		velocityY = v.velocityY;
	}
	
	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
		
		velocityX = 0;
		velocityY = 0;
	}
	
	public void Normalize()
	{
		double magnitude = Math.sqrt(velocityX*velocityX + velocityY*velocityY);
		
		velocityX = velocityX / (float)magnitude;
		velocityY = velocityY / (float)magnitude;
	}
	
	/**
	 * Gets normalized values
	 * @return Vector with normalized values
	 */
	public Vector getNormalized()
	{
		return new Vector(velocityX, velocityY);
	}
	
	public boolean isNormilzed()
	{
		Vector v = new Vector();
		
		v.x = velocityX;
		v.y = velocityY;
		
		if((1 - getDistance(this, v)) <= 0.001)
		{
			return true;
		}
		
		return false;
	}
	
	
	public static Vector Normalize(Vector a, Vector b)
	{
		//Get magnitude
		Vector c = new Vector();
		
		
		double magnitude = getDistance(a, b);
		
		c.x = (float)((b.x - a.x) / magnitude);
		c.y = (float)((b.y - a.y) / magnitude);
		
		return c;
	}
	
	public static double getDistance(Vector a, Vector b)
	{
		if(a == null || b == null)
			return -1f;
		
		double magnitude = Math.sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y));
		
		return magnitude;
	}
	
	public static double getAngleBetweenVectors(Vector a, Vector b)
	{
		if((a.x*b.x + a.y*b.y) == 0) return 90;
		
		//double test = a.x*b.x + a.y*b.y;
		
		double angle = Math.acos(a.x*b.x + a.y*b.y);
		
		//Returns angle in radians
		return angle;
	}
	
	public static int getSign(Vector main, Vector b)
	{
		if(main.y*b.x > main.x*b.y)
		{
			return Common.COUNTERCLOCKWISE;
		}
		else
		{
			return Common.CLOCKWISE;
		}
	}
};