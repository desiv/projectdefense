/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

public class Telegram
{
	public int Sender;
	public int Receiver;
	public int Msg;
	public ExtraInfo Extra;
	public long dispatchTime = 0;	//time to dispatch the message. 0 never
	
	public Telegram(int receiver, int sender, int msg, ExtraInfo extr)
	{
		Receiver = receiver;
		Sender = sender;
		Msg = msg;
		Extra = extr;
	}
};