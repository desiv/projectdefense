/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

//Container for foes
public class FOES
{
	public int type;
	public int level;
	
	public FOES(){type = 0; level = 0;}
}