/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */

package com.projdef.projectdefense.Utilities;

import java.util.Date;

public class Timer
{
	private Date date = new Date();
	
	private long _timeStamp = 0;
	private long _fpsStamp = 0;
	
	public Timer()
	{
		_timeStamp = date.getTime();
	}
	
	public long getTimeStamp()
	{
		return _timeStamp;
	}
	
	public long getFpsStamp()
	{
		return _fpsStamp;
	}
	
	public void updateTimeStamp()
	{
		_timeStamp = timeNow();
	}
	
	public void updateFpsStamp()
	{
		_fpsStamp = FpsCounter.Instance().getFpsCount();
	}
	
	//Gets updated time
	public long timeNow()
	{
		date = null;
		date = new Date();
		
		return date.getTime();
	}
	
	public long fpsCountNow()
	{
		return FpsCounter.Instance().getFpsCount();
	}
	
	// Checks to see if certain time has passed
	public boolean ifTimePassed(long t)
	{
		if(timeNow() - _timeStamp >= t)
		{
			_timeStamp = timeNow();
			return true;
		}
		
		return false;
	}
	
	//Checks to see if certain fps count has passed
	public boolean ifFpsPassed(long t)
	{
		if(fpsCountNow() - _fpsStamp >= t)
		{
			_fpsStamp = fpsCountNow();
			return true;
		}
		
		return false;
	}
	
	
	public void releaseMemory()
	{
		date = null;
	}
};