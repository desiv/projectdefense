/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense.Utilities;

import com.projdef.projectdefense.Entity.Enemy;

public class ExtraInfo
{
	public float PhyATK;
	public float MagATK;
	public float PhyDEF;
	public float MagDEF;
	public int Range;
	public int Category = 0;
	public int FlameType 	= 0;
	public int Lvl			= 0;
	public int intPar = 0;
	public float floatPar = 0;
	public Enemy enemyPar = null;
};