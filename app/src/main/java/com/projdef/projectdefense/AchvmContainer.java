/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.mutable.MutableLong;
import com.projdef.projectdefense.Manager.TransactionManager;
import com.projdef.projectdefense.Utilities.Common;

/**
 * Main class for handling achievements
 * @author Marius Savickas
 *
 *	//1 == true / 0 == false
 *
 *Logic:
 *
 *Fist element in Achievement.properties should contain everything. Other elements - only property, operand, value
 *everything else is optional.
 *
 *Achieved/shown/global flags should only be modified in the first element of Achievement.properties
 */
public class AchvmContainer
{
	private HashMap<String, MutableLong> Properties = new HashMap<String, MutableLong>();
	private ArrayList<Achievement> Achievements = new ArrayList<Achievement>();
	private HashMap<String, MutableLong> AchvState = new HashMap<String, MutableLong>();
	private ArrayList<Achievement> GlobalAchievements = new ArrayList<Achievement>();
	private HashMap<String, MutableLong> GlobalAchvState = new HashMap<String, MutableLong>();
	
	/**
	 * Create a property
	 * @param name	- name for the property
	 * @param value	- reference to a variable
	 */
	public void addProperty(String name, MutableLong value)
	{
		Properties.put(name, value);
	}
	
	
	/**
	 * Retrieves the reference to a value by properties name
	 * @param name
	 * @return
	 */
	public MutableLong getProperty(String name)
	{
		return Properties.get(name);
	}
	
	/**
	 * Retrieves the state of an achievement
	 * @param name - name of achievement
	 * @return reference to a value
	 */
	public MutableLong getAchievement(String name)
	{
		return AchvState.get(name);
	}
	
	
	/**
	 * Retrieves the state of an achievement
	 * @param name - name of achievement
	 * @return reference to a value
	 */
	public MutableLong getGlobalAchievement(String name)
	{
		return GlobalAchvState.get(name);
	}
	
	/**
	 * Creates new achievement
	 * @param value - new achievement
	 */
	public void addAchievement(Achievement value)
	{
		Achievements.add(value);
		//Maps achieved values for quick accessing
		AchvState.put(value.properties.get(0).name, value.properties.get(0).achieved);
	}
	
	
	/**
	 * Creates new achievement
	 * @param value - new achievement
	 */
	public void addGlobalAchievement(Achievement value)
	{
		GlobalAchievements.add(value);
		//Maps achieved values for quick accessing
		GlobalAchvState.put(value.properties.get(0).name, value.properties.get(0).achieved);
	}
	
	
	/**
	 * Processes all achievements: checks flags and does appropriate actions
	 */
	public void Process()
	{
		//Check for accomplishments
		for(Achievement A: Achievements)
		{
			//If one property is hit false then all of the achievement is false
			boolean achved = true;
			//Check for all the properties
			for(Achievement.Container C: A.properties)
			{
				if(C.operand == "<")
				{
					if(C.property.getValue() < C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == ">")
				{
					if(C.property.getValue() > C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "==")
				{
					if(C.property.getValue() == C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "!=")
				{
					if(C.property.getValue() > C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "<=")
				{
					if(C.property.getValue() <= C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == ">=")
				{
					if(C.property.getValue() >= C.value.getValue()) {}
					else achved = false;
				}
			}
			
			//Set final verdict of achievement
			if(achved) A.properties.get(0).achieved.setValue(Common.TRUE);
			else A.properties.get(0).achieved.setValue(Common.FALSE);
		}
		
		//Show the achievements
		for(Achievement A: Achievements)
		{
			if(A.properties.get(0).achieved.getValue() == Common.TRUE && A.properties.get(0).shown == false)
			{
				A.properties.get(0).command.execute();
				A.properties.get(0).shown = true;
			}
		}
	}
	
	
	/**
	 * Processes all achievements: checks flags and does appropriate actions
	 */
	public void GlobalProcess()
	{
		//Check for accomplishments
		for(Achievement A: GlobalAchievements)
		{
			//If one property is hit false then all of the achievement is false
			boolean achved = true;
			//Check for all the properties
			for(Achievement.Container C: A.properties)
			{
				if(C.operand == "<")
				{
					if(C.property.getValue() < C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == ">")
				{
					if(C.property.getValue() > C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "==")
				{
					if(C.property.getValue() == C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "!=")
				{
					if(C.property.getValue() > C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == "<=")
				{
					if(C.property.getValue() <= C.value.getValue()) {}
					else achved = false;
				}
				else if(C.operand == ">=")
				{
					if(C.property.getValue() >= C.value.getValue()) {}
					else achved = false;
				}
			}
			
			//Set final verdict of achievement
			if(achved) A.properties.get(0).achieved.setValue(Common.TRUE);
			else A.properties.get(0).achieved.setValue(Common.FALSE);
		}
		
		//Show the achievements
		for(Achievement A: GlobalAchievements)
		{
			if(A.properties.get(0).achieved.getValue() == Common.TRUE && A.properties.get(0).shown == false)
			{
				A.properties.get(0).command.execute();
				A.properties.get(0).shown = true;
			}
		}
	}
	
	
	/*
	 *  Methods to call
	 */
	public class ChangeToLose implements Command
	{
		public void execute()
		{
			((Lost)Settings.Instance().Game.Lost).setPhrase();
			Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_lose);
			TransactionManager.Instance().doTransaction(Settings.Instance().Game.getCurrentState(), Settings.Instance().Game.Lost);
		}
	}
	
	public class ChangeToWin implements Command
	{
		public void execute()
		{
			((Win)Settings.Instance().Game.Win).setPhrase();
			Settings.Instance().Sound.play(Settings.Instance().Sound.soundID_win);
			TransactionManager.Instance().doTransaction(Settings.Instance().Game.getCurrentState(), Settings.Instance().Game.Win);
		}
	}
	
	public class DoNothing implements Command{public void execute(){}}
	
	
	/**
	 * Class containing achievement details
	 * @author Marius Savickas
	 *
	 */
	public class Achievement
	{	
		public ArrayList<Container> properties = new ArrayList<Container>();
		
		public Achievement(String name, MutableLong property, String operand, MutableLong value, Command command, boolean global)
		{
			Container c = new Container();
			c.name = name;
			c.property = property;
			c.operand = operand;
			c.value = value;
			c.command = command;
			c.achieved = new MutableLong(Common.FALSE);
			c.shown = false;
			c.global = global;
			properties.add(c);
		}
		
		/**
		 * Adds an additional property to process
		 * @param property	- reference to a property
		 * @param operand	- operand
		 * @param value		- reference to a value
		 */
		public void addAdditional(MutableLong property, String operand, MutableLong value)
		{
			Container c = new Container();
			c.name = properties.get(0).name;
			c.property = property;
			c.operand = operand;
			c.value = value;
			c.command = null;
			c.achieved = new MutableLong(Common.FALSE);
			c.shown = properties.get(0).shown;
			c.global = properties.get(0).global;
			properties.add(c);
		}
		
		/**
		 * Structure for properties
		 * @author Marius Savickas
		 *
		 */
		public class Container
		{
			public String name;
			public MutableLong property;
			public String operand;
			public MutableLong value;
			public Command command;
			public MutableLong achieved;
			public boolean shown = false;
			//Achievement is global or local
			public boolean global = true;
		}
	}
	
	public void reset()
	{
		for(Achievement A: Achievements)
		{
			A.properties.get(0).achieved.setValue(Common.FALSE);
			A.properties.get(0).shown = false;
		}
	}
	
	public void deleteAll()
	{
		Properties.clear();
		Achievements.clear();
		AchvState.clear();
		GlobalAchievements.clear();
		GlobalAchvState.clear();
		
	}
	
	/**
	 * Interface for methods
	 */
	public interface Command
	{
		public void execute();
	}
	
	
	/*
	 * Singleton
	 */
	private static class Singleton
	{
		public static final AchvmContainer instance = new AchvmContainer();
	}
	
	public static AchvmContainer Instance()
	{
		return Singleton.instance;
	}
}