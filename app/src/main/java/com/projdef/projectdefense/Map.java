/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.projdef.projectdefense.Entity.Gun;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Graphics.KeyAnimation;
import com.projdef.projectdefense.Manager.OnScreenMsgManager;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.FOES;
import com.projdef.projectdefense.Utilities.FileParser;
import com.projdef.projectdefense.Utilities.Vector;
import com.projdef.projectdefense.Utilities.Wave;
import com.projdef.projectdefense.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * 
 * @author Marius Savickas
 *
 */
public class Map
{
	private World World;
	
	private ArrayList<BUILD> Build = new ArrayList<BUILD>();	//Array to keep updates on process of building guns
	
	//Array to avoid concurrent modification when removing BUILD objects from main Build array
	private ArrayList<BUILD> removeBuild = new ArrayList<BUILD>();	
	
	private int numOfSteps = 0;			//walking path. step count towards end
	private ArrayList<Vector> steps;	//walking path. position of every step
	
	private Paint paint_build_stroke = new Paint();
	private Paint paint_build_dark = new Paint();
	private Paint paint_build = new Paint();
	private Paint paint_black = new Paint();
	private Paint paint_stats = new Paint();
	
	private int numOfBuildings;			//number of locations per map
	private Vector[] BuildingPos;		//coordinations of possible builing locations
	public KeyAnimation[] SpriteBuilding;		//sprite of the building area
	private int[] BuildingOccupied;		//This will hold the id of an occupying entity 
	
	private Vector EnemyStartPos = new Vector();			//starting pos
	private Vector EnemyEndPos = new Vector();				//end pos
	
	//Offset for drawing a map on bigger screens
	public int BGx, BGy;
	
	private int drawXGold, drawXHumans, drawY;
	//Values held by bitmaps
	private int bitmapGold, bitmapHumans;

	//================= Constructor =====================
	Map(World w)
	{
		World = w;
		
		paint_black.setColor(Color.BLACK);
		paint_black.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_black.setStrokeWidth(1);
		paint_black.setAntiAlias(false);
		paint_black.setDither(false);
		paint_black.setFilterBitmap(false);
		
		paint_build.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_build.setStrokeWidth(1);
		paint_build.setAntiAlias(Settings.Instance().AntiAlias);
		paint_build.setColor(0xf00000F0);	//blue
		paint_build.setAntiAlias(false);
		paint_build.setDither(false);
		paint_build.setFilterBitmap(false);
		
		paint_build_dark.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_build_dark.setStrokeWidth(1);
		paint_build_dark.setAntiAlias(Settings.Instance().AntiAlias);
		paint_build_dark.setColor(0xF0000037);	//dark blue
		paint_build_dark.setAntiAlias(false);
		paint_build_dark.setDither(false);
		paint_build_dark.setFilterBitmap(false);
		
		paint_build_stroke.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_build_stroke.setStrokeWidth(3);
		paint_build_stroke.setAntiAlias(Settings.Instance().AntiAlias);
		paint_build_stroke.setColor(0xf0A0A0F0);	//blue
		paint_build_stroke.setAntiAlias(false);
		paint_build_stroke.setDither(false);
		paint_build_stroke.setFilterBitmap(false);
		
		paint_stats.setAlpha(255);
	}
	
	public void Initialize(int mapNumber)
	{	
		BGx = 0;
		BGy = (Settings.Instance().screenHeight - Graphics.MAP_1.get(0).getHeight())/2;
		
		drawXGold = (int)(10*Settings.Instance().screenDensity); 
		drawXHumans = (int)(175*Settings.Instance().screenDensity);
		
		if(Settings.Instance().screenDensity == 0.75f)
			drawY = (int)(15*Settings.Instance().screenDensity);
		else
			drawY = (int)(15*Settings.Instance().screenDensity)+BGy;
		
		//Load map background
		Graphics.MAP_1.set(0, Graphics.loadImg(R.drawable.bg1+mapNumber-1, Settings.Instance().screenScaleMap));		
		
		//Load all map data
		LoadMap(mapNumber);
		
		bitmapGold = Settings.Instance().GOLD.getValue().intValue();
		bitmapHumans = Settings.Instance().aliveHumans.getValue().intValue();
		
		//Create bitmap for data
		Graphics.CACHED_MAP_DATA.set(1, OnScreenMsgManager.Instance().MakeBitmap(Integer.toString(bitmapGold), Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity));
		Graphics.CACHED_MAP_DATA.set(3, OnScreenMsgManager.Instance().MakeBitmap(Integer.toString(bitmapHumans), Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity-2*Settings.Instance().screenDensity));
		
		
		//Draw pads into map bmp
		Bitmap buff = Bitmap.createBitmap(Graphics.MAP_1.get(0));
		Canvas c = new Canvas(buff);
		
		//Draws building areas
		for(int i = 0; i < numOfBuildings; i++)
		{
			SpriteBuilding[i].SetLocation(SpriteBuilding[i].getLocation().x, SpriteBuilding[i].getLocation().y-BGy);
			SpriteBuilding[i].Draw(c);
			SpriteBuilding[i].SetLocation(SpriteBuilding[i].getLocation().x, SpriteBuilding[i].getLocation().y+BGy);
		}
		
		Graphics.MAP_1.set(0, buff);
	}
	
	public void Uninitialize()
	{
		World = null;
	}
	
	public void LoadMap(int mapNumber)
	{		
		/*
		 * 
		 * WORD Money
		 * BYTE Humans
		 *
		 * BYTE building positions of pad quantity
		 * LOOP
		 * WORD (x,y) (position of pads)
		 *
		 * BYTE Step quantity
		 * LOOP
		 * WORD (x, y) (position of step)
		 *
		 * BYTE quantity of Waves
		 * LOOP
		 * BYTE quantity of foes
		 * LOOP
		 * BYTE Level
		 * WORD foe type
		*/
		
		byte[] buffer = null;
		InputStream stream = null;

		//Open and read the file
		try {
			
			stream = Settings.Instance().Context.getAssets().open("maps/" + "map"+ mapNumber +".dat", Context.MODE_PRIVATE);
			buffer = new byte[stream.available()];
			stream.read(buffer);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		FileParser ReadFile = new FileParser(buffer);
		
		//****** Loading map detail ********
		//Set gold
		if(Settings.Instance().GOLD.getValue() == 0L) 
			Settings.Instance().GOLD.setValue(ReadFile.ReadWord());
		else ReadFile.Skip(2);
		
		//Set humans
		if(Settings.Instance().aliveHumans.getValue() == 0L)
		{
			Settings.Instance().aliveHumans.setValue(ReadFile.ReadByte());
			//Set current value as max
			Settings.Instance().totalHumans.setValue(Settings.Instance().aliveHumans.getValue());
		}
		else ReadFile.Skip(1);
		
		
		numOfBuildings 	= ReadFile.ReadByte();
		
		//Initialize totalGuns value
		Settings.Instance().totalGuns.setValue(numOfBuildings);
		
		BuildingPos = new Vector[numOfBuildings];
		SpriteBuilding = new KeyAnimation[numOfBuildings];
		BuildingOccupied = new int[numOfBuildings];
		
		//Avoiding null exception
		for(int i = 0; i < numOfBuildings; i++)
			BuildingPos[i] = new Vector();
		
		for(int i = 0; i < numOfBuildings; i++)
			SpriteBuilding[i] = new KeyAnimation(false, true);
		
			
		for(int i = 0; i < numOfBuildings; i++)
			BuildingOccupied[i] = -1;
		
		//Loading building positions
		//============================== Building positions =============
		for(int i = 0; i < numOfBuildings; i++)
		{
			BuildingPos[i].x = ReadFile.ReadWord()*Settings.Instance().screenDensity;
			BuildingPos[i].y = ReadFile.ReadWord()*Settings.Instance().screenDensity+BGy;
			
			//Initialize sprite
			SpriteBuilding[i].AddBitmap(Graphics.MAP_1.get(1));
			SpriteBuilding[i].AddBitmap(Graphics.MAP_1.get(2));
			SpriteBuilding[i].SetLocation(BuildingPos[i].x, BuildingPos[i].y);
		}
		//===============================================================
		
		
		//Loading path
		numOfSteps = ReadFile.ReadByte();
		steps = new ArrayList<Vector>();
		
		for(int i = 0; i < numOfSteps; i++)
		{
			Vector step = new Vector();
			
			step.x = ReadFile.ReadWordSigned()*Settings.Instance().screenDensity;
			step.y = ReadFile.ReadWordSigned()*Settings.Instance().screenDensity+BGy;
			
			steps.add(step);
		}
		
		
		//Setting up enemies starting position
		EnemyStartPos.x = steps.get(0).x;
		EnemyStartPos.y = steps.get(0).y;
		
		//Setting up enemies final destination
		EnemyEndPos.x = steps.get(numOfSteps-1).x;
		EnemyEndPos.y = steps.get(numOfSteps-1).y;
		
		int WaveQuantity = ReadFile.ReadByte();
		
		Wave wave[] = new Wave[WaveQuantity];
		
		Settings.Instance().totalZombies.setValue(0);
		//Allocate memory for waves to avoid a null exception
		for(int i = 0; i < WaveQuantity; i++)
			wave[i] = new Wave();
		
		//Read waves and foes
		for(int i = 0; i < WaveQuantity; i++)
		{
			//Read foe quantity
			int FoeQuantity = ReadFile.ReadByte();
			//Get the total number of zombies
			Settings.Instance().totalZombies.setValue(Settings.Instance().totalZombies.getValue() + FoeQuantity);
			
			//Allocate memory for array of foes
			FOES foes[] = new FOES[FoeQuantity];
			
			for(int j = 0; j < FoeQuantity; j++)
				foes[j] = new FOES();
			
			//Read types
			for(int j = 0; j < FoeQuantity; j++)
			{
				foes[j].level 	= ReadFile.ReadByte();
				foes[j].type 	= ReadFile.ReadWord();
				
				calculateScore(foes[j].type, foes[j].level);
			}
			
			//Initialize wave
			wave[i].Initialize(FoeQuantity, foes);
		}
		
		Settings.Instance().SpawnManager.Initialize(World, WaveQuantity, wave);
	}
	
	/**
	 * Draws everything that has a relationship with a map
	 */
	public void DrawMap()
	{
		//Clears the screen and at the same draws the background
		Settings.Instance().Canvas.drawBitmap(Graphics.MAP_1.get(0), BGx, BGy, null);
		
		//Draws changed building areas
		for(int i = 0; i < numOfBuildings; i++)
		{
			if(SpriteBuilding[i].getFrame() == 1) 
				SpriteBuilding[i].Draw();
		}
		
		
		//Draw building process
		//Get a copy of a list to avoid concurrent modification
		ArrayList<BUILD> tmplist = new ArrayList<BUILD>(Build);
		for(BUILD B: tmplist)
		{	
			//Draw
			Settings.Instance().Canvas.drawRect(SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().y-2*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity+((B.maxTime*30*Settings.Instance().screenDensity)/B.maxTime),
												SpriteBuilding[B.buildingIndex].getLocation().y+1*Settings.Instance().screenDensity,
												paint_build_stroke);
										
			
			Settings.Instance().Canvas.drawRect(SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().y-2*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity+((B.maxTime*30*Settings.Instance().screenDensity)/B.maxTime),
												SpriteBuilding[B.buildingIndex].getLocation().y+1*Settings.Instance().screenDensity,
												paint_build_dark);
			
			Settings.Instance().Canvas.drawRect(SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().y-2*Settings.Instance().screenDensity,
												SpriteBuilding[B.buildingIndex].getLocation().x-15*Settings.Instance().screenDensity+((B.variableTime*30*Settings.Instance().screenDensity)/B.maxTime),
												SpriteBuilding[B.buildingIndex].getLocation().y+1*Settings.Instance().screenDensity,
												paint_build);
		}
	}
	
	/**
	 * Draws map effects
	 */
	public void DrawMapEffects()
	{
		//Apply screen effects
		//Top
		Settings.Instance().Canvas.drawBitmap(Graphics.MAP_1.get(3),
											  0,
											  BGy,
											  null);
		//Bottom
		Settings.Instance().Canvas.drawBitmap(Graphics.MAP_1.get(4),
											  0,
											  -BGy + Settings.Instance().screenHeight - Graphics.MAP_1.get(4).getHeight(),
											  null);
		
		//Left
		Settings.Instance().Canvas.drawBitmap(Graphics.MAP_1.get(5),
											  0,
											  BGy + Graphics.MAP_1.get(3).getHeight(),
											  null);
		
		//Right
		Settings.Instance().Canvas.drawBitmap(Graphics.MAP_1.get(6),
											  Settings.Instance().screenWidth - Graphics.MAP_1.get(6).getWidth(),
											  BGy + Graphics.MAP_1.get(3).getHeight(),
											  null);
		
		
		//Black out unused spots
		if(Settings.Instance().screenDensity >= 1.5)
		{
			Settings.Instance().Canvas.drawRect(0, 0, Settings.Instance().screenWidth, BGy, paint_black);
			Settings.Instance().Canvas.drawRect(0, Settings.Instance().screenHeight-BGy, Settings.Instance().screenWidth, Settings.Instance().screenHeight, paint_black);
		}
	}
	
	/**
	 * Draws stats
	 */
	public void DrawStats()
	{	
		//Draw gold amount
		Settings.Instance().Canvas.drawBitmap(Graphics.CACHED_MAP_DATA.get(1),
											  drawXGold+Graphics.CACHED_MAP_DATA.get(0).getWidth(),
											  drawY+(Graphics.CACHED_MAP_DATA.get(0).getHeight()-Graphics.CACHED_MAP_DATA.get(1).getHeight())/2,
											  paint_stats);
		//Draw number of humans
		Settings.Instance().Canvas.drawBitmap(Graphics.CACHED_MAP_DATA.get(3),
											  drawXHumans+Graphics.CACHED_MAP_DATA.get(2).getWidth(),
											  drawY+(Graphics.CACHED_MAP_DATA.get(2).getHeight()-Graphics.CACHED_MAP_DATA.get(3).getHeight())/2,
											  paint_stats);
			
		
		//Gold
		Settings.Instance().Canvas.drawBitmap(Graphics.CACHED_MAP_DATA.get(0), drawXGold, drawY, paint_stats);
		//Humans
		Settings.Instance().Canvas.drawBitmap(Graphics.CACHED_MAP_DATA.get(2), drawXHumans, drawY, paint_stats);
	}
	
	/**
	 * Updates on map processes. Must be called in a loop
	 */
	public void Update()
	{
		//Update statistic bitmaps
		//Create bitmap for data
		if(Settings.Instance().GOLD.getValue() != bitmapGold)
		{
			bitmapGold = Settings.Instance().GOLD.getValue().intValue();
			Graphics.CACHED_MAP_DATA.set(1, OnScreenMsgManager.Instance().MakeBitmap(Integer.toString(bitmapGold), Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity));
		}
		
		if(Settings.Instance().aliveHumans.getValue() != bitmapHumans)
		{
			bitmapHumans = Settings.Instance().aliveHumans.getValue().intValue();
			Graphics.CACHED_MAP_DATA.set(3, OnScreenMsgManager.Instance().MakeBitmap(Integer.toString(bitmapHumans), Common.MAP_STATS_NUMBER_SIZE*Settings.Instance().screenDensity-2*Settings.Instance().screenDensity));
		}
		
		//Update timers
		for(BUILD B: Build)
		{
			B.variableTime++;
			
			//Remove from updating list and move it to the world, because it has been built
			if(B.variableTime >= B.maxTime)
			{
				//Add gun to the world
				World.GunList.add(B.entity);
				
				//Remove from Build list
				removeBuild.add(B);
			}
		}
		
		//Remove finished builds
		for(BUILD remove: removeBuild)
		{
			Build.remove(remove);
		}
	}
	
	/**
	 * Establishes a building process
	 * @param enityID - id of an entity
	 * @param index	- index of a building area
	 * @param seconds - seconds to elapse when building the gun
	 */
	public void BuildGun(Gun entity, int index, float seconds)
	{
		//Document that we have this entity in map
		RegisterEntity(index, entity.getID());
		
		//Add building to the array to process
		BUILD B = new BUILD();
		B.entity = entity;
		B.buildingIndex = index; 
		B.maxTime = (int)(seconds*Common.FPS_EQUALTOSECOND);
		B.variableTime = 0;
		
		Build.add(B);
	}
	
	/**
	 * Establishes an upgrading process
	 * @param enityID - id of an entity
	 * @param index	- index of a building area
	 * @param seconds - seconds to elapse when building the gun
	 */
	public void UpgradeGun(Gun entity, float seconds)
	{
		World.GunList.remove(entity);
		
		//Add building to the array to process
		BUILD B = new BUILD();
		B.entity = entity;
		B.buildingIndex = getOccupyingEnIndex(entity.getID()); 
		B.maxTime = (int)(seconds*Common.FPS_EQUALTOSECOND);
		B.variableTime = 0;
		
		Build.add(B);
	}
	
	/**
	 * Registers entity on map
	 * @param spotIndex	- index of a spot
	 * @param entityID	- id of a gun
	 * @return true if spot was empty, otherwise false
	 */
	public boolean RegisterEntity(int spotIndex, int entityID)
	{
		if(BuildingOccupied[spotIndex] == -1)
		{
			BuildingOccupied[spotIndex] = entityID;
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Unregisters entity from map
	 * @param spotIndex	- index of a spot
	 * @param entityID	- id of a gun
	 * @return true if spot was empty, otherwise false
	 */
	public void UnregisterEntity(int spotIndex)
	{
		BuildingOccupied[spotIndex] = -1;
	}
	
	/**
	 * Gets the id of an occupying entity. if the spot is clear - 0 is returned.
	 * @param indexOfSpot - index of a spot
	 * @return id returned
	 */
	public int getOccupyingEnID(int indexOfSpot)
	{
		return BuildingOccupied[indexOfSpot];
	}
	
	
	/**
	 * Gets the spot index of an occupying entity.
	 * @param indexOfSpot - index of a spot
	 * @return index returned. -1 is returned when there is no entity at that index.
	 */
	public int getOccupyingEnIndex(int ID)
	{
		for(int i = 0; i < getBuildingPosCount(); i++)
		{
			if(BuildingOccupied[i] == ID)
				return i;
		}
		
		return -1;
	}
	
	/**
	 * Checks if a spot was clicked
	 * @param x
	 * @param y
	 * @return index of a spot or -1 if nothing was clicked
	 */
	public int CheckBuildingArea(float x, float y)
	{
		Vector v = new Vector(x, y);
		
		for(int i = 0; i < numOfBuildings; i++)
		{
			if(Vector.getDistance(BuildingPos[i], v) < Settings.Instance().ENTITY_GUN_INPUT_BUILD_DISTANCE)
				return i;
		}
		return -1;
	}
	
	
	//Returns array of positions
	public Vector[] getBuildingPos() {return BuildingPos;}
	
	//Returns array of destinations
	public ArrayList<Vector> getEnemyWalkingPath() {return steps;}
	
	public int getEnemyWalkingPathCount() {return numOfSteps;}
	
	public int getBuildingPosCount() {return numOfBuildings;}
	
	//Returns starting possition
	public Vector getEnemyStartPos() {return EnemyStartPos;}
	
	//Returns final destination pos
	public Vector getEnemyEndPos() {return EnemyEndPos;}
	
	private void calculateScore(int type, int level)
	{
		Settings S = Settings.Instance();
		
		switch(type)
		{
		case Common.ENTITY_TYPE_ENEMY_STRONG_TEAR:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_ST*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_STRONG_BURN:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_SB*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_TEAR:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_T*level));
			break; 
			
		case Common.ENTITY_TYPE_ENEMY_BURN:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_B*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_TEAR:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_FT*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_FAST_BURN:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_FT*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_TEAR:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_SPT*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_SPECIAL_BURN:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_SPB*level));
			break;
			
		case Common.ENTITY_TYPE_ENEMY_BOSS:
			S.totalkillScore.setValue(S.totalkillScore.getValue()+(S.SCORE_PK_BOSS*level));
			break;
		}
	}
};


class BUILD
{
	public Gun entity;
	public int buildingIndex;
	public int variableTime, maxTime;
};