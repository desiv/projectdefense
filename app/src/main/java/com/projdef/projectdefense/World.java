/* 
 * Copyright � 2013 by Marius Savickas, all rights reserved
 * 
 */
package com.projdef.projectdefense;

import java.util.ArrayList;
import java.util.ListIterator;

import com.projdef.projectdefense.Base.BaseWorld;
import com.projdef.projectdefense.Entity.Bullet;
import com.projdef.projectdefense.Entity.Enemy;
import com.projdef.projectdefense.Entity.Explosion;
import com.projdef.projectdefense.Entity.Fire;
import com.projdef.projectdefense.Entity.Gun;
import com.projdef.projectdefense.Entity.Lightning;
import com.projdef.projectdefense.Graphics.Graphics;
import com.projdef.projectdefense.Graphics.Line;
import com.projdef.projectdefense.Manager.EntityDelayedRemove;
import com.projdef.projectdefense.Manager.OnScreenMsgManager;
import com.projdef.projectdefense.Manager.TransactionManager;
import com.projdef.projectdefense.Utilities.Common;
import com.projdef.projectdefense.Utilities.Vector;
import com.projdef.projectdefense.R;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.KeyEvent;

/**
 * Gameplay class
 * @author Marius Savickas
 *
 */
public class World extends BaseWorld
{
	//Options for building/upgrading etc
	public Options Options;
	
	
	//Information how to draw text, bitmaps
	private Paint paint = new Paint();
	private Paint paint_circle = new Paint();
	private Paint paint_health = new Paint();
	private Paint paint_health_dark = new Paint();
	
	//Create map
	public Map Map;
	public int mapNumber = 0;
	
	//Lists of sprite to interact
	public ArrayList<Enemy> EnemyList;
	public ArrayList<Gun> GunList;
	public ArrayList<Fire> FireList;
	public ArrayList<Bullet> BulletList;
	public ArrayList<Lightning> LightningList;
	public ArrayList<Explosion> ExplosionList;
	//Used for lasers
	public ArrayList<Line> LineList;
	
	//=============== Constructor ===========================
	public World()
	{
		//Text information
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setAntiAlias(Settings.Instance().AntiAlias);
		paint.setColor(Color.WHITE);
		paint.setTextSize(15);
		
		paint_circle.setStyle(Paint.Style.STROKE);
		paint_circle.setStrokeWidth(1);
		paint_circle.setAntiAlias(true);//Settings.Instance().AntiAlias);
		paint_circle.setColor(0xC00e92e6);
		
		paint_health.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_health.setStrokeWidth(1);
		paint_health.setAntiAlias(Settings.Instance().AntiAlias);
		paint_health.setColor(0xf0A00000);	//red
		
		paint_health_dark.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_health_dark.setStrokeWidth(1);
		paint_health_dark.setAntiAlias(Settings.Instance().AntiAlias);
		paint_health_dark.setColor(0xF0370000);	//red
	}
	
	//Must be called before Initialize()
	public void setMapNumber(int n)
	{
		mapNumber = n;
		Initialised = false;
	}
	
	public int getMapNumber()
	{
		return mapNumber;
	}
	
	public void Initialize()
	{	
		
		Settings.Instance().Sound.playWorld(Settings.Instance().Context);
		Settings.Instance().Sound.pauseMenu();
		
		if(Initialised) return;
	
		Settings.Instance().GOLD.setValue(9999);
		Settings.Instance().aliveHumans.setValue(0);
		Settings.Instance().killScore.setValue(0);
		Settings.Instance().totalkillScore.setValue(0);
		Settings.Instance().KILLS.setValue(0);
		Settings.Instance().deployedGuns.setValue(0);
		Settings.Instance().totalGuns.setValue(0);
		Settings.Instance().aliveZombies.setValue(0);
		Settings.Instance().totalZombies.setValue(0);
		Settings.Instance().totalHumans.setValue(0);
		Settings.Instance().releasedZombies.setValue(0);
		
		LoadStats();	//Must come before map initialization
		
		
		//****** Load map *********************************
		//*************************************************
		Map = new Map(this);
		Map.Initialize(mapNumber);
		
		OnScreenMsgManager.Instance().clear();
		Settings.Instance().Grid.clear();
		Settings.Instance().SpawnManager.reset();
		AchvmContainer.Instance().reset();
		Settings.Instance().EntityManager.reset();
		
		Settings.Instance().torchBuilt = false;
		Settings.Instance().turretBuilt = false;
		
		EnemyList 		= new ArrayList<Enemy>();
		EnemyList.ensureCapacity(40);
		
		if(GunList != null)
			for(Gun G: GunList)
				G.releasememory();
		
		GunList 		= new ArrayList<Gun>();
		GunList.ensureCapacity(10);
		
		FireList 		= new ArrayList<Fire>();
		FireList.ensureCapacity(80);
		
		BulletList 		= new ArrayList<Bullet>();
		BulletList.ensureCapacity(20);
		
		
		LightningList	= new ArrayList<Lightning>();
		LightningList.ensureCapacity(40);
		
		ExplosionList	= new ArrayList<Explosion>();
		ExplosionList.ensureCapacity(20);
		
		LineList 		= new ArrayList<Line>();
		LineList.ensureCapacity(20);
		
		Options  = new Options(this);
		Options.Initialize();
		
		Initialised = true;
		
		OnScreenMsgManager.Instance().CreateMessageQueue("Start building!", 2, 18*Settings.Instance().screenDensity);
	}
	
	//============================ Exit() ================
	//Executed on exit
	public void Exit()
	{
		for(Gun G: GunList)
		{
			Settings.Instance().Sound.pause(G.soundStreamID, true);
			G.soundPlaying = false;
		}
		
		Settings.Instance().Sound.stopWorld();
	}
	
	/////////////////////////////////////////////////////////////
	//============================ HandleInput() ================
	/////////////////////////////////////////////////////////////
	public void HandleTouchRelease(float x, float y)
	{	
		//Check if hit an option bar
		if(Options.HandleTouch(x,  y)) return;
		
		//Check if building area is valid
		int indexPos = Map.CheckBuildingArea(x, y);
		if(indexPos == Common.INVALID_BUILD_SPOT) return;	//-1 is returned if the spot has not been hit
		
		//set selected spot index
		Options.SelectSpot(indexPos, Map.getOccupyingEnID(indexPos));
	}
	
	public void HandleTouchPress(float x, float y){}
	
	public boolean HandlePhysicalKey(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
	    {
			Settings S = Settings.Instance();
			Notice notice = (Notice)Settings.Instance().Game.Notice;
			
			notice.setupStates(this, S.Game.Menu, true, true);
			notice.Text("Do you want to exit?");
			notice.Text(" ");
			notice.Text("All the progress will be lost.");
			TransactionManager.Instance().doSimple(notice);	
			return true;
	    }
		
		return false;
	}
	
	public void HandleMovement(float x, float y){}
	
	public boolean HandleExit()
	{
		Settings.Instance().Game.ChangeState(Settings.Instance().Game.Menu);
		return false;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//============================================ Draw() ===============================================
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	public void Draw(Canvas canvas)
	{	
		//===============================================================================================
		//Draw map
		Map.DrawMap();
		
		
//		for(int i = 0; i < 6; i++)
//			Settings.Instance().Canvas.drawLine(0, i*80-1, 320, i*80+1, paint);
//		
//		for(int i = 0; i < 4; i++)
//			Settings.Instance().Canvas.drawLine(i*80-1, 0, i*80+1, 480, paint);
		
		//===============================================================================================
		//Draw Guns 
		@SuppressWarnings("unchecked")
		ArrayList<Gun> arrlg = (ArrayList<Gun>)GunList.clone();
		for(Gun g: arrlg)
		{
			//Draw id
			if(Settings.Instance().DEBUG)Settings.Instance().Canvas.drawText(g.getID()+"", g.getPosition().x-15, g.getPosition().y-15, paint);
			//c.drawText(".", g._gun_sprite.getPosition().x+g._gun_sprite.getPosition().velocityX*50, g._gun_sprite.getPosition().y+g._gun_sprite.getPosition().velocityY*50, paint);
			
			//Draw circle if it is selected
			if(Options.getVisibility() && Options.getEntityID() == g.ID && !Options.getAnimationState())
				Settings.Instance().Canvas.drawCircle(g.getPosition().x, g.getPosition().y, g.getRange(), paint_circle);
			
			//Draw gun
			g.DrawSprite(Settings.Instance().Canvas);
		}
		
		
		//===============================================================================================
		//Draw enemies
		for(Enemy E: EnemyList)
		{
			//Draw ID over the enemy
			if(Settings.Instance().DEBUG)Settings.Instance().Canvas.drawText(E.getID()+"", E.getPosition().x-10, E.getPosition().y-17, paint);
			
			//Draw HP over the enemy if it is still alive
			if(E.isAlive())
			{
				
				
				Settings.Instance().Canvas.drawRect(E.getPosition().x-15*Settings.Instance().screenDensity,
													E.getPosition().y-23*Settings.Instance().screenDensity,
													E.getPosition().x-15*Settings.Instance().screenDensity+((E.getMaxHP()*30*Settings.Instance().screenDensity)/E.getMaxHP()),
													E.getPosition().y-20*Settings.Instance().screenDensity,
													paint_health_dark);
				
				float ValueHP = (E.getHP()*30*Settings.Instance().screenDensity)/E.getMaxHP();
				Settings.Instance().Canvas.drawRect(E.getPosition().x-15*Settings.Instance().screenDensity,
													E.getPosition().y-23*Settings.Instance().screenDensity,
													E.getPosition().x-15*Settings.Instance().screenDensity+ValueHP,
												    E.getPosition().y-20*Settings.Instance().screenDensity,
												    paint_health);
			}

			//c.drawText((int)E.getHP()+"", E._enemy_sprite.getPosition().x-10, E._enemy_sprite.getPosition().y-14, paint);
			
			//Draw enemy
			E.DrawSprite(Settings.Instance().Canvas);
		}
		
		//===============================================================================================
		//Draw Fire
		for(Fire f: FireList)
		{	
			//Draw circle if it is selected
			//c.drawCircle(f.FireSprite.getPosition().x, f.FireSprite.getPosition().y, f.getRange(), paint_circle);
			
			if(Settings.Instance().DEBUG)Settings.Instance().Canvas.drawText(f.getID()+"", f.getPosition().x-15, f.getPosition().y-15, paint);
			f.DrawSprite(Settings.Instance().Canvas);
		}
		
		
		//===============================================================================================
		//Draw Bullet
		for(Bullet B: BulletList)
		{	
			//Draw circle if it is selected
			//c.drawCircle(f.FireSprite.getPosition().x, f.FireSprite.getPosition().y, f.getRange(), paint_circle);
			
			if(Settings.Instance().DEBUG) Settings.Instance().Canvas.drawText(B.getID()+"", B.getPosition().x-15, B.getPosition().y-15, paint);
			B.DrawSprite(Settings.Instance().Canvas);
		}
		
		//===============================================================================================
		//Draw Lightning
		for(Lightning L: LightningList)
		{	
			//Draw circle if it is selected
			//c.drawCircle(f.FireSprite.getPosition().x, f.FireSprite.getPosition().y, f.getRange(), paint_circle);
			
			//if(Settings.Instance().DEBUG) Settings.Instance().Canvas.drawText(B.getID()+"", B.getPosition().x-15, B.getPosition().y-15, paint);
			L.Draw(Settings.Instance().Canvas);
		}
		
		
		//===============================================================================================
		//Draw Lines
		for(Line L: LineList)
		{	
			//Draw circle if it is selected
			//c.drawCircle(f.FireSprite.getPosition().x, f.FireSprite.getPosition().y, f.getRange(), paint_circle);
			
			//if(Settings.Instance().DEBUG) Settings.Instance().Canvas.drawText(B.getID()+"", B.getPosition().x-15, B.getPosition().y-15, paint);
			L.Draw(Settings.Instance().Canvas);
		}
		LineList.clear();
		
		
		//===============================================================================================
		//Draw Explosions
		for(Explosion E: ExplosionList)
		{	
			E.DrawSprite(Settings.Instance().Canvas);
		}
		
		
		Map.DrawMapEffects();
		
		//Draw options
		Options.Draw();
		
		//Draw messages
		OnScreenMsgManager.Instance().Draw();
			
		Map.DrawStats();
		
		/* Draw fps */
		//Settings.Instance().Canvas.drawText(Settings.Instance().Game.fps + "fps", 260, 60, paint);
		
		//Debug: wave
		//Settings.Instance().Canvas.drawText("Wave: " + Settings.Instance().SpawnManager.indexWave, 200, 60, paint);
		
		//Draw on final canvas
		canvas.drawBitmap(Settings.Instance().BBuffer, 0,  0, new Paint(Paint.ANTI_ALIAS_FLAG));
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	//============================================ Physics() =======================================
	////////////////////////////////////////////////////////////////////////////////////////////////
	public void Physics()
	{
		//Check if the game is still going, all the humans are alive
		//if(Settings.Instance().HUMANS.getValue() < 1) Settings.Instance().Game.ChangeState(Settings.Instance().Game.Lost);
		
		try
		{
			AchvmContainer.Instance().Process();
		}catch (NullPointerException e)
		{
			Log.e("Achievement", "Might be a wrong label: " + e);
			Settings.Instance().Activity.finish();
		}
		
		//Update everything
		Map.Update();
		Options.Update();
		
		//Process messages
		OnScreenMsgManager.Instance().Process();
		//Send Delayed messages from queue
		MessageDispatcher.Instance().DispatchDelayedMessages();
		//Loops through list of entities to be removed
		EntityDelayedRemove.Instance().Process(this);
		
		//Spawns enemies
		Settings.Instance().SpawnManager.onSpawn();
		
		//Update Enemy AI
		for(Enemy E: EnemyList)
			E.UpdateAI();	
		
		//Update Gun AI
		for(Gun G: GunList)
			G.UpdateAI();
		
		//Update Lightning AI
		for(Fire F: FireList)
			F.UpdateAI();
		
		//Update Lightning AI
		for(Bullet B: BulletList)
			B.UpdateAI();
		
		//Update Lightning AI
		for(Lightning L: LightningList)
				L.UpdateAI();
		
		//Update explosions
		ListIterator<Explosion> iter = ExplosionList.listIterator();
		Explosion exp;
		while(iter.hasNext())
		{
			exp = iter.next();
			exp.UpdateAI();
			if(!exp.getState()) iter.remove();
		}
		
		Tutorial();
	}
	
	
	
	//======================= CreateEnemies() =======================
	public void CreateEnemies(int lvl, int type)
	{
			Enemy E = new Enemy();
			E.Initialize(Map,  Map.getEnemyStartPos().x, Map.getEnemyStartPos().y, lvl, type);
			EnemyList.add(E);
	}
	
	//======================= CreateGuns() =======================
	public boolean CreateGun(int indexPos, int type, float buildTime)
	{	
		
		switch(type)
		{
		case Common.ENTITY_TYPE_GUN_TURRET:
			//Check to see if you can afford to build a gun
			if((Settings.Instance().GOLD.getValue() - Settings.Instance().COST_BULLET_LEVEL_1) < 0){return false;}
			Settings.Instance().GOLD.setValue(Settings.Instance().GOLD.getValue() - Settings.Instance().COST_BULLET_LEVEL_1);
			break;
			
		case Common.ENTITY_TYPE_GUN_TORCH:
			if((Settings.Instance().GOLD.getValue() - Settings.Instance().COST_FIRE_LEVEL_1) < 0){return false;}
			Settings.Instance().GOLD.setValue(Settings.Instance().GOLD.getValue() - Settings.Instance().COST_FIRE_LEVEL_1);
			break;
		};
		
		Gun G = new Gun(this);
		G.Initialize(Map, indexPos, type);
		
		//Build entity in map
		Map.BuildGun(G, indexPos, buildTime);
		
		Settings.Instance().CostIncrease(Common.COST_INCREASE);
		
		return true;
	}
	
	public void CreateFire(float x, float y, float Vx, float Vy, float rotation, int level, Object extr)
	{
		Fire F = new Fire();
		F.Initialize(x, y, Vx, Vy, rotation, level, extr);
		
		FireList.add(F);
	}
	
	
	public void CreateBullet(float x, float y, float Vx, float Vy, float rotation, Object extr)
	{
		Bullet B = new Bullet(this);
		B.Initialize(x, y, Vx, Vy, rotation, extr);
		
		BulletList.add(B);
	}
	
	public void CreateLightning(Vector src, Vector dest, int thickness)
	{
		Lightning L = new Lightning(src, dest, thickness);
		
		LightningList.add(L);
	}
	
	
	public void CreateLaser(Vector src, Vector dest, int thickness)
	{
		Line L = new Line(Graphics.LASER, src, dest, thickness);
		LineList.add(L);
	}
	
	public void CreateBlueLaser(Vector src, Vector dest, int thickness)
	{
		Line L = new Line(src, dest, thickness);
		LineList.add(L);
	}
	
	public void CrateExplosion(Vector coords)
	{
		ExplosionList.add( new Explosion(Graphics.EXPLOSION, coords.x, coords.y) );
	}
	
	/**
	 * Reduces money
	 * @param amount - amount to reduce
	 * @return returns true if operation was successful, otherwise false.
	 */
	public boolean ReduceMoney(int amount)
	{
		if(Settings.Instance().GOLD.getValue() - amount >= 0)
		{
			Settings.Instance().GOLD.setValue(Settings.Instance().GOLD.getValue() - amount);
			return true;
		}
		return false;
	}
	
	
	public void reloadSound()
	{
		if(GunList != null)
			for(Gun G: GunList)
				if(G != null)
					G.loadSound();
	}
	
	public void unloadSound()
	{
		if(GunList != null)
			for(Gun G: GunList)
				if(G != null)
					G.unloadSound();
	}
	
	private void Tutorial()
	{
		//Check whether it's the first map
		if(mapNumber != 0) return;
		
		Player P = Player.Instance();
		
		if((P.tutorialFlags & Common.FLAG_TUT_ON) == 0)
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			MapSelect MS = (MapSelect)S.Game.MapSelect;
			
			Settings.Instance().stopFPSCount = true;
			
			//NOTE: tutorial flag is adjusted in the notice
			notice.setupStates(this,  MS.new TutOn(), MS.new TutOff());
			notice.Text("Do you want a small tutorial?");
			TransactionManager.Instance().doSimple(notice);
		}
		else if((P.tutorialFlags & Common.FLAG_TUT1) == 0)
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT1);
			Player.Instance().SaveFile();
		
			Settings.Instance().stopFPSCount = true;
			notice.setupStates(this, this, false, false);
			notice.Text("The main idea of this game is to kill everything that is moving from point A to point B. ");
			notice.Text("The entry (point A) is marked with: ");
			notice.Image(Graphics.loadImg(R.drawable.entry, 1));
			notice.Text("In point B, there are humans, which you have to protect (they are hiding behind the screen). To enable the protection and initiate the massacre you have to press on ");
			notice.Image(Graphics.loadImg(R.drawable.ba1, 1));
			notice.Text("which will bring up an option menu where you can select the weapon of mass destruction.");
			notice.Text("Guns shoot automatically, so you only need to put them strategically.");
			TransactionManager.Instance().doSimple(notice);
		}
		else if(((P.tutorialFlags & Common.FLAG_TUT2) == 0) && (Settings.Instance().deployedGuns.getValue() >= 1))
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT2);
			Player.Instance().SaveFile();
			
			Settings.Instance().stopFPSCount = true;
			notice.setupStates(this, this, false, false);
			notice.Text("In this age the building costs are huge, so keep an eye on your current gold balance and the gun's prices.");
			TransactionManager.Instance().doSimple(notice);
		}
		else if(((P.tutorialFlags & Common.FLAG_TUT3) == 0) && (Settings.Instance().releasedZombies.getValue() >= 1))
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT3);
			Player.Instance().SaveFile();
			
			Settings.Instance().stopFPSCount = true;
			notice.setupStates(this, this, false, false);
			notice.Text("Every enemy is different: monster might be weak against the Turret branch, but strong against the Torch branch and vice versa.");
			notice.Text("");
			notice.Text("Turret branch will disintegrate it faster: ");
			notice.Image(Graphics.loadImg(R.drawable.burn, 1));
			notice.Text("");
			notice.Text("Torch branch will disintegrate it faster: ");
			notice.Image(Graphics.loadImg(R.drawable.tear, 1));
			notice.Text("");
			TransactionManager.Instance().doSimple(notice);
		}
		else if(((P.tutorialFlags & Common.FLAG_TUT5) == 0) && (Settings.Instance().torchBuilt == true))
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT5);
			Player.Instance().SaveFile();
			
			Settings.Instance().stopFPSCount = true;
			notice.setupStates(this, this, false, false);
			notice.Text("Torch branch has Fire, Frost and Poison types. ");
			notice.Text("");
			notice.Text("* Fire does 1.5x damage");
			notice.Image(Graphics.loadImg(R.drawable.fire1, 1));
			notice.Text("* Frost does 0.5x damage and stops monsters. ");
			notice.Image(Graphics.loadImg(R.drawable.frost, 1));
			notice.Text("* Poison does 1.0x damage and does a 20pts poison damage (Works on every monster). ");
			notice.Image(Graphics.loadImg(R.drawable.poison, 1));
			notice.Text("");
			notice.Text("By combining them you can make the gun stronger. ");
			TransactionManager.Instance().doSimple(notice);
		}
		else if(((P.tutorialFlags & Common.FLAG_TUT6) == 0) && (Settings.Instance().turretBuilt == true))
		{
			Settings S = Settings.Instance();
			Notice notice = (Notice)S.Game.Notice;
			
			Player.Instance().tutorialFlags = (byte)(Player.Instance().tutorialFlags | Common.FLAG_TUT6);
			Player.Instance().SaveFile();
			
			Settings.Instance().stopFPSCount = true;
			notice.setupStates(this, this, false, false);
			notice.Text("Turret branch has Bullet, Electricity and Laser types. ");
			notice.Text("");
			notice.Text("* Bullet does a single damage. ");
			notice.Image(Graphics.loadImg(R.drawable.bullet1, 1));
			notice.Text("* Electricity damages multiple monsters simultaneously. ");
			notice.Image(Graphics.loadImg(R.drawable.lightning_thumb, 1));
			notice.Text("* Laser makes a continuous damage");
			notice.Image(Graphics.loadImg(R.drawable.laser_thumb, 1));
			notice.Text("");
			TransactionManager.Instance().doSimple(notice);
		}
	}
	
	//======================= Load stats ===========================
	private void LoadStats()
	{	
		Settings S = Settings.Instance();
		
		/*
		 * Cost
		 */
		S.COST_BULLET_LEVEL_1 		= 50;
		S.COST_FIRE_LEVEL_1 		= 90;
		
		S.COST_BULLET_LEVEL_2		= 85;
		S.COST_BULLET_LEVEL_3		= 225;
		
		S.COST_ELECTR_LEVEL_2		= 250;
		S.COST_ELECTR_LEVEL_3		= 400;
		
		S.COST_LASER_LEVEL_2		= 150;
		S.COST_LASER_LEVEL_3		= 255;
		
		S.COST_FIRE_LEVEL_2			= 250;
		S.COST_FIRE_LEVEL_3			= 500;
		
		S.COST_POISON_LEVEL_2		= 300;
		S.COST_POISON_LEVEL_3		= 450;
		
		S.COST_FREEZE_LEVEL_2		= 150;
		S.COST_FREEZE_LEVEL_3		= 300;
		
		
		//Score per kill
		S.SCORE_PK_T	= 50;
		S.SCORE_PK_B	= 85;
		S.SCORE_PK_FT	= 90;
		S.SCORE_PK_FB	= 110;
		S.SCORE_PK_ST	= 150;
		S.SCORE_PK_SB	= 185;
		S.SCORE_PK_SPT	= 120;
		S.SCORE_PK_SPB	= 160;
		S.SCORE_PK_BOSS	= 300;
	}
};