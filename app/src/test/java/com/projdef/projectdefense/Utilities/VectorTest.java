package com.projdef.projectdefense.Utilities;

import com.projdef.projectdefense.Utilities.Vector;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VectorTest
    {
    @Test
    public void cloneTest () throws Exception
        {
        Vector vec = new Vector (1, -1);
        Vector vec2 = vec.clone ();
        assertEquals (1, vec2.x, DELTA);
        assertEquals (-1, vec2.y, DELTA);
        }

    private static double DELTA = 0.0001;

    @Test
    public void setDirection () throws Exception
        {
        Vector vec = new Vector (0, 0);
        vec.setDirection (5, 5);
        assertEquals (5, vec.velocityX, DELTA);
        assertEquals (5, vec.velocityY, DELTA);
        }

    @Test
    public void setDirectionWithNormalize () throws Exception
        {
        Vector vec = new Vector (new Vector (0,0));
        vec.setDirection (5, 0, true);
        assertEquals (1, vec.velocityX, DELTA);
        assertEquals (0, vec.velocityY, DELTA);
        }


    @Test
    public void set () throws Exception
        {
        Vector vec = new Vector (0, 0);
        vec.set (1, -1);
        assertEquals (1, vec.x, DELTA);
        assertEquals (-1, vec.y, DELTA);
        }

    @Test
    public void setWithVector () throws Exception
        {
        Vector vec = new Vector (0, 0);
        vec.set (new Vector (1, -1));
        assertEquals (1, vec.x, DELTA);
        assertEquals (-1, vec.y, DELTA);
        }


    @Test
    public void normalize () throws Exception
        {
        Vector vec = new Vector ();
        vec.setDirection (5, 5);
        vec.Normalize ();
        assertEquals (0.7071, vec.velocityX, DELTA);
        assertEquals (0.7071, vec.velocityY, DELTA);
        }

    @Test
    public void getNormalized () throws Exception
        {
        Vector vec = new Vector ();
        vec.setDirection (5, 5);
        vec.Normalize ();
        vec = vec.getNormalized ();
        assertEquals (0.7071, vec.x, DELTA);
        assertEquals (0.7071, vec.y, DELTA);
        }

    @Test
    public void isNormilzed () throws Exception
        {
        Vector vec = new Vector ();
        vec.setDirection (5, 5);
        vec.Normalize ();
        assertEquals (true, vec.isNormilzed ());
        }

    @Test
    public void getDistance () throws Exception
        {
        assertEquals (5, Vector.getDistance (new Vector (0, 0), new Vector (0, 5)), DELTA);
        }

    @Test
    public void getDistance_Null () throws Exception
        {
        assertEquals (-1, Vector.getDistance (null, new Vector (0, 5)), DELTA);
        }

    @Test
    public void getAngleBetweenVectors () throws Exception
        {
        Vector a = new Vector (0.12f, 0.56f);
        Vector b = new Vector (-0.34f, 0.125f);
        assertEquals (1.5415, Vector.getAngleBetweenVectors (a, b), DELTA);

        Vector d = new Vector (0.4f, 0.25f);
        Vector g = new Vector (-0.35f, -0.2f);
        assertEquals (1.7619, Vector.getAngleBetweenVectors (d, g), DELTA);

        Vector h = new Vector (1, 0);
        Vector j = new Vector (0, -1);
        assertEquals (90, Vector.getAngleBetweenVectors (h, j), DELTA);
        }


    @Test
    public void subtract () throws Exception
        {
        Vector a = new Vector (5, 5);
        Vector b = new Vector (3, 4);
        Vector c = a.subtract (b);
        assertEquals (2, c.x, DELTA);
        assertEquals (1, c.y, DELTA);
        }

    @Test
    public void add () throws Exception
        {
        Vector a = new Vector (5, 5);
        Vector b = new Vector (3, 4);
        Vector c = a.add (b);
        assertEquals (8, c.x, DELTA);
        assertEquals (9, c.y, DELTA);
        }

     @Test
    public void multiply () throws Exception
        {
        Vector a = new Vector (5, 5);
        Vector c = a.multiply (5);
        assertEquals (25, c.x, DELTA);
        assertEquals (25, c.y, DELTA);
        }

     @Test
    public void multiplyWithVector () throws Exception
        {
        Vector a = new Vector (5, 5);
        Vector c = a.multiply (a);
        assertEquals (25, c.x, DELTA);
        assertEquals (25, c.y, DELTA);
        }

     @Test
    public void length () throws Exception
        {
        Vector a = new Vector (4, 4);
        assertEquals (5, a.length ());
        }

     @Test
    public void getSign () throws Exception
        {
        Vector a = new Vector (4, 4);
        Vector b = new Vector (2, 2);
        assertEquals (-1, Vector.getSign (a, b));

        Vector d = new Vector (0, 1);
        Vector g = new Vector (2, 2);
        assertEquals (1, Vector.getSign (d, g));
        }
    }